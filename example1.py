import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from npbad.utils import load_numenta_data
from npbad.ipbad.main import run_ipbad
from npbad.eval_methods import eval_best_f1
from npbad.visualisation import plot_data

taxi = './data/realKnownCause/nyc_taxi.csv'
labels = './data/realKnownCause/combined_labels.json'
df, anomalies = load_numenta_data(taxi, labels)

#run anomaly detection
an_scores, ts_i, segments_discretised, windows = run_ipbad( df=df, interval=96, stride=1, no_symbols=5, no_bins=10)                                                                             
(f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies, point_adjust=False)
print(f"F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")

#plot time series, discrete sequence and groundtruth and predicted anomalies
fig, axs = plt.subplots(2, figsize=(30,3), sharex=True)
plot_data(axs[0], df, anomalies)
timestamps = [window[0] for window, score in an_scores]
scores = [score for window, score in an_scores]
axs[1].plot(timestamps, scores, color='red',alpha=0.5)
axs[1].axhline(y=t, color='b', linestyle='--')
axs[1].set_ylabel('IPBAD')
plt.show()
