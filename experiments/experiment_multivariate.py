import pandas as pd
import random
import numpy as np
from collections import defaultdict
import time

from utils import print_full
from preprocess_timeseries import pre_process_mv_df
from eval_methods import eval_best_f1
from baselines.iso_forest import run_iso_forest_mv
from ipbad.main import run_ipbad_mv
from visualisation import plot_mv_data

np.set_printoptions(suppress=True, precision=3)



#load data
data_folder = '/Users/lfereman/git2/conscious/data/multivariate/SMD/'

results = defaultdict(list)

time_start_overall = time.time()
machines = ['machine-1-1','machine-1-2','machine-1-3',
                'machine-1-4','machine-1-5','machine-1-6',
                'machine-1-7','machine-1-8',
                'machine-2-1','machine-2-2','machine-2-3',
                'machine-2-4','machine-2-5','machine-2-6',
                'machine-1-7','machine-1-8','machine-2-9',
                'machine-3-1','machine-3-2','machine-3-3',
                'machine-3-4','machine-3-5','machine-3-6',
                'machine-3-7','machine-3-8','machine-3-9',
                'machine-3-10','machine-3-11']
machines = []
for machine in machines:
    #load data
    machine_1_test = f'{machine}_test.csv'
    machine_1_train = f'{machine}_train.csv'
    machine_1_test_labels = f'{machine}_test_label.csv'
    machine_1_interpretation_label = f'{machine}_interpretation_label.csv'

    #combine training and test
    df_train = pd.read_csv(data_folder + machine_1_train) #i.e. contains metric-1,...,metric-38,time_id,time
    df_train['time'] = pd.to_datetime(df_train['time'])
    df_train = df_train.drop(columns=['time_id'])
    df_test = pd.read_csv(data_folder + machine_1_test) #i.e. contains metric-1,...,metric-38,time_id,time
    df_test['time'] = pd.to_datetime(df_test['time'])
    df_test = df_test.drop(columns=['time_id'])
    #test is right after train, so add date
    diff_seconds = int((df_train['time'].max() - df_test['time'].min()) / np.timedelta64(1, 's'))
    df_test['time'] = df_test['time'].apply(lambda x: x + np.timedelta64(diff_seconds,'s'))
    print("test head after adding time:")
    print(df_test.head())
    df = pd.concat([df_train, df_test])    
    labels_df = pd.read_csv(data_folder + machine_1_test_labels) #i.e. contains label,time_id,time where label==1
    labels_df['time'] = pd.to_datetime(labels_df['time'])
    #also for labels
    labels_df['time'] = labels_df['time'].apply(lambda x: x + np.timedelta64(diff_seconds,'s'))
    
    print(labels_df.head())
    #labels_inter_df = pd.read_csv(data_folder + machine_1_interpretation_label) #i.e. contains from_time_id,to_time_id,dimensions
    #for SMD: known for which subgroup of metrics causes anomaly... ignore for now
    anomalies = [row['time'] for idx, row in labels_df.iterrows()]
    print(f'No anomalies: {len(anomalies)}. First: {anomalies[0]}')
    
    df, components = pre_process_mv_df(df)   
    print_full(df)
    #window of 100 used, i.e. every 1.5hour
    #anomaly is next value
    
    #random search
    do_random_search = False
    if do_random_search:
        grid={"interval": [1,2,4,12,24,72],
              "stride": [1],
              "no_symbols": list(range(4,20,2)), 
              "no_bins" : list(range(3,20,2)) + [50,100],
              "preprocess_outlier": [True, False], 
              "use_MDL": [True, False],
              "binning_method": ['normal','local','kmeans'],
              "use_iso": [True, False]
              }
        no_iter = 150
        best_parameters = None
        best_metrics = None
        best_an_scores = None
        while no_iter > 0:
            random_parameter = {}
            for key, options in grid.items():
                idx = random.randint(0,len(options)-1)
                random_parameter[key] = options[idx]
            print(f"random parameters: {random_parameter}")
            try:
                an_scores = run_ipbad_mv(df,  **random_parameter)
                an_score_second_half = []
                for window, score in an_scores:
                    if window[0] > df_test['time'].min():
                        an_score_second_half.append((window,score))
                (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies, point_adjust=False)
                print(f"F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}")
                no_iter = no_iter - 1
                if best_metrics is None or f1 > best_metrics[0]:
                    best_metrics =   (f1,prec,rec,TP,FP,FN,TN, t)
                    best_parameters = random_parameter
                    best_an_scores = an_scores
            except Exception as e:
                print(e)
        print('#### Best result ####')
        (f1,prec,rec,TP,FP,FN,TN, t) = best_metrics
        print(f"Parameters: {best_parameters}")
        print(f"F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN: {TN}")
        an_scores = best_an_scores
        ''' best:
        -random parameters: {'interval': 24, 'stride': 1, 'no_symbols': 4, 'no_bins': 13, 'preprocess_outlier': True, 'use_MDL': False, 'binning_method': 'local', 'use_iso': False}
         F1@best: 0.9200, precision: 0.9306, recall: 0.9096, threshold: 0.9998, TP: 161, FP: 12, FN:16
        -random parameters: {'interval': 12, 'stride': 1, 'no_symbols': 8, 'no_bins': 13, 'preprocess_outlier': False, 'use_MDL': False, 'binning_method': 'normal', 'use_iso': True}
         F1@best: 0.8845, precision: 0.9569, recall: 0.8222, threshold: 0.4081, TP: 111, FP: 5, FN:24
        -random parameters: {'interval': 72, 'stride': 1, 'no_symbols': 4, 'no_bins': 5, 'preprocess_outlier': False, 'use_MDL': True, 'binning_method': 'local', 'use_iso': True}
         F1@best: 0.9746, precision: 0.9505, recall: 1.0000, threshold: 0.4396, TP: 211, FP: 11, FN:0
        - random parameters: {'interval': 72, 'stride': 1, 'no_symbols': 12, 'no_bins': 11, 'preprocess_outlier': False, 'use_MDL': False, 'binning_method': 'kmeans', 'use_iso': False}
         F1@best: 0.8621, precision: 0.8929, recall: 0.8333, threshold: 0.9997, TP: 50, FP: 6, FN:10
        -random parameters: {'interval': 72, 'stride': 1, 'no_symbols': 16, 'no_bins': 7, 'preprocess_outlier': True, 'use_MDL': True, 'binning_method': 'kmeans', 'use_iso': True}
         F1@best: 0.9701, precision: 0.9420, recall: 1.0000, threshold: 0.4699, TP: 211, FP: 13, FN:0
        '''
        
        '''Re-run 16/06
        random parameters: {'interval': 24, 'stride': 1, 'no_symbols': 4, 'no_bins': 15, 'preprocess_outlier': False, 'use_MDL': False, 'binning_method': 'kmeans', 'use_iso': False}
        F1@best: 0.7122, precision: 0.9802, recall: 0.5593, threshold: 0.9990, TP: 99, FP: 2, FN:78
        random parameters: {'interval': 72, 'stride': 1, 'no_symbols': 4, 'no_bins': 19, 'preprocess_outlier': True, 'use_MDL': True, 'binning_method': 'normal', 'use_iso': False}
        F1@best: 0.7350, precision: 0.6933, recall: 0.7820, threshold: 0.9996, TP: 165, FP: 73, FN:46
        random parameters: {'interval': 24, 'stride': 1, 'no_symbols': 18, 'no_bins': 50, 'preprocess_outlier': False, 'use_MDL': True, 'binning_method': 'kmeans', 'use_iso': False}
        F1@best: 0.7247, precision: 0.9455, recall: 0.5876, threshold: 0.9997, TP: 104, FP: 6, FN:73
        random parameters: {'interval': 4, 'stride': 1, 'no_symbols': 4, 'no_bins': 7, 'preprocess_outlier': False, 'use_MDL': False, 'binning_method': 'kmeans', 'use_iso': False}
        F1@best: 0.6951, precision: 0.6477, recall: 0.7500, threshold: 0.9965, TP: 57, FP: 31, FN:19
        '''
    run_hetero = True
    if run_hetero:
        preprocess_parameter = {'interval': 72, 'no_symbols': 4, 'no_bins': 20} #no_bins was 5
        preprocess_outlier=False
        
        for iter in range(0,5):
            #iso forest
            an_scores = run_iso_forest_mv(df, interval=preprocess_parameter['interval'], stride=1, preprocess_outlier=preprocess_outlier)
            an_score_second_half = []
            for window, score in an_scores:
                if window[0] > df_test['time'].min():
                    an_score_second_half.append((window,score))
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_score_second_half, anomalies)
            print(f"ISO F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results[f'iso_{machine}'].append(f1)
            
            #ipbad
            an_scores = run_ipbad_mv(df,  interval=preprocess_parameter['interval'], stride=1, no_symbols=preprocess_parameter['no_symbols'], no_bins=preprocess_parameter['no_bins'], 
                                           preprocess_outlier=preprocess_outlier, use_MDL=True, binning_method='normal', use_iso=True)
            #eval after time
            an_score_second_half = []
            for window, score in an_scores:
                if window[0] > df_test['time'].min():
                    an_score_second_half.append((window,score))
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_score_second_half, anomalies)
            print(f"IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results[f'ipbad_{machine}'].append(f1)
            #an_scores = sorted(an_scores, key=lambda p: p[0][0], reverse=False)
            #plot_mv_data(f'SMD-{machine}', df, anomalies, components, an_scores)  
            
            #ipbad-fpof
            an_scores = run_ipbad_mv(df,  interval=preprocess_parameter['interval'], stride=1, no_symbols=preprocess_parameter['no_symbols'], no_bins=preprocess_parameter['no_bins'], 
                                           preprocess_outlier=preprocess_outlier, use_MDL=True, binning_method='normal', use_iso=False)
            #eval after time
            an_score_second_half = []
            for window, score in an_scores:
                if window[0] > df_test['time'].min():
                    an_score_second_half.append((window,score))
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_score_second_half, anomalies)
            print(f"IPBAD-FPOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results[f'ipbad_fpof_{machine}'].append(f1)
        for key, values in results.items():
            print(f'method-dataset: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}. vals: {values}')  

for key, values in results.items():
    print(f'method-dataset: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}. vals: {values}')  
print(results)
print(f'total elapsed: {time.time() - time_start_overall}s')
#37.03 min
d = {'iso_machine-1-1': [1.0, 0.9952830188679246, 0.9768518518518519, 1.0, 0.9906103286384975], 'ipbad_machine-1-1': [0.9154013015184382, 0.8791666666666667, 0.8921775898520083, 0.8959660297239915, 0.9114470842332613], 'ipbad_fpof_machine-1-1': [0.9336283185840708, 0.9336283185840708, 0.9336283185840708, 0.9336283185840708, 0.9336283185840708], 'iso_machine-1-2': [0.9591397849462365, 0.9449152542372881, 0.9529914529914529, 0.9612068965517241, 0.9469214437367304], 'ipbad_machine-1-2': [0.9098039215686274, 0.9570815450643777, 0.9151873767258382, 0.9591397849462365, 0.9373737373737374], 'ipbad_fpof_machine-1-2': [0.983050847457627, 0.983050847457627, 0.983050847457627, 0.983050847457627, 0.983050847457627], 'iso_machine-1-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-1-3': [0.9966555183946488, 0.998324958123953, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-1-3': [0.994991652754591, 0.994991652754591, 0.994991652754591, 0.994991652754591, 0.994991652754591], 'iso_machine-1-4': [0.9778534923339013, 0.9761904761904763, 0.9761904761904763, 0.9761904761904763, 0.9761904761904763], 'ipbad_machine-1-4': [0.9614740368509213, 0.9471947194719472, 0.959866220735786, 0.9630872483221476, 0.9550748752079866], 'ipbad_fpof_machine-1-4': [0.9876543209876543, 0.9876543209876543, 0.9876543209876543, 0.9876543209876543, 0.9876543209876543], 'iso_machine-1-5': [0.9393139841688655, 0.978021978021978, 0.9319371727748691, 0.9393139841688655, 0.9222797927461139], 'ipbad_machine-1-5': [0.9518716577540107, 0.9175257731958762, 0.9104859335038362, 0.898989898989899, 0.9035532994923858], 'ipbad_fpof_machine-1-5': [0.9319371727748691, 0.9319371727748691, 0.9319371727748691, 0.9319371727748691, 0.9319371727748691], 'iso_machine-1-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-1-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-1-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-1-7': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-1-7': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-1-7': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-1-8': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-1-8': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-1-8': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-2-1': [0.9965156794425087, 1.0, 0.9896193771626297, 0.9678510998307953, 0.9828178694158076], 'ipbad_machine-2-1': [0.9794520547945206, 0.9727891156462585, 0.9761092150170648, 0.9777777777777777, 0.9828178694158076], 'ipbad_fpof_machine-2-1': [0.9982547993019197, 0.9982547993019197, 0.9982547993019197, 0.9982547993019197, 0.9982547993019197], 'iso_machine-2-2': [0.9680851063829787, 0.962962962962963, 0.9584487534626039, 0.9918256130790191, 0.9918256130790191], 'ipbad_machine-2-2': [0.9758713136729222, 1.0, 0.9918256130790191, 0.994535519125683, 0.986449864498645], 'ipbad_fpof_machine-2-2': [0.9972602739726028, 0.9972602739726028, 0.9972602739726028, 0.9972602739726028, 0.9972602739726028], 'iso_machine-2-3': [0.9921011058451817, 0.9858712715855572, 0.9968253968253968, 0.9921011058451817, 0.9968253968253968], 'ipbad_machine-2-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-2-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-2-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-2-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-2-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-2-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-2-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-2-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-2-6': [0.9915682967959528, 0.98989898989899, 0.9702970297029703, 0.9719008264462811, 0.9816360601001669], 'ipbad_machine-2-6': [0.9607843137254902, 0.9029982363315697, 0.9408000000000001, 0.92018779342723, 0.9453376205787781], 'ipbad_fpof_machine-2-6': [0.8634361233480177, 0.8634361233480177, 0.8634361233480177, 0.8634361233480177, 0.8634361233480177], 'iso_machine-2-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-2-9': [0.9934354485776805, 0.9912663755458515, 0.9690265486725663, 0.9978021978021978, 0.9956140350877193], 'ipbad_fpof_machine-2-9': [0.9912663755458515, 0.9912663755458515, 0.9912663755458515, 0.9912663755458515, 0.9912663755458515], 'iso_machine-3-1': [0.6556169429097606, 0.6508226691042048, 0.6300884955752213, 0.6368515205724509, 0.6312056737588653], 'ipbad_machine-3-1': [0.8538011695906433, 0.8488372093023256, 0.8821752265861027, 0.8690476190476191, 0.896358543417367], 'ipbad_fpof_machine-3-1': [0.6941431670281996, 0.6941431670281996, 0.6941431670281996, 0.6941431670281996, 0.6941431670281996], 'iso_machine-3-2': [0.9705882352941176, 0.9721767594108021, 0.9642857142857143, 0.9753694581280787, 0.9658536585365853], 'ipbad_machine-3-2': [0.9883527454242929, 0.9883527454242929, 0.9883527454242929, 0.9850746268656716, 0.9834437086092715], 'ipbad_fpof_machine-3-2': [0.9753694581280787, 0.9753694581280787, 0.9753694581280787, 0.9753694581280787, 0.9753694581280787], 'iso_machine-3-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_machine-3-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_machine-3-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_machine-3-4': [0.9816971713810315, 0.9833333333333333, 0.9800664451827242, 0.9800664451827242, 0.9816971713810315], 'ipbad_machine-3-4': [1.0, 0.9949409780775718, 0.9983079526226735, 1.0, 1.0], 'ipbad_fpof_machine-3-4': [0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621], 'iso_machine-3-5': [0.9469548133595285, 0.9432485322896282, 0.94140625, 0.9525691699604744, 0.9447415329768272], 'ipbad_machine-3-5': [0.9566787003610108, 0.9671532846715328, 0.9601449275362318, 0.9636363636363636, 0.9636363636363636], 'ipbad_fpof_machine-3-5': [0.9464285714285715, 0.9464285714285715, 0.9464285714285715, 0.9464285714285715, 0.9464285714285715], 'iso_machine-3-6': [0.9985855728429986, 1.0, 1.0, 0.995768688293371, 0.9985855728429986], 'ipbad_machine-3-6': [0.9901823281907433, 0.988795518207283, 0.9671232876712329, 0.988795518207283, 0.9583931133428981], 'ipbad_fpof_machine-3-6': [0.9971751412429378, 0.9971751412429378, 0.9971751412429378, 0.9971751412429378, 0.9971751412429378], 'iso_machine-3-7': [0.8484848484848484, 0.7916666666666666, 0.8347457627118643, 0.8329809725158561, 0.8542372881355932], 'ipbad_machine-3-7': [0.9725776965265083, 0.9743589743589743, 0.9851851851851852, 0.9708029197080292, 0.9925373134328358], 'ipbad_fpof_machine-3-7': [0.8650406504065041, 0.8650406504065041, 0.8650406504065041, 0.8650406504065041, 0.8650406504065041], 'iso_machine-3-8': [0.9933184855233852, 0.9933184855233852, 0.9933184855233852, 0.9933184855233852, 0.9933184855233852], 'ipbad_machine-3-8': [0.9818181818181819, 0.9707865168539327, 0.9802197802197802, 0.9773755656108596, 0.9674620390455532], 'ipbad_fpof_machine-3-8': [0.9889135254988913, 0.9889135254988913, 0.9889135254988913, 0.9889135254988913, 0.9889135254988913], 'iso_machine-3-9': [0.6334519572953736, 0.6312056737588653, 0.6300884955752213, 0.6357142857142857, 0.6368515205724509], 'ipbad_machine-3-9': [0.9012345679012346, 0.8957055214723927, 0.9012345679012346, 0.8957055214723927, 0.9012345679012346], 'ipbad_fpof_machine-3-9': [0.8957055214723927, 0.8957055214723927, 0.8957055214723927, 0.8957055214723927, 0.8957055214723927], 'iso_machine-3-10': [0.9304635761589404, 0.9304635761589404, 0.9304635761589404, 0.9304635761589404, 0.9304635761589404], 'ipbad_machine-3-10': [0.9689655172413794, 0.9672977624784854, 0.9672977624784854, 0.9689655172413794, 0.9842381786339756], 'ipbad_fpof_machine-3-10': [0.9304635761589404, 0.9304635761589404, 0.9304635761589404, 0.9304635761589404, 0.9304635761589404], 'iso_machine-3-11': [0.8543689320388349, 0.9887640449438202, 0.8585365853658536, 0.9943502824858756, 1.0], 'ipbad_machine-3-11': [0.8957055214723927, 0.8795180722891567, 0.8934010152284264, 0.888888888888889, 0.8902439024390244], 'ipbad_fpof_machine-3-11': [0.9723756906077349, 0.9723756906077349, 0.9723756906077349, 0.9723756906077349, 0.9723756906077349]}

for method in ['iso','ipbad_machine','ipbad_fpof']:
    mean_all_data = []
    for method_dataset, results in d.items():
        if method in method_dataset:
            mean_folds = np.mean(results)
            print(f'Method:{method_dataset}: {mean_folds}')
        mean_all_data.append(mean_folds)
    print(f'Method:{method} Mean all data: {np.mean(mean_all_data):.3f}. std:  {np.std(mean_all_data):.3f}')

'''            
Fold-0 Method:iso Mean all data: 0.9491745848196198
Fold-1 Method:iso Mean all data: 0.9495434793299411
Fold-2 Method:iso Mean all data: 0.9504182491454305
Fold-3 Method:iso Mean all data: 0.945464660389397
Fold-4 Method:iso Mean all data: 0.9443344171845361
Fold-1-5: Method:iso Mean folds: 0.948. std:  0.002
Fold-0 Method:ipbad Mean all data: 0.9670961162032897
Fold-1 Method:ipbad Mean all data: 0.9683370991624923
Fold-2 Method:ipbad Mean all data: 0.9659572291315668
Fold-3 Method:ipbad Mean all data: 0.9663820321936394
Fold-4 Method:ipbad Mean all data: 0.9713690292374536
Fold-1-5: Method:ipbad Mean folds: 0.968. std:  0.002
Fold-0 Method:ipbad-fpof Mean all data: 0.9586485476977109
Fold-1 Method:ipbad-fpof Mean all data: 0.9586485476977109
Fold-2 Method:ipbad-fpof Mean all data: 0.9586485476977109
Fold-3 Method:ipbad-fpof Mean all data: 0.9586485476977109
Fold-4 Method:ipbad-fpof Mean all data: 0.9586485476977109
Fold-1-5: Method:ipbad-fpof Mean folds: 0.959. std:  0.000
'''

#Results of OmnyAnomaly / SMD - Machine 1-1
#2 uur
'''
{'FN': 7.0,
'FP': 0.0,
'TN': 25686.0,
'TP': 2687.0,
'best-f1': 0.9986941228779496,
'best_valid_loss': -56.808830028289506,
'latency': 155.79688406231875,
'pot-FN': 0.0,
'pot-FP': 936.0,
'pot-TN': 24750.0,
'pot-TP': 2694.0,
'pot-f1': 0.8519875167320038,
'pot-latency': 3.74995312558593,
'pot-precision': 0.7421487582860915,
'pot-recall': 0.9999999962880475,
'pot-threshold': 13.370233542106783,
'precision': 0.9999999962783773,
'pred_time': 0.3057939662060267,
'pred_total_time': 173.8386149406433,
'recall': 0.9974016295567868,
'threshold': -399.0,
'train_time': 437.66284918785095,
'valid_time': 0.3041952294439525}
'''
#eval 2: optimising threshold, t: 0.9961, f1: 0.807910, precision: 1.0000, recall: 0.6777, TP: 143, FP: 0, FN:68, TN:192



#---- NASA --- 
do_MSL = False  #weird data....., i.e. all except first channel are 0/1
if do_MSL:
    #data_folder = '/Users/lfereman/git2/conscious/data/multivariate/SMAP/'
    data_folder = '/Users/lfereman/git2/conscious/data/multivariate/MSL/'
    import os
    files = os.listdir(data_folder) 
    datasets = [f[0:f.index('_train.csv')] for f in files if f.endswith('_train.csv')]
    print(len(datasets),datasets)
    results = defaultdict(list)
    for machine in datasets:
        #machine = 'A-1'
        machine_1_test = f'{machine}_test.csv'
        machine_1_train = f'{machine}_train.csv'
        machine_1_test_labels = f'{machine}_labels.csv'
        df = pd.read_csv(data_folder + machine_1_test) #i.e. contains metric-1,...,metric-38,time_id,time
        df['time'] = pd.to_datetime(df['time'])
        df_train = pd.read_csv(data_folder + machine_1_train) #i.e. contains metric-1,...,metric-38,time_id,time
        df_train['time'] = pd.to_datetime(df_train['time'])
        
        labels_df = pd.read_csv(data_folder + machine_1_test_labels) #i.e. contains chan_id,spacecraft,anomaly_sequences,class,num_values
        labels_df['anomaly_sequences'] = labels_df['anomaly_sequences'].apply(eval)
        anomaly_ranges = labels_df.iloc[0]['anomaly_sequences'] #i.e. [[550, 750], [2100, 2210]]
        anomalies = []
        for anomaly_range in anomaly_ranges:
            sample = df[(df['time_id'] >= anomaly_range[0]) & (df['time_id'] <= anomaly_range[0])]
            anomalies.extend([row['time'] for idx, row in sample.iterrows()])
        print(f'No anomalies: {len(anomalies)}. First: {anomalies[0]}')
        
        df, components = pre_process_mv_df(df)   
        
        try:
            run_hetero =True
            if run_hetero:
                preprocess_parameter = {'interval': 4, 'no_symbols': 30, 'no_bins': 30} #{'interval': 72, 'no_symbols': 4, 'no_bins': 20} #no_bins was 5
                preprocess_outlier=False
                
                for iter in range(0,5): 
                    #iso forest
                    an_scores = run_iso_forest_mv(df, interval=preprocess_parameter['interval'], stride=1, preprocess_outlier=preprocess_outlier)
                    an_score_second_half = []
                    for window, score in an_scores:
                        if window[0] > df_train['time'].max():
                            an_score_second_half.append((window,score))
                    (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_score_second_half, anomalies)
                    print(f"ISO F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
                    results[f'iso_{machine}'].append(f1)
                    
                    #ipbad
                    an_scores = run_ipbad_mv(df,  interval=preprocess_parameter['interval'], stride=1, no_symbols=preprocess_parameter['no_symbols'], no_bins=preprocess_parameter['no_bins'], 
                                                   preprocess_outlier=preprocess_outlier, use_MDL=True, binning_method='normal', use_iso=True)
                    an_score_second_half = []
                    for window, score in an_scores:
                        if window[0] > df_train['time'].max():
                            an_score_second_half.append((window,score))
                    (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_score_second_half, anomalies)
                    print(f"IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
                    results[f'ipbad_{machine}'].append(f1)
                    #an_scores = sorted(an_scores, key=lambda p: p[0][0], reverse=False)
                    #plot_mv_data(f'SMD-{machine}', df, anomalies, components, an_scores)  
                    
                    #ipbad-fpof
                    an_scores = run_ipbad_mv(df,  interval=preprocess_parameter['interval'], stride=1, no_symbols=preprocess_parameter['no_symbols'], no_bins=preprocess_parameter['no_bins'], 
                                                   preprocess_outlier=preprocess_outlier, use_MDL=True, binning_method='normal', use_iso=False)
                    #eval after time
                    an_score_second_half = []
                    for window, score in an_scores:
                        if window[0] > df_train['time'].max():
                            an_score_second_half.append((window,score))
                    (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_score_second_half, anomalies)
                    print(f"IPBAD-FPOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
                    results[f'ipbad_fpof_{machine}'].append(f1)
                for key, values in results.items():
                    print(f'method-dataset: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}. vals: {values}')  
        except Exception as inst:
            print(inst)
            print(f'error in {machine}')
    for key, values in results.items():
        print(f'method-dataset: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}. vals: {values}')  
    print(results)

print("--------")
#SMAP:
#d = {'iso_P-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_P-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_P-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-3': [0.35555555555555557, 0.3018867924528302, 0.32, 0.326530612244898, 0.5], 'ipbad_E-3': [0.5333333333333333, 0.5333333333333333, 0.5161290322580645, 0.5517241379310345, 0.5517241379310345], 'ipbad_fpof_E-3': [0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273], 'iso_A-1': [0.5161290322580645, 0.5517241379310345, 0.5517241379310345, 0.48484848484848486, 0.48484848484848486], 'ipbad_A-1': [0.6666666666666666, 0.8421052631578948, 0.7272727272727273, 0.7272727272727273, 0.761904761904762], 'ipbad_fpof_A-1': [0.5714285714285715, 0.5714285714285715, 0.5714285714285715, 0.5714285714285715, 0.5714285714285715], 'iso_D-13': [1.0, 0.9411764705882353, 1.0, 0.9411764705882353, 1.0], 'ipbad_D-13': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-13': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-7': [0.3902439024390244, 0.3720930232558139, 0.34782608695652173, 0.38095238095238093, 0.3902439024390244], 'ipbad_D-7': [0.6956521739130436, 0.888888888888889, 0.6956521739130436, 0.6399999999999999, 0.7272727272727273], 'ipbad_fpof_D-7': [0.6399999999999999, 0.6399999999999999, 0.6399999999999999, 0.6399999999999999, 0.6399999999999999], 'iso_E-5': [0.6399999999999999, 1.0, 1.0, 0.888888888888889, 0.9411764705882353], 'ipbad_E-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_R-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_R-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_R-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-13': [0.6, 0.6666666666666666, 0.56, 0.6086956521739131, 0.5925925925925926], 'ipbad_E-13': [0.6666666666666666, 0.7000000000000001, 0.711864406779661, 0.7241379310344828, 0.7636363636363637], 'ipbad_fpof_E-13': [0.64, 0.64, 0.64, 0.64, 0.64], 'iso_A-7': [0.26229508196721313, 0.6399999999999999, 0.25, 0.25, 0.33333333333333337], 'ipbad_A-7': [0.761904761904762, 0.6399999999999999, 0.8, 0.761904761904762, 0.8], 'ipbad_fpof_A-7': [0.33333333333333337, 0.33333333333333337, 0.33333333333333337, 0.33333333333333337, 0.33333333333333337], 'iso_P-2': [1.0, 0.9411764705882353, 1.0, 1.0, 1.0], 'ipbad_P-2': [0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948], 'ipbad_fpof_P-2': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'iso_G-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_G-6': [0.24615384615384614, 0.34782608695652173, 0.34782608695652173, 0.4, 0.34782608695652173], 'ipbad_fpof_G-6': [0.4324324324324324, 0.4324324324324324, 0.4324324324324324, 0.4324324324324324, 0.4324324324324324], 'iso_E-8': [1.0, 0.9473684210526316, 0.9473684210526316, 1.0, 1.0], 'ipbad_E-8': [0.9, 0.8571428571428571, 0.9, 0.8181818181818181, 0.8181818181818181], 'ipbad_fpof_E-8': [0.9473684210526316, 0.9473684210526316, 0.9473684210526316, 0.9473684210526316, 0.9473684210526316], 'iso_F-2': [0.23880597014925373, 0.3076923076923077, 0.23188405797101452, 0.3018867924528302, 0.20512820512820512], 'ipbad_F-2': [0.3018867924528302, 0.3076923076923077, 0.326530612244898, 0.3137254901960785, 0.3137254901960785], 'ipbad_fpof_F-2': [0.2758620689655173, 0.2758620689655173, 0.2758620689655173, 0.2758620689655173, 0.2758620689655173], 'iso_D-1': [0.9411764705882353, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948], 'ipbad_D-1': [0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273], 'ipbad_fpof_D-1': [0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948], 'iso_G-1': [0.21333333333333335, 0.29629629629629634, 0.3076923076923077, 0.25396825396825395, 0.29629629629629634], 'ipbad_G-1': [0.9411764705882353, 0.9411764705882353, 0.888888888888889, 0.9411764705882353, 0.9411764705882353], 'ipbad_fpof_G-1': [0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666], 'iso_D-12': [0.3018867924528302, 0.3018867924528302, 0.3018867924528302, 0.3018867924528302, 0.3018867924528302], 'ipbad_D-12': [0.3404255319148936, 0.5714285714285715, 0.3636363636363636, 0.35555555555555557, 0.5714285714285715], 'ipbad_fpof_D-12': [0.3018867924528302, 0.3018867924528302, 0.3018867924528302, 0.3018867924528302, 0.3018867924528302], 'iso_E-2': [1.0, 1.0, 0.888888888888889, 0.888888888888889, 1.0], 'ipbad_E-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-6': [0.9411764705882353, 0.8421052631578948, 0.9411764705882353, 0.9411764705882353, 0.7272727272727273], 'ipbad_D-6': [0.41025641025641024, 0.38095238095238093, 0.3720930232558139, 0.3902439024390244, 0.32], 'ipbad_fpof_D-6': [0.5333333333333333, 0.5333333333333333, 0.5333333333333333, 0.5333333333333333, 0.5333333333333333], 'iso_E-12': [0.761904761904762, 0.888888888888889, 0.8, 0.8648648648648648, 0.761904761904762], 'ipbad_E-12': [0.9411764705882353, 0.9696969696969697, 0.9696969696969697, 1.0, 0.8648648648648648], 'ipbad_fpof_E-12': [0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666], 'iso_A-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_A-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-4': [0.888888888888889, 0.888888888888889, 0.9411764705882353, 0.888888888888889, 0.9411764705882353], 'ipbad_E-4': [0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948], 'ipbad_fpof_E-4': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'iso_T-1': [0.5161290322580645, 0.5, 0.5, 0.5, 0.5161290322580645], 'ipbad_T-1': [0.5161290322580645, 0.5333333333333333, 0.6666666666666666, 0.5333333333333333, 0.5517241379310345], 'ipbad_fpof_T-1': [0.5, 0.5, 0.5, 0.5, 0.5], 'iso_P-3': [0.888888888888889, 0.9411764705882353, 1.0, 0.9411764705882353, 0.9411764705882353], 'ipbad_P-3': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.888888888888889], 'ipbad_fpof_P-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_G-7': [0.9795918367346939, 0.9795918367346939, 0.9795918367346939, 0.9795918367346939, 0.9795918367346939], 'ipbad_G-7': [0.923076923076923, 0.888888888888889, 0.888888888888889, 0.9056603773584906, 0.9056603773584906], 'ipbad_fpof_G-7': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'iso_B-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_B-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_B-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-9': [1.0, 1.0, 1.0, 0.9411764705882353, 0.9411764705882353], 'ipbad_E-9': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'ipbad_fpof_E-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_F-3': [1.0, 0.761904761904762, 0.8421052631578948, 0.9411764705882353, 1.0], 'ipbad_F-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_F-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-1': [0.8648648648648648, 0.9696969696969697, 0.8421052631578948, 0.7111111111111111, 0.9142857142857143], 'ipbad_E-1': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.888888888888889], 'ipbad_fpof_E-1': [0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666], 'iso_A-3': [0.48484848484848486, 0.4444444444444445, 0.6399999999999999, 0.8, 0.761904761904762], 'ipbad_A-3': [0.9411764705882353, 0.888888888888889, 0.9411764705882353, 0.9411764705882353, 0.888888888888889], 'ipbad_fpof_A-3': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'iso_D-11': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'ipbad_D-11': [0.6153846153846153, 0.6399999999999999, 0.6399999999999999, 0.6956521739130436, 0.6666666666666666], 'ipbad_fpof_D-11': [0.3137254901960785, 0.3137254901960785, 0.3137254901960785, 0.3137254901960785, 0.3137254901960785], 'iso_G-2': [0.9411764705882353, 0.761904761904762, 0.7272727272727273, 0.8421052631578948, 0.7272727272727273], 'ipbad_G-2': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'ipbad_fpof_G-2': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'iso_D-8': [1.0, 1.0, 1.0, 1.0, 0.9411764705882353], 'ipbad_D-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_S-1': [0.5161290322580645, 0.888888888888889, 0.4210526315789474, 0.6956521739130436, 0.6153846153846153], 'ipbad_S-1': [0.9411764705882353, 0.888888888888889, 1.0, 0.9411764705882353, 1.0], 'ipbad_fpof_S-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-5': [1.0, 0.8571428571428571, 0.8181818181818181, 1.0, 0.9], 'ipbad_D-5': [0.36, 0.45000000000000007, 0.4736842105263158, 0.45000000000000007, 0.43902439024390244], 'ipbad_fpof_D-5': [0.72, 0.72, 0.72, 0.72, 0.72], 'iso_G-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_G-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_G-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_T-2': [0.5806451612903226, 0.5454545454545454, 0.7499999999999999, 0.5294117647058824, 0.5806451612903226], 'ipbad_T-2': [0.5454545454545454, 0.6206896551724138, 0.5625, 0.5454545454545454, 0.6206896551724138], 'ipbad_fpof_T-2': [0.6923076923076924, 0.6923076923076924, 0.6923076923076924, 0.6923076923076924, 0.6923076923076924], 'iso_E-7': [0.9411764705882353, 0.888888888888889, 0.8421052631578948, 0.9411764705882353, 0.8421052631578948], 'ipbad_E-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_A-5': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'ipbad_fpof_A-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-11': [0.9696969696969697, 0.9411764705882353, 0.7272727272727273, 0.9696969696969697, 0.9411764705882353], 'ipbad_E-11': [1.0, 0.888888888888889, 0.9142857142857143, 0.8421052631578948, 0.9411764705882353], 'ipbad_fpof_E-11': [0.6808510638297872, 0.6808510638297872, 0.6808510638297872, 0.6808510638297872, 0.6808510638297872], 'iso_D-3': [0.23529411764705882, 0.21917808219178084, 0.23880597014925373, 0.23188405797101452, 0.2807017543859649], 'ipbad_D-3': [0.3137254901960785, 0.23529411764705882, 0.3137254901960785, 0.3018867924528302, 0.2424242424242424], 'ipbad_fpof_D-3': [0.3404255319148936, 0.3404255319148936, 0.3404255319148936, 0.3404255319148936, 0.3404255319148936], 'iso_A-8': [0.21333333333333335, 0.23188405797101452, 0.18604651162790695, 0.18604651162790695, 0.21333333333333335], 'ipbad_A-8': [0.12598425196850394, 0.12598425196850394, 0.12598425196850394, 0.12598425196850394, 0.12598425196850394], 'ipbad_fpof_A-8': [0.12598425196850394, 0.12598425196850394, 0.12598425196850394, 0.12598425196850394, 0.12598425196850394], 'iso_A-2': [0.2807017543859649, 0.29629629629629634, 0.35555555555555557, 0.4, 0.38095238095238093], 'ipbad_A-2': [0.8421052631578948, 0.9411764705882353, 0.9411764705882353, 1.0, 0.9411764705882353], 'ipbad_fpof_A-2': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'iso_G-3': [0.4615384615384615, 0.5, 0.45000000000000007, 0.6206896551724138, 0.7499999999999999], 'ipbad_G-3': [0.9, 0.8571428571428571, 0.8181818181818181, 0.9, 0.9], 'ipbad_fpof_G-3': [0.8571428571428571, 0.8571428571428571, 0.8571428571428571, 0.8571428571428571, 0.8571428571428571], 'iso_P-7': [0.4210526315789474, 0.4210526315789474, 0.41025641025641024, 0.41025641025641024, 0.41025641025641024], 'ipbad_P-7': [0.6153846153846153, 0.5714285714285715, 0.5714285714285715, 0.5714285714285715, 0.5925925925925926], 'ipbad_fpof_P-7': [0.35555555555555557, 0.35555555555555557, 0.35555555555555557, 0.35555555555555557, 0.35555555555555557], 'iso_D-9': [0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.5925925925925926], 'ipbad_D-9': [0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948, 0.8421052631578948], 'ipbad_fpof_D-9': [0.5925925925925926, 0.5925925925925926, 0.5925925925925926, 0.5925925925925926, 0.5925925925925926], 'iso_D-4': [0.2909090909090909, 0.2807017543859649, 0.2758620689655173, 0.2909090909090909, 0.2711864406779661], 'ipbad_D-4': [0.29629629629629634, 0.32, 0.2758620689655173, 0.326530612244898, 0.3137254901960785], 'ipbad_fpof_D-4': [0.34782608695652173, 0.34782608695652173, 0.34782608695652173, 0.34782608695652173, 0.34782608695652173], 'iso_T-3': [0.41025641025641024, 0.2857142857142857, 0.2666666666666667, 0.41025641025641024, 0.38095238095238093], 'ipbad_T-3': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9411764705882353], 'ipbad_fpof_T-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_P-1': [0.7111111111111111, 0.6666666666666666, 0.8205128205128205, 0.9142857142857143, 0.7272727272727273], 'ipbad_P-1': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'ipbad_fpof_P-1': [0.761904761904762, 0.761904761904762, 0.761904761904762, 0.761904761904762, 0.761904761904762], 'iso_A-4': [0.5517241379310345, 0.2711864406779661, 0.6956521739130436, 0.5161290322580645, 0.34782608695652173], 'ipbad_A-4': [0.761904761904762, 0.761904761904762, 0.8, 0.9411764705882353, 0.761904761904762], 'ipbad_fpof_A-4': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'iso_E-10': [0.7441860465116279, 0.8205128205128205, 1.0, 0.9142857142857143, 0.8648648648648648], 'ipbad_E-10': [0.9411764705882353, 0.9411764705882353, 0.9411764705882353, 0.9142857142857143, 0.9411764705882353], 'ipbad_fpof_E-10': [0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666], 'iso_E-6': [1.0, 0.888888888888889, 0.9411764705882353, 0.888888888888889, 0.9411764705882353], 'ipbad_E-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-2': [0.25396825396825395, 0.48484848484848486, 0.4, 0.4, 0.5], 'ipbad_D-2': [0.6399999999999999, 0.6956521739130436, 0.6399999999999999, 0.6153846153846153, 0.6399999999999999], 'ipbad_fpof_D-2': [0.41025641025641024, 0.41025641025641024, 0.41025641025641024, 0.41025641025641024, 0.41025641025641024], 'iso_A-9': [0.3636363636363636, 0.3636363636363636, 0.38095238095238093, 0.41025641025641024, 0.48484848484848486], 'ipbad_A-9': [0.1904761904761905, 0.1951219512195122, 0.1904761904761905, 0.18823529411764706, 0.18604651162790695], 'ipbad_fpof_A-9': [0.125, 0.125, 0.125, 0.125, 0.125], 'iso_F-1': [0.4444444444444445, 0.4210526315789474, 0.5517241379310345, 0.5517241379310345, 0.2758620689655173], 'ipbad_F-1': [0.761904761904762, 0.8, 0.8421052631578948, 0.761904761904762, 0.7272727272727273], 'ipbad_fpof_F-1': [0.3902439024390244, 0.3902439024390244, 0.3902439024390244, 0.3902439024390244, 0.3902439024390244]}

#MSL:
d = {'iso_M-7': [0, 0, 0, 0, 0], 'ipbad_M-7': [0, 0, 0, 0, 0], 'ipbad_fpof_M-7': [0, 0, 0, 0, 0], 'iso_C-2': [0.7272727272727273, 0.8, 0.8, 0.7272727272727273, 0.7272727272727273], 'ipbad_C-2': [0.8, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273], 'ipbad_fpof_C-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_F-4': [0.8333333333333333, 0.8333333333333333, 0.9090909090909091, 0.7142857142857143, 0.8333333333333333], 'ipbad_F-4': [0.9090909090909091, 0.9090909090909091, 0.9090909090909091, 0.9090909090909091, 0.9090909090909091], 'ipbad_fpof_F-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_P-14': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'ipbad_P-14': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_P-14': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'iso_M-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_M-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_M-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_S-2': [0, 0, 0, 0, 0], 'ipbad_S-2': [0, 0, 0, 0, 0], 'ipbad_fpof_S-2': [0, 0, 0, 0, 0], 'iso_F-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_F-5': [0.5333333333333333, 0.5, 0.5, 0.5333333333333333, 0.5], 'ipbad_fpof_F-5': [0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273], 'iso_P-10': [1.0, 1.0, 1.0, 0.888888888888889, 1.0], 'ipbad_P-10': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_P-10': [0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889, 0.888888888888889], 'iso_T-13': [0.888888888888889, 1.0, 0.8, 0.888888888888889, 0.7272727272727273], 'ipbad_T-13': [0.47058823529411764, 0.5333333333333333, 0.5333333333333333, 0.5333333333333333, 0.5], 'ipbad_fpof_T-13': [0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273, 0.7272727272727273], 'iso_M-5': [0, 0, 0, 0, 0], 'ipbad_M-5': [0, 0, 0, 0, 0], 'ipbad_fpof_M-5': [0, 0, 0, 0, 0], 'iso_T-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_T-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_T-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_T-12': [0, 0, 0, 0, 0], 'ipbad_T-12': [0, 0, 0, 0, 0], 'ipbad_fpof_T-12': [0, 0, 0, 0, 0], 'iso_F-7': [0.56, 0.6666666666666666, 0.6666666666666666, 0.6666666666666666, 0.6153846153846153], 'ipbad_F-7': [0.7000000000000001, 0.7000000000000001, 0.7777777777777778, 0.7000000000000001, 0.7000000000000001], 'ipbad_fpof_F-7': [0.4117647058823529, 0.4117647058823529, 0.4117647058823529, 0.4117647058823529, 0.4117647058823529], 'iso_T-8': [0.9090909090909091, 0.8333333333333333, 0.9090909090909091, 1.0, 1.0], 'ipbad_T-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_T-8': [0.7692307692307693, 0.7692307692307693, 0.7692307692307693, 0.7692307692307693, 0.7692307692307693], 'iso_D-16': [0, 0, 0, 0, 0], 'ipbad_D-16': [0, 0, 0, 0, 0], 'ipbad_fpof_D-16': [0, 0, 0, 0, 0]}

for key, values in d.items():
    print(f'method-dataset: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}. vals: {values}')  
    
#results = {'iso_P-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_P-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_P-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-3': [0.972972972972973, 0.9642857142857143, 0.9818181818181818, 0.9642857142857143, 0.9818181818181818], 'ipbad_E-3': [0.9908256880733944, 0.9908256880733944, 0.9818181818181818, 0.9908256880733944, 0.9908256880733944], 'ipbad_fpof_E-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-1': [1.0, 1.0, 0.9923664122137404, 0.9923664122137404, 1.0], 'ipbad_A-1': [0.9848484848484849, 0.9848484848484849, 0.9848484848484849, 0.9848484848484849, 0.9848484848484849], 'ipbad_fpof_A-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-13': [0.9772727272727273, 0.9662921348314606, 0.9662921348314606, 0.9662921348314606, 0.9662921348314606], 'ipbad_D-13': [0.9555555555555556, 0.9555555555555556, 0.9555555555555556, 0.9555555555555556, 0.9555555555555556], 'ipbad_fpof_D-13': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-5': [0.9473684210526316, 0.9574468085106383, 0.9090909090909091, 0.9782608695652174, 0.9375], 'ipbad_E-5': [1.0, 1.0, 0.989010989010989, 1.0, 1.0], 'ipbad_fpof_E-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_R-1': [0.9574468085106383, 0.9574468085106383, 0.9574468085106383, 0.9574468085106383, 0.9574468085106383], 'ipbad_R-1': [0.9574468085106383, 0.9574468085106383, 0.9574468085106383, 0.9574468085106383, 0.9574468085106383], 'ipbad_fpof_R-1': [0.989010989010989, 0.989010989010989, 0.989010989010989, 0.989010989010989, 0.989010989010989], 'iso_E-13': [0.9909909909909909, 0.9649122807017544, 0.9821428571428572, 0.9909909909909909, 0.9821428571428572], 'ipbad_E-13': [0.9909909909909909, 0.9909909909909909, 0.9821428571428572, 0.9821428571428572, 0.9821428571428572], 'ipbad_fpof_E-13': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-7': [0.7766990291262136, 0.7476635514018691, 0.7547169811320755, 0.7843137254901961, 0.8080808080808081], 'ipbad_A-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_P-2': [0.9896907216494846, 0.9795918367346939, 1.0, 0.9896907216494846, 1.0], 'ipbad_P-2': [0.9896907216494846, 1.0, 1.0, 0.9896907216494846, 0.9896907216494846], 'ipbad_fpof_P-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_G-6': [0.99009900990099, 0.99009900990099, 1.0, 1.0, 0.99009900990099], 'ipbad_G-6': [0.9345794392523363, 0.9433962264150945, 0.9433962264150945, 0.9433962264150945, 0.9433962264150945], 'ipbad_fpof_G-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-8': [0.9814814814814815, 0.9464285714285715, 0.9464285714285715, 0.9906542056074767, 0.9906542056074767], 'ipbad_E-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-8': [0.9814814814814815, 0.9814814814814815, 0.9814814814814815, 0.9814814814814815, 0.9814814814814815], 'iso_F-2': [0.98, 1.0, 1.0, 1.0, 1.0], 'ipbad_F-2': [0.98, 0.98, 0.98, 0.98, 0.98], 'ipbad_fpof_F-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-1': [0.9642857142857143, 0.9818181818181818, 0.9818181818181818, 0.9557522123893806, 0.972972972972973], 'ipbad_D-1': [0.9908256880733944, 1.0, 0.9908256880733944, 0.9908256880733944, 1.0], 'ipbad_fpof_D-1': [0.9908256880733944, 0.9908256880733944, 0.9908256880733944, 0.9908256880733944, 0.9908256880733944], 'iso_G-1': [1.0, 1.0, 0.9919999999999999, 0.9919999999999999, 1.0], 'ipbad_G-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_G-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-12': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-12': [0.967741935483871, 0.967741935483871, 0.967741935483871, 0.967741935483871, 0.967741935483871], 'ipbad_fpof_D-12': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-2': [0.98, 0.9702970297029703, 0.9423076923076923, 0.9607843137254902, 0.9514563106796117], 'ipbad_E-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-6': [0.99009900990099, 1.0, 0.970873786407767, 0.99009900990099, 1.0], 'ipbad_D-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-12': [0.9354838709677419, 0.9206349206349206, 0.9914529914529915, 0.9354838709677419, 0.9747899159663865], 'ipbad_E-12': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-12': [0.9914529914529915, 0.9914529914529915, 0.9914529914529915, 0.9914529914529915, 0.9914529914529915], 'iso_A-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_A-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-4': [0.9607843137254902, 0.98989898989899, 0.9423076923076923, 0.9607843137254902, 0.9514563106796117], 'ipbad_E-4': [1.0, 1.0, 1.0, 0.98989898989899, 1.0], 'ipbad_fpof_E-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_T-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_T-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_T-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_P-3': [0.9357798165137614, 0.9532710280373832, 0.9807692307692307, 0.9444444444444444, 1.0], 'ipbad_P-3': [0.9902912621359222, 0.9902912621359222, 0.9902912621359222, 0.9902912621359222, 0.9902912621359222], 'ipbad_fpof_P-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_G-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_G-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_G-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_B-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_B-1': [0.9433962264150945, 0.9433962264150945, 0.9433962264150945, 0.9433962264150945, 0.9433962264150945], 'ipbad_fpof_B-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-9': [0.9484536082474228, 0.9787234042553191, 1.0, 0.9583333333333334, 0.989247311827957], 'ipbad_E-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_F-3': [0.9019607843137255, 0.9583333333333334, 0.968421052631579, 0.9019607843137255, 0.9583333333333334], 'ipbad_F-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_F-3': [0.989247311827957, 0.989247311827957, 0.989247311827957, 0.989247311827957, 0.989247311827957], 'iso_E-1': [0.9586776859504132, 0.983050847457627, 0.9666666666666666, 0.9666666666666666, 0.9914529914529915], 'ipbad_E-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-1': [0.9914529914529915, 0.9914529914529915, 0.9914529914529915, 0.9914529914529915, 0.9914529914529915], 'iso_A-3': [1.0, 0.9917355371900827, 1.0, 1.0, 1.0], 'ipbad_A-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-3': [0.9836065573770492, 0.9836065573770492, 0.9836065573770492, 0.9836065573770492, 0.9836065573770492], 'iso_D-11': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-11': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-11': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_G-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_G-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_G-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-8': [0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621], 'ipbad_D-8': [0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621], 'ipbad_fpof_D-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_S-1': [0.9855072463768115, 0.9714285714285714, 0.9444444444444444, 0.9444444444444444, 0.9577464788732395], 'ipbad_S-1': [1.0, 1.0, 0.9855072463768115, 1.0, 1.0], 'ipbad_fpof_S-1': [0.9444444444444444, 0.9444444444444444, 0.9444444444444444, 0.9444444444444444, 0.9444444444444444], 'iso_D-5': [1.0, 1.0, 0.9896907216494846, 1.0, 0.9896907216494846], 'ipbad_D-5': [0.9696969696969697, 0.9696969696969697, 0.9696969696969697, 0.9696969696969697, 0.9696969696969697], 'ipbad_fpof_D-5': [0.9696969696969697, 0.9696969696969697, 0.9696969696969697, 0.9696969696969697, 0.9696969696969697], 'iso_G-4': [0.98], 'ipbad_G-4': [0.9333333333333333], 'iso_T-2': [0.9523809523809523, 0.967741935483871, 0.9836065573770492, 0.9836065573770492, 0.967741935483871], 'ipbad_T-2': [0.967741935483871, 0.967741935483871, 0.967741935483871, 0.967741935483871, 0.967741935483871], 'ipbad_fpof_T-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-7': [0.8990825688073394, 0.9514563106796117, 0.9607843137254902, 0.9514563106796117, 0.98], 'ipbad_E-7': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-7': [0.9514563106796117, 0.9514563106796117, 0.9514563106796117, 0.9514563106796117, 0.9514563106796117], 'iso_A-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_A-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-5': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-11': [0.983050847457627, 0.9666666666666666, 0.9586776859504132, 0.9747899159663865, 0.9666666666666666], 'ipbad_E-11': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-11': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-3': [0.9911504424778761, 0.9911504424778761, 0.9911504424778761, 0.9911504424778761, 0.9911504424778761], 'ipbad_fpof_D-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-8': [0.9767441860465117, 0.9767441860465117, 0.9843749999999999, 0.9843749999999999, 0.9843749999999999], 'ipbad_A-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-8': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-2': [0.9913043478260869, 0.9913043478260869, 0.9913043478260869, 1.0, 1.0], 'ipbad_A-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_G-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_G-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_G-3': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_P-7': [0.9904761904761905, 0.9719626168224299, 0.9811320754716981, 0.9904761904761905, 0.9811320754716981], 'ipbad_P-7': [0.9904761904761905, 0.9904761904761905, 0.9904761904761905, 0.9904761904761905, 0.9904761904761905], 'ipbad_fpof_P-7': [0.9811320754716981, 0.9811320754716981, 0.9811320754716981, 0.9811320754716981, 0.9811320754716981], 'iso_D-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-9': [0.5352112676056338, 0.5352112676056338, 0.5352112676056338, 0.5352112676056338, 0.5352112676056338], 'ipbad_fpof_D-9': [0.5588235294117647, 0.5588235294117647, 0.5588235294117647, 0.5588235294117647, 0.5588235294117647], 'iso_D-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_T-3': [1.0], 'ipbad_T-3': [1.0], 'iso_P-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_P-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_P-1': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_A-4': [0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621, 0.9915966386554621], 'ipbad_fpof_A-4': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-10': [0.983050847457627, 0.9747899159663865, 0.9747899159663865, 0.983050847457627, 0.9508196721311475], 'ipbad_E-10': [0.9914529914529915, 0.9914529914529915, 0.9914529914529915, 0.9914529914529915, 0.9914529914529915], 'ipbad_fpof_E-10': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_E-6': [0.9375, 1.0, 0.967741935483871, 1.0, 0.967741935483871], 'ipbad_E-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_E-6': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_D-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_D-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_D-2': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_A-9': [0.9922480620155039, 0.9846153846153847, 0.9922480620155039, 0.9922480620155039, 0.9922480620155039], 'ipbad_A-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'ipbad_fpof_A-9': [1.0, 1.0, 1.0, 1.0, 1.0], 'iso_F-1': [0.8571428571428571, 0.8571428571428571, 0.8571428571428571, 0.878048780487805, 0.8571428571428571], 'ipbad_F-1': [0.8571428571428571, 0.8571428571428571, 0.8571428571428571, 0.8571428571428571, 0.8571428571428571], 'ipbad_fpof_F-1': [0.9818181818181818, 0.9818181818181818, 0.9818181818181818, 0.9818181818181818, 0.9818181818181818]}
for method in ['iso','ipbad_','ipbad_fpof']:
    mean_all_data = []
    for method_dataset, results in d.items():
        if method == 'ipbad_' and 'ipbad_' in method_dataset and not ('fpof' in method_dataset):
            mean_folds = np.mean(results)
            mean_all_data.append(mean_folds)
            #print(f'Method:{method_dataset}: {mean_folds}')
        elif method != 'ipbad_' and method in method_dataset:
            mean_folds = np.mean(results)
            mean_all_data.append(mean_folds)
    print(f'Method:{method} Datasets: {len(mean_all_data)} Mean all data: {np.mean(mean_all_data):.3f}. std:  {np.std(mean_all_data):.3f}')

'''
Method:iso Datasets: 54 Mean all data: 0.708. std:  0.272
Method:ipbad_ Datasets: 54 Mean all data: 0.775. std:  0.245
Method:ipbad_fpof Datasets: 54 Mean all data: 0.733. std:  0.270
'''
