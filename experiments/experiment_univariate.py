import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.dates as mdates
import json 
import os
from tqdm import tqdm
from collections import defaultdict

from utils import load_numenta_data, load_colruyt_data, cart_product
from baselines.fpof import run_pfof
from baselines.pbad import run_pbad
from baselines.iso_forest import run_iso_forest
from ipbad.main import run_ipbad
from eval_methods import eval_best_f1
from visualisation import plot_data, plot_discrete
from scipy.constants.constants import point


np.set_printoptions(suppress=True, precision=3)
  

def plot_anomalies(label_dataset, df, anomalies, an_scores, segments_discretised,  windows, interval, stride, no_symbols, no_bins, t):
    fig, axs = plt.subplots(3, figsize=(20,6), sharex=True)
    plot_data(axs[0], df, anomalies)
    plot_discrete(axs[1], segments_discretised, windows, interval, stride, no_symbols, no_bins)
    axs[0].text(windows[0][0], 0, label_dataset, fontsize=12)
    an_scores_time = [window[0] + np.timedelta64((window[1]-window[0]).seconds,'s') for window, score in an_scores]
    an_scores_val = [score for window, score in an_scores]
    axs[2].plot(an_scores_time, an_scores_val, color='red',alpha=0.5)
    axs[2].axhline(y=t, color='b', linestyle='--')
    plt.savefig(f'{label_dataset}.png')
    
#TODO: Implemented PBAD + FPOF + Search for bugs? 
 
# ----- NUMENTA -----
data_folder = '/Users/lfereman/git2/NAB/'
taxi = data_folder + '/data/realKnownCause/nyc_taxi.csv'
ambient = data_folder + '/data/realKnownCause/ambient_temperature_system_failure.csv'
latency = data_folder + '/data/realKnownCause/ec2_request_latency_system_failure.csv'
labels = data_folder + '/labels/combined_labels.json'


run_latency_grid = False
run_latency = False       
run_taxi_grid = False
run_taxi = False
run_temp_grid = False
run_temp = False
run_water_grid = False
run_water = True


if run_latency_grid:
    df, anomalies = load_numenta_data(latency, labels)
    
    results_fpof = []
    results_ipbad = []
    
    grid = cart_product(interval=[24,12,6,3], no_symbols=[5,10], no_bins=[5,10,25,50,100])    
    for preprocess_parameter in grid:
        #pfof
        try:
            an_scores1, ts_i, segments_discretised, windows = run_pfof(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                       preprocess_outlier=False, relative_minsup=0.01)
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores1, anomalies, point_adjust=False) 
            print(f'preprocessing: {preprocess_parameter}')
            print(f">>>>>>RUN_PFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results_fpof.append((f1, preprocess_parameter))
            ###FPOF
            '''
(0.6533333333333333, {'interval': 24, 'no_symbols': 10, 'no_bins': 100})
(0.5730994152046783, {'interval': 24, 'no_symbols': 5, 'no_bins': 100})
(0.5617977528089887, {'interval': 12, 'no_symbols': 10, 'no_bins': 100})
(0.42608695652173906, {'interval': 24, 'no_symbols': 10, 'no_bins': 50})
(0.29239766081871343, {'interval': 12, 'no_symbols': 5, 'no_bins': 100})
(0.29069767441860467, {'interval': 24, 'no_symbols': 5, 'no_bins': 50})
(0.26, {'interval': 6, 'no_symbols': 10, 'no_bins': 100})
(0.25490196078431376, {'interval': 6, 'no_symbols': 5, 'no_bins': 100})
(0.22522522522522526, {'interval': 12, 'no_symbols': 10, 'no_bins': 50})
(0.19083969465648853, {'interval': 12, 'no_symbols': 5, 'no_bins': 50})
(0.1272727272727273, {'interval': 6, 'no_symbols': 10, 'no_bins': 50})
(0.12499999999999999, {'interval': 3, 'no_symbols': 10, 'no_bins': 100})
(0.12264150943396228, {'interval': 6, 'no_symbols': 10, 'no_bins': 10})
(0.11594202898550726, {'interval': 3, 'no_symbols': 5, 'no_bins': 100})
(0.09811320754716982, {'interval': 6, 'no_symbols': 5, 'no_bins': 50})
(0.08115942028985507, {'interval': 6, 'no_symbols': 5, 'no_bins': 25})
(0.06477732793522266, {'interval': 3, 'no_symbols': 10, 'no_bins': 50})
(0.050955414012738856, {'interval': 3, 'no_symbols': 5, 'no_bins': 50})
'''
        except Exception as e:
            print(e)
        #plot_anomalies(f'latency-pfof-f1:{f1:.3f}', df, anomalies, an_scores1, segments_discretised,  windows, 12, 1, 10, 100, t)
        
        
        print('>>>ipbad')
        #ipad variants
        for parameter in [{"use_iso":True, "use_MDL":True},
                          {"use_iso":True, "use_MDL":False},
                          {"use_iso":False, "use_MDL":True},
                          {"use_iso":False, "use_MDL":False}]:
            try:
                an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                   no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                   preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                   use_MDL=parameter['use_MDL'], 
                                                                                   use_iso=parameter['use_iso'])
                print(an_scores2)
                (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
                print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
                print(f">>>>>>RUN_PPAB F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
                results_ipbad.append((f1, preprocess_parameter,parameter))
                #plot_anomalies(f'latency-ipab-{str(parameter)}-f1:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 12, 1, 10, 100, t)
                #debug_eval_html(an_scores2, anomalies, t, ofile_test=f'latency-ipab-{str(parameter)}-f1:{f1:.3f}_debug.html')
                ###IPBAD
                '''
(0.962962962962963, {'interval': 6, 'no_symbols': 10, 'no_bins': 50}, {'use_iso': True, 'use_MDL': False})
(0.9433962264150944, {'interval': 12, 'no_symbols': 5, 'no_bins': 25}, {'use_iso': True, 'use_MDL': True})
(0.9423076923076924, {'interval': 24, 'no_symbols': 5, 'no_bins': 100}, {'use_iso': True, 'use_MDL': False})
(0.9333333333333333, {'interval': 24, 'no_symbols': 5, 'no_bins': 50}, {'use_iso': True, 'use_MDL': False})
(0.9333333333333333, {'interval': 3, 'no_symbols': 5, 'no_bins': 50}, {'use_iso': True, 'use_MDL': False})
(0.9245283018867924, {'interval': 24, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': True, 'use_MDL': True})
(0.9245283018867924, {'interval': 24, 'no_symbols': 10, 'no_bins': 50}, {'use_iso': True, 'use_MDL': False})
(0.9245283018867924, {'interval': 24, 'no_symbols': 5, 'no_bins': 100}, {'use_iso': True, 'use_MDL': True})
(0.9090909090909091, {'interval': 12, 'no_symbols': 5, 'no_bins': 50}, {'use_iso': True, 'use_MDL': True})
(0.9074074074074074, {'interval': 24, 'no_symbols': 10, 'no_bins': 10}, {'use_iso': True, 'use_MDL': False})
(0.896551724137931, {'interval': 6, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': True, 'use_MDL': False})
(0.8928571428571429, {'interval': 12, 'no_symbols': 5, 'no_bins': 25}, {'use_iso': True, 'use_MDL': False})
(0.8672566371681417, {'interval': 24, 'no_symbols': 10, 'no_bins': 50}, {'use_iso': True, 'use_MDL': True})
(0.8672566371681417, {'interval': 24, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': True, 'use_MDL': True})
(0.8672566371681417, {'interval': 24, 'no_symbols': 10, 'no_bins': 10}, {'use_iso': True, 'use_MDL': True})
(0.8521739130434782, {'interval': 24, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': True, 'use_MDL': False})
(0.8448275862068965, {'interval': 24, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': False, 'use_MDL': True})
(0.8448275862068965, {'interval': 24, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': False, 'use_MDL': False})
(0.8448275862068965, {'interval': 24, 'no_symbols': 5, 'no_bins': 50}, {'use_iso': True, 'use_MDL': True})
(0.819672131147541, {'interval': 12, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': True, 'use_MDL': False})
(0.8064516129032258, {'interval': 12, 'no_symbols': 5, 'no_bins': 50}, {'use_iso': True, 'use_MDL': False})
(0.8032786885245902, {'interval': 24, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': True, 'use_MDL': False})
(0.7903225806451613, {'interval': 24, 'no_symbols': 5, 'no_bins': 10}, {'use_iso': True, 'use_MDL': True})
(0.784, {'interval': 24, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': False, 'use_MDL': True})
(0.7462686567164178, {'interval': 12, 'no_symbols': 5, 'no_bins': 100}, {'use_iso': True, 'use_MDL': False})
(0.7462686567164178, {'interval': 12, 'no_symbols': 5, 'no_bins': 25}, {'use_iso': False, 'use_MDL': False})
(0.7259259259259259, {'interval': 24, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': False, 'use_MDL': False})
(0.7142857142857143, {'interval': 12, 'no_symbols': 5, 'no_bins': 25}, {'use_iso': False, 'use_MDL': True})
(0.7042253521126761, {'interval': 12, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': False, 'use_MDL': True})
(0.7027027027027025, {'interval': 6, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': False, 'use_MDL': False})
(0.6853146853146853, {'interval': 24, 'no_symbols': 5, 'no_bins': 50}, {'use_iso': False, 'use_MDL': True})
(0.684931506849315, {'interval': 12, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': False, 'use_MDL': False})
(0.6842105263157894, {'interval': 6, 'no_symbols': 5, 'no_bins': 25}, {'use_iso': False, 'use_MDL': False})
(0.6805555555555556, {'interval': 24, 'no_symbols': 5, 'no_bins': 10}, {'use_iso': True, 'use_MDL': False})
(0.6756756756756757, {'interval': 12, 'no_symbols': 10, 'no_bins': 25}, {'use_iso': True, 'use_MDL': False})
(0.6666666666666666, {'interval': 6, 'no_symbols': 10, 'no_bins': 10}, {'use_iso': True, 'use_MDL': False})
(0.6666666666666666, {'interval': 6, 'no_symbols': 10, 'no_bins': 5}, {'use_iso': True, 'use_MDL': True})
(0.6666666666666666, {'interval': 6, 'no_symbols': 10, 'no_bins': 5}, {'use_iso': True, 'use_MDL': False})
(0.6666666666666666, {'interval': 6, 'no_symbols': 10, 'no_bins': 5}, {'use_iso': False, 'use_MDL': True})
'''

            except Exception as e:
                print(e)
    
    print("###FPOF")
    results_fpof.sort(key=lambda t: t[0], reverse=True)
    for res in results_fpof:
        print(res)
    print("###IPBAD")
    results_ipbad.sort(key=lambda t: t[0], reverse=True)
    for res in results_ipbad:
        print(res)
        
        
if run_latency:
   #repeat 10 times because of non-determinism iso forest 
   results = defaultdict(list)
   for k in range(0,5):
       df, anomalies = load_numenta_data(latency, labels)
       preprocess_parameter = {"interval": 24, "no_symbols": 10, "no_bins": 100} #see grid search
       
       #pfof
       an_scores1, ts_i, segments_discretised, windows = run_pfof(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                           no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                           preprocess_outlier=False, relative_minsup=0.01)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores1, anomalies) 
       results['fpof'].append(f1)
       print(f'preprocessing: {preprocess_parameter}')
       print(f">>>>>>RUN_PFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       plot_anomalies(f'latency-pfof:{f1:.3f}', df, anomalies, an_scores1, segments_discretised,  windows, 
                      interval=preprocess_parameter["interval"], stride=1, 
                      no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       
       #iso forest
       an_scores, ts_i, segments, windows =  run_iso_forest(df, 
                                                            interval=preprocess_parameter["interval"], stride=1, 
                                                            preprocess_outlier=False)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
       print(f">>>>>>Run ISO forest F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['iso'].append(f1) 
        
       #pbad
       an_scores, ts_i, segments_discretised, windows = run_pbad(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                 no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"],
                                                                 preprocess_outlier=False, relative_minsup=0.01)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
       print(f">>>>>>RUN_PBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['pbad'].append(f1) 
       
       #ipbad
       parameter = {"use_iso":True, "use_MDL":True}
       an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                       preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                       use_MDL=parameter['use_MDL'], 
                                                                                       use_iso=parameter['use_iso'])
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
       print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
       print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['ipbad'].append(f1) 
       #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
       #               interval=preprocess_parameter["interval"], stride=1, 
       #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       
       #ipbad comprehesnive
       parameter = {'use_iso': False, 'use_MDL': True}
       an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                       preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                       use_MDL=parameter['use_MDL'], 
                                                                                       use_iso=parameter['use_iso'])
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
       print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
       print(f">>>>>>RUN_IPBAD-FPFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
       #               interval=preprocess_parameter["interval"], stride=1, 
       #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       results['ipbad_fpof'].append(f1) 
    
   for key, values in results.items():
       print(f'method: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}')
   '''
final 14/05
method: fpof, mean f1: 0.653. std f1:  0.000
method: iso, mean f1: 0.827. std f1:  0.077
method: pbad, mean f1: 0.864. std f1:  0.034
method: ipbad, mean f1: 0.901. std f1:  0.039
method: ipbad_fpof, mean f1: 0.845. std f1:  0.000
'''
if run_taxi_grid:
    df, anomalies = load_numenta_data(taxi, labels)
    results_ipbad = []
    grid = cart_product(interval=[72,24,12,6,3], no_symbols=[5,10], no_bins=[5,10,25,50,100])   
    for preprocess_parameter in grid:
        
        try:
            parameter = {"use_iso":True, "use_MDL":True}
            an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                   no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                   preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                   use_MDL=parameter['use_MDL'], use_iso=parameter['use_iso'])
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies, point_adjust=False)
            print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
            print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results_ipbad.append((f1, preprocess_parameter,parameter))
        except Exception as e:
            print(e)
    print("###IPBAD")
    results_ipbad.sort(key=lambda t: t[0], reverse=True)
    for res in results_ipbad:
        print(res)
    '''
    (0.4212454212454212, {'interval': 72, 'no_symbols': 5, 'no_bins': 5}, {'use_iso': True, 'use_MDL': True})
    (0.1649269311064718, {'interval': 72, 'no_symbols': 5, 'no_bins': 10}, {'use_iso': True, 'use_MDL': True})
    (0.1373510861948143, {'interval': 72, 'no_symbols': 10, 'no_bins': 5}, {'use_iso': True, 'use_MDL': True})
    '''

if run_taxi:
   df, anomalies = load_numenta_data(taxi, labels)
   #repeat 10 times because of non-determinism iso forest 
   results = defaultdict(list)
   for k in range(0,5):
       df, anomalies = load_numenta_data(taxi, labels)
       preprocess_parameter = {"interval": 72, "no_symbols": 5, "no_bins": 5} #see grid search
       preprocess_outlier=False
       #pfof
       #FOR TAXI: minsup is 0.001
       an_scores1, ts_i, segments_discretised, windows = run_pfof(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                           no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                           preprocess_outlier=preprocess_outlier, relative_minsup=0.01)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores1, anomalies) 
       results['fpof'].append(f1)
       print(f'preprocessing: {preprocess_parameter}')
       print(f">>>>>>RUN_PFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       plot_anomalies(f'latency-pfof:{f1:.3f}', df, anomalies, an_scores1, segments_discretised,  windows, 
                      interval=preprocess_parameter["interval"], stride=1, 
                      no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       
       #iso forest
       an_scores, ts_i, segments, windows =  run_iso_forest(df, 
                                                            interval=preprocess_parameter["interval"], stride=1, 
                                                            preprocess_outlier=preprocess_outlier)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
       print(f">>>>>>Run ISO forest F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['iso'].append(f1) 
        
       #pbad
       an_scores, ts_i, segments_discretised, windows = run_pbad(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                 no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"],
                                                                 preprocess_outlier=preprocess_outlier, relative_minsup=0.01)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
       print(f">>>>>>RUN_PBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['pbad'].append(f1) 
       
       #ipbad
       parameter = {"use_iso":True, "use_MDL":True}
       an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                       preprocess_outlier=preprocess_outlier, binning_method='normal',verbose=True,
                                                                                       use_MDL=parameter['use_MDL'], 
                                                                                       use_iso=parameter['use_iso'])
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
       print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
       print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['ipbad'].append(f1) 
       #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
       #               interval=preprocess_parameter["interval"], stride=1, 
       #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       
       #ipbad comprehesnive
       parameter = {'use_iso': False, 'use_MDL': True}
       an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                       preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                       use_MDL=parameter['use_MDL'], 
                                                                                       use_iso=parameter['use_iso'])
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
       print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
       print(f">>>>>>RUN_IPBAD-FPFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
       #               interval=preprocess_parameter["interval"], stride=1, 
       #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       results['ipbad_fpof'].append(f1) 
    
   for key, values in results.items():
       print(f'method: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}')    
   '''
14/05:
method: fpof, mean f1: 0.718. std f1:  0.000
method: iso, mean f1: 0.782. std f1:  0.033
method: pbad, mean f1: 0.807. std f1:  0.037
method: ipbad, mean f1: 0.851. std f1:  0.004
method: ipbad_fpof, mean f1: 0.630. std f1:  0.000

'''



if run_temp_grid:
    df, anomalies = load_numenta_data(ambient, labels)
    results_ipbad = []
    grid = cart_product(interval=[72,24,12,6,3], no_symbols=[5,10], no_bins=[5,10,25,50,100])   
    for preprocess_parameter in grid:
        
        try:
            parameter = {"use_iso":True, "use_MDL":True}
            an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                   no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                   preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                   use_MDL=parameter['use_MDL'], use_iso=parameter['use_iso'])
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
            print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
            print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results_ipbad.append((f1, preprocess_parameter,parameter))
        except Exception as e:
            print(e)
    print("###IPBAD")
    results_ipbad.sort(key=lambda t: t[0], reverse=True)
    for res in results_ipbad:
        print(res)
    ''''###IPBAD
(0.5816733067729084, {'interval': 72, 'no_symbols': 5, 'no_bins': 10}, {'use_iso': True, 'use_MDL': True})
(0.512280701754386, {'interval': 72, 'no_symbols': 10, 'no_bins': 10}, {'use_iso': True, 'use_MDL': True})
(0.25840707964601767, {'interval': 72, 'no_symbols': 5, 'no_bins': 5}, {'use_iso': True, 'use_MDL': True})
(0.10799136069114472, {'interval': 24, 'no_symbols': 10, 'no_bins': 5}, {'use_iso': True, 'use_MDL': True})

'''

if run_temp:
   df, anomalies = load_numenta_data(ambient, labels)
   results = defaultdict(list)
   for k in range(0,5):
       df, anomalies = load_numenta_data(latency, labels)
       preprocess_parameter = {"interval": 72, "no_symbols": 10, "no_bins": 10} #see grid search
       preprocess_outlier=True
       #pfof
       #FOR TAXI: minsup is 0.001
       an_scores1, ts_i, segments_discretised, windows = run_pfof(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                           no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                           preprocess_outlier=preprocess_outlier, relative_minsup=0.01)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores1, anomalies) 
       results['fpof'].append(f1)
       print(f'preprocessing: {preprocess_parameter}')
       print(f">>>>>>RUN_PFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       plot_anomalies(f'latency-pfof:{f1:.3f}', df, anomalies, an_scores1, segments_discretised,  windows, 
                      interval=preprocess_parameter["interval"], stride=1, 
                      no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       
       #iso forest
       an_scores, ts_i, segments, windows =  run_iso_forest(df, 
                                                            interval=preprocess_parameter["interval"], stride=1, 
                                                            preprocess_outlier=preprocess_outlier)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
       print(f">>>>>>Run ISO forest F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['iso'].append(f1) 
        
       #pbad
       an_scores, ts_i, segments_discretised, windows = run_pbad(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                 no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"],
                                                                 preprocess_outlier=preprocess_outlier, relative_minsup=0.01)
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
       print(f">>>>>>RUN_PBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['pbad'].append(f1) 
       
       #ipbad
       parameter = {"use_iso":True, "use_MDL":True}
       an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                       preprocess_outlier=preprocess_outlier, binning_method='normal',verbose=True,
                                                                                       use_MDL=parameter['use_MDL'], 
                                                                                       use_iso=parameter['use_iso'])
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
       print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
       print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       results['ipbad'].append(f1) 
       #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
       #               interval=preprocess_parameter["interval"], stride=1, 
       #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       
       #ipbad comprehesnive
       parameter = {'use_iso': False, 'use_MDL': True}
       an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                       no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                       preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                       use_MDL=parameter['use_MDL'], 
                                                                                       use_iso=parameter['use_iso'])
       (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
       print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
       print(f">>>>>>RUN_IPBAD-FPFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
       #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
       #               interval=preprocess_parameter["interval"], stride=1, 
       #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
       results['ipbad_fpof'].append(f1) 
    
   for key, values in results.items():
       print(f'method: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}')    
   '''14/05
method: fpof, mean f1: 0.764. std f1:  0.000
method: iso, mean f1: 0.943. std f1:  0.024
method: pbad, mean f1: 0.844. std f1:  0.037
method: ipbad, mean f1: 0.916. std f1:  0.004
method: ipbad_fpof, mean f1: 0.813. std f1:  0.000
    '''


# ----- COLRUYT -----
data_folder = '/Users/lfereman/git2/conscious/data/colruyt/'
hasselt = data_folder + 'bio-planet_hasselt.csv'
aalst = data_folder + 'colruyt_aalst.csv'
aarschot = data_folder + 'colruyt_aarschot.csv'
heverlee = data_folder + 'colruyt_heverlee.csv'

if run_water_grid:
    df, anomalies = load_colruyt_data(aalst)
    results_ipbad = []
    grid = cart_product(interval=[72,24,12,6,3], no_symbols=[5,10], no_bins=[5,10,25,50,100])   
    for preprocess_parameter in grid:
        
        try:
            parameter = {"use_iso":True, "use_MDL":True}
            preprocess_outlier = True
            an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                   no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                   preprocess_outlier=preprocess_outlier, binning_method='normal',verbose=True,
                                                                                   use_MDL=parameter['use_MDL'], use_iso=parameter['use_iso'])
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
            print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
            print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
            results_ipbad.append((f1, preprocess_parameter,parameter))
        except Exception as e:
            print(e)
    print("###IPBAD")
    results_ipbad.sort(key=lambda t: t[0], reverse=True)
    for res in results_ipbad:
        print(res)
        
    '''###IPBAD -> Aalst
0.7589743589743589, {'interval': 72, 'no_symbols': 5, 'no_bins': 100}, {'use_iso': True, 'use_MDL': True})
(0.6851186826661995, {'interval': 72, 'no_symbols': 10, 'no_bins': 100}, {'use_iso': True, 'use_MDL': True})
(0.6335479914136768, {'interval': 24, 'no_symbols': 10, 'no_bins': 50}, {'use_iso': True, 'use_MDL': True})
'''
        
if run_water:
   for dataset in [heverlee, aalst, hasselt, aarschot]:
       results = defaultdict(list)
       for k in range(0,5):
           #repeat for hasselt, aarschot, heverlee
           #df, anomalies = load_colruyt_data(aalst)
           #df, anomalies = load_colruyt_data(hasselt)
           #df, anomalies = load_colruyt_data(aarschot)
           df, anomalies = load_colruyt_data(dataset)
           
           preprocess_parameter = {"interval": 72, "no_symbols": 10, "no_bins": 100} #see grid search
           preprocess_outlier=True
           
           #ipbad comprehesnive
           parameter = {'use_iso': False, 'use_MDL': True}
           an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                           no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                           preprocess_outlier=False, binning_method='normal',verbose=True,
                                                                                           use_MDL=parameter['use_MDL'], 
                                                                                           use_iso=parameter['use_iso'])
           (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
           print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
           print(f">>>>>>RUN_IPBAD-FPFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
           #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
           #               interval=preprocess_parameter["interval"], stride=1, 
           #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
           results['ipbad_fpof'].append(f1) 
           
           #iso forest
           an_scores, ts_i, segments, windows =  run_iso_forest(df, 
                                                                interval=preprocess_parameter["interval"], stride=1, 
                                                                preprocess_outlier=preprocess_outlier)
           (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
           print(f">>>>>>Run ISO forest F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
           results['iso'].append(f1) 
           #pfof
           #FOR TAXI: minsup is 0.001
           an_scores1, ts_i, segments_discretised, windows = run_pfof(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                               preprocess_outlier=preprocess_outlier, relative_minsup=0.01)
           (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores1, anomalies) 
           results['fpof'].append(f1)
           print(f'preprocessing: {preprocess_parameter}')
           print(f">>>>>>RUN_PFOF F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
           
           
            
           #pbad
           an_scores, ts_i, segments_discretised, windows = run_pbad(df, interval=preprocess_parameter["interval"], stride=1, 
                                                                     no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"],
                                                                     preprocess_outlier=preprocess_outlier, relative_minsup=0.01)
           (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies) 
           print(f">>>>>>RUN_PBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
           results['pbad'].append(f1) 
           
           #ipbad
           parameter = {"use_iso":True, "use_MDL":True}
           an_scores2, ts_i, segments_discretised, windows = run_ipbad(df=df, interval=preprocess_parameter["interval"], stride=1, 
                                                                                           no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], 
                                                                                           preprocess_outlier=preprocess_outlier, binning_method='normal',verbose=True,
                                                                                           use_MDL=parameter['use_MDL'], 
                                                                                           use_iso=parameter['use_iso'])
           (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores2, anomalies)
           print(f'preprocessing: {preprocess_parameter} parameter: {parameter}')
           print(f">>>>>>RUN_IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
           results['ipbad'].append(f1) 
           #plot_anomalies(f'latency-ipbad:{f1:.3f}', df, anomalies, an_scores2, segments_discretised,  windows, 
           #               interval=preprocess_parameter["interval"], stride=1, 
           #               no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], t=t)
           
          
        
       print(dataset)
       for key, values in results.items():
           print(f'method: {key}, mean f1: {np.mean(values) :.3f}. std f1: {np.std(values): .3f}')  
           
    #aalst 1-fold:
    #Iso 0.7064
    #fpof: 0.3331
    #pbad: 0.7549
    #ipbad: 0.7939
    #IPBAD-FPFOF: 0.5491
    
    #16/05
    #method: ipbad_fpof, mean f1: 0.549. std f1:  0.000
    #method: iso, mean f1: 0.686. std f1:  0.009
    #method: fpof, mean f1: 0.333. std f1:  0.000
    #method: pbad, mean f1: 0.755. std f1:  0.007
    #method: ipbad, mean f1: 0.787. std f1:  0.019

    #hasselt
    #16/05: method: ipbad_fpof, mean f1: 0.452. std f1:  0.000
    #method: iso, mean f1: 0.528. std f1:  0.013
    #method: fpof, mean f1: 0.450. std f1:  0.000
    #method: pbad, mean f1: 0.596. std f1:  0.008
    #method: ipbad, mean f1: 0.539. std f1:  0.011

    #aarschot 16/05
#method: ipbad_fpof, mean f1: 0.447. std f1:  0.000
#method: iso, mean f1: 0.791. std f1:  0.011
#method: fpof, mean f1: 0.321. std f1:  0.000
#method: pbad, mean f1: 0.615. std f1:  0.023
#method: ipbad, mean f1: 0.567. std f1:  0.012

    #heverlee   
    #16/05:
    #method: ipbad_fpof, mean f1: 0.663. std f1:  0.000
    #method: iso, mean f1: 0.561. std f1:  0.005
    #method: fpof, mean f1: 0.661. std f1:  0.000
    #method: pbad, mean f1: 0.760. std f1:  0.010
    #method: ipbad, mean f1: 0.666. std f1:  0.006

    