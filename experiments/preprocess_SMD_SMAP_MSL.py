import os
import numpy as np
import pandas as pd
#Load ServerMachineDataset
'''
SMD (Server Machine Dataset) is a new 5-week-long dataset. We collected it from a large Internet company. 
This dataset contains 3 groups of entities. Each of them is named by machine-<group_index>-<index>.

SMD is made up by data from 28 different machines, and the 28 subsets should be trained and tested separately. 
For each of these subsets, we divide it into two parts of equal length for training and testing. 
We provide labels for whether a point is an anomaly and the dimensions contribute to every anomaly.

Thus SMD is made up by the following parts:

train: The former half part of the dataset.
test: The latter half part of the dataset.
test_label: The label of the test set. It denotes whether a point is an anomaly.
interpretation_label: The lists of dimensions contribute to each anomaly.

38 metrics per device 

#28 840 measurements * 2 in 5*7*24*60=50400 minutes (+- every 1 minute a measure)
'''
def preproces_SMD(dataset_folder, output_folder):
    #code from OmnyAnomaly data_preprocess
    def load_and_save_mv_data(category, filename, dataset, dataset_folder, no_cols):
        fname = os.path.join(dataset_folder, category, filename)
        temp = np.genfromtxt(fname,dtype=np.float32,delimiter=',')
        df = pd.read_csv(fname, names=[f'metric-{i+1}' for i in range(0,no_cols)])
        df['time_id'] = list(range(0,df.shape[0]))
        #add some "fake" dates
        df['time'] = [np.datetime64('2022-01-01 00:00:00') + np.timedelta64(i,'m') for i in range(0,df.shape[0])]
        df.to_csv(f'{output_folder}/{dataset}_{category}.csv', index=False)
        
    def load_and_save_labels(category, filename, dataset, dataset_folder):
        fname = os.path.join(dataset_folder, category, filename)
        temp = np.genfromtxt(fname,dtype=np.float32,delimiter=',')
        df = pd.DataFrame(temp, columns=['label'])
        df['time_id'] = list(range(0,df.shape[0]))
        df['time'] = [np.datetime64('2022-01-01 00:00:00') + np.timedelta64(i,'m') for i in range(0,df.shape[0])]
        df = df[df['label'] != 0]
        df.to_csv(f'{output_folder}/{dataset}_{category}.csv', index=False)
        
    #i.e. ServerMachineDataset contains
    #15849-16368:1,9,10,12,13,14,15
    #16963-17517:1,2,3,4,6,7,9,10,11,12,13,14,15,16,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35,36
    #meaning anomaly from time_id 15849 to 16368 are related to metric 1, ...
    def load_and_save_label_interpretation(category, filename, dataset, dataset_folder):
        fname = os.path.join(dataset_folder, category, filename)
        records = []
        for line in open(fname).readlines():
            first, second = line.strip().split(":")
            frm, to = first.split('-')
            dimensions = [f'metric-{i}' for i in second.split(',')]
            records.append([int(frm),int(to), dimensions])
        df = pd.DataFrame.from_records(records, columns=['from_time_id','to_time_id','dimensions'])
        df.to_csv(f'{output_folder}/{dataset}_{category}.csv', index=False)
        
        
    file_list = os.listdir(os.path.join(dataset_folder, "train"))
    for filename in file_list:
        if filename.endswith('.txt'):
            load_and_save_mv_data('train', filename, filename.strip('.txt'), dataset_folder, 38)
            load_and_save_mv_data('test', filename, filename.strip('.txt'), dataset_folder, 38)
            load_and_save_labels('test_label', filename, filename.strip('.txt'), dataset_folder)
            load_and_save_label_interpretation('interpretation_label', filename, filename.strip('.txt'), dataset_folder)


#The anomaly labels and metadata are available in labeled_anomalies.csv, which includes:
'''
channel id: anonymized channel id - first letter represents nature of channel (P = power, R = radiation, etc.)
spacecraft: spacecraft that generated telemetry stream
anomaly_sequences: start and end indices of true anomalies in stream
class: the class of anomaly (see paper for discussion)
num values: number of telemetry values in each stream
'''
def preproces_NASA(dataset_folder, output_folder, spacecraft='SMAP'): 
    #spacecraft is Soil Moisture Active Passive (SMAP) satellite and the Mars Science Laboratory (MSL) rove
    #Note: OmnyAnomaly excludes P-2 for some reason
    fname = os.path.join(dataset_folder, 'labeled_anomalies.csv')
    df = pd.read_csv(fname) #chan_id,spacecraft,anomaly_sequences,class,num_values
    df = df.sort_values(by='chan_id') 
    df = df[df['spacecraft'] == spacecraft]
    df['anomaly_sequences'] = df['anomaly_sequences'].apply(eval)
    for category in ['train','test']:
        for idx, row in df.iterrows():
            filename = row['chan_id']
            metric = np.load(os.path.join(dataset_folder, category, filename + '.npy'))
            #note: time series * 25 dimensional matrix... but only first column is not 0
            df2 = pd.DataFrame(metric, columns=[f'metric-{i+1}' for i in range(0,metric.shape[1])])  #25
            df2['time_id'] = list(range(0,df2.shape[0]))
            df2['time'] = [np.datetime64('2022-01-01 00:00:00') + np.timedelta64(i,'m') for i in range(0,df2.shape[0])]
            df2.to_csv(f'{output_folder}/{spacecraft}/{filename}_{category}.csv', index=False)
            df_sample = df[df['chan_id'] == filename]
            df_sample.to_csv(f'{output_folder}/{spacecraft}/{filename}_labels.csv', index=False)
    
preproces_SMD('/Users/lfereman/git2/OmniAnomaly/ServerMachineDataset', '/Users/lfereman/git2/conscious/data/multivariate/SMD/')
#preproces_NASA('/Users/lfereman/git2/OmniAnomaly/data', 
#               output_folder='//Users/lfereman/git2/conscious/data/multivariate/',
#               spacecraft='MSL') #SMAP