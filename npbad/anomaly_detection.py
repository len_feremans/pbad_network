import numpy as np
import collections
from sklearn.ensemble import IsolationForest

def fpof(embedding, no_patterns): 
    '''
    Compute frequent pattern outlier factors
    Divide sum of relative support (so between 0 and 1) between total number of patterns
    Note: For future work also interesting definitions on contradictness of patterns in FPOF paper, see http://www.doiserbia.nb.rs/img/doi/1820-0214/2005/1820-02140501103H.pdf
    '''
    fpof_scores = []
    for embedding_instance in embedding:
        fpof = 0
        for pattern,rsupport in embedding_instance:
            fpof += rsupport
        fpof = (1.0 - fpof / no_patterns) #inverse, i.e. high value more likely anomaly
        fpof_scores.append(fpof)
    return fpof_scores


def iso_forest(embedding, verbose=False): 
    '''Make pattern-based embedding unsparse and run iso forest '''
    embedding_unsparse = _make_unsparse(embedding, verbose=verbose)
    #remove 0 columns
    shape_before = embedding_unsparse[0].shape[0]
    embedding_unsparse = np.stack(embedding_unsparse)
    #print(embedding_unsparse)
    idx = np.argwhere(np.all(embedding_unsparse[..., :] == 0, axis=0))
    embedding_unsparse = np.delete(embedding_unsparse, idx, axis=1)
    if verbose:
        print(f'iso_forest: removing all-0 columns: columns before/after {shape_before} -> {embedding_unsparse[0].shape}')
    #clf = IsolationForest(random_state=0, n_estimators=100)
    clf = IsolationForest(n_estimators=500, max_samples=1500, max_features=min(50, embedding_unsparse[0].shape[0]))
    clf.fit(embedding_unsparse)
    isof_scores = clf.decision_function(embedding_unsparse) #classifier returns -1 for outlier and 1 for inlier... not can also return score
    isof_scores = -isof_scores
    isof_scores = (isof_scores + 1) / 2
    return isof_scores

def iso_forest_no_embedding(embedding): 
    '''Run iso forest '''
    #clf = IsolationForest(random_state=0, n_estimators=100)
    clf = IsolationForest(n_estimators=500, max_samples=1500, max_features=min(50, embedding[0].shape[0]))
    clf.fit(embedding)
    isof_scores = clf.decision_function(embedding) #classifier returns -1 for outlier and 1 for inlier... not can also return score
    isof_scores = -isof_scores
    isof_scores = (isof_scores + 1) / 2
    #isof_scores = (isof_scores - isof_scores.min()) / (isof_scores.max() - isof_scores.min())
    #isof_scores[isof_scores < np.quantile(isof_scores, q=q)] = 0
    return isof_scores


def _make_unsparse(embedding, verbose=False):
    '''
    Make pattern-based embedding unsparse before using isolation forest.
    '''
    total_patterns = 0
    for inst in embedding:
        for pattern_idx, rsupport in inst:
            total_patterns = max(total_patterns,pattern_idx)
    total_patterns +=1
    embedding_unsparse = []
    total_non_zero_patterns = set()
    for inst in embedding:
        embedding = np.zeros(total_patterns)
        for pattern_idx, rsupport in inst:
            total_non_zero_patterns.add(pattern_idx)
            embedding[pattern_idx] = rsupport
        embedding_unsparse.append(embedding)
    if verbose:
        print(f'Create unsparse embedding. Size: {len(embedding_unsparse)}.  Shape first: {len(embedding_unsparse[0])}. Minimal non-zero size:{len(total_non_zero_patterns)} ')
    return embedding_unsparse
   