import os
import time
import pandas as pd

from npbad.preprocess_timeseries import min_max_norm, remove_outliers_ts_new
from npbad.symbolic_representation import create_windows, create_segments, discretise_segments_equal_distance_bins_global
from npbad.baselines.pattern_mining_spmf import mine_closed_itemsets 
from npbad.anomaly_detection import fpof

def run_pfof(df, interval, stride, no_symbols, no_bins, preprocess_outlier=True, relative_minsup=0.01):
    print("run_pfof")
    #preprocess
    df_temp = df.copy()
    if preprocess_outlier:
        #ts_i = min_max_norm(remove_outliers_ts(df_temp,q=0.95)) 
        ts_i = min_max_norm(remove_outliers_ts_new(df_temp,q=0.99))  
    else:
        ts_i = min_max_norm(df_temp)
    #symbolic representation
    windows = create_windows(ts_i, interval=interval, stride=stride) 
    segments = create_segments(ts_i, windows)
    segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins, no_symbols)
    print(f'segmentation: len:{len(segments_discretised)}, first: {segments_discretised[0]}')
    for i in range(0,20):
        print(segments_discretised[i])
    #pattern mining
    if not os.path.exists('./temp'):
        os.makedirs('./temp')
    patterns, supports = mine_closed_itemsets(segments_discretised, relative_minsup, './temp')
    patterns_df = pd.DataFrame.from_records(zip(patterns,supports), columns=['pattern','support'])
    patterns_df['rsupport'] = patterns_df['support'] / len(segments_discretised)
    patterns_df = patterns_df.sort_values(by='support', ascending=False)
    patterns_df['id'] = list(range(0,patterns_df.shape[0]))
    if patterns_df.shape[0] < 5:
        raise RuntimeError('run_pfof: less than 5 patterns')
    print(f'loading #{patterns_df.shape[0]} patterns. Min/Max rsupport :{patterns_df["rsupport"].min():.3f}/{patterns_df["rsupport"].max():.3f}')
    print(patterns_df.head())
    #create embedding
    embedding = create_embedding_naive_itemset(patterns_df, segments_discretised)
    print(segments_discretised[0],embedding[0])
    print(segments_discretised[-1],embedding[-1])
    
    #fpof
    fpof_scores = fpof(embedding, patterns_df.shape[0])
    an_scores = list(zip(windows, fpof_scores))
    print(f'create anomaly scores fpof. Number of scores: {len(an_scores)}. First 5: {an_scores[0:5]}')
    return an_scores, ts_i, segments_discretised, windows



def itemset_matches(itemset, segment_d):
    segment_d_lst = segment_d.tolist()
    for token in itemset:
        try:
            idx = segment_d_lst.index(token)
        except ValueError:
            return False
    return True
    
def create_embedding_naive_itemset(patterns_df, segments_discretised):
    start = time.time()
    embeddings = []
    for segment_d in segments_discretised:
        embedding = []
        for idx, row in patterns_df.iterrows():
            pattern_idx = row['id']
            pattern = row['pattern']
            rsupport = row['rsupport']
            if itemset_matches(pattern, segment_d): #only store matches
                embedding.append((pattern_idx, rsupport))
        embeddings.append(embedding)
    print(f'create_embedding_naive_itemset elapsed {time.time() -start}s')
    return embeddings
