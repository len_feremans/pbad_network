import numpy as np

from npbad.preprocess_timeseries  import min_max_norm, remove_outliers_ts_new, check_and_fix_equal_length_segments
from npbad.symbolic_representation import create_windows, create_segments
from npbad.anomaly_detection import iso_forest_no_embedding


def run_iso_forest(df, interval, stride, preprocess_outlier=True):
    print("run_iso_forest")
    #preprocess
    df_temp = df.copy()
    if preprocess_outlier:
        #ts_i = min_max_norm(remove_outliers_ts(df_temp,q=0.95)) 
        ts_i = min_max_norm(remove_outliers_ts_new(df_temp,q=0.99))  
    else:
        ts_i = min_max_norm(df_temp)
    #symbolic representation
    windows = create_windows(ts_i, interval=interval, stride=stride) 
    segments = create_segments(ts_i, windows)
    segments = check_and_fix_equal_length_segments(segments)
        
    segments_iso = np.stack(segments) #creates  2d array instead of array of arrays
    isof_scores = iso_forest_no_embedding(segments_iso)
    an_scores = list(zip(windows, isof_scores))
    return an_scores, ts_i, segments, windows


def run_iso_forest_mv(df, interval, stride, preprocess_outlier=True):
    print("run_iso_forest_mv")
    columns = [col for col in df.columns if col != 'time']
    all_segments = []
    for k, col in enumerate(columns):    
        ts_i = df[['time', col]].copy()
        ts_i = ts_i.rename(columns={col: 'average_value'})
        
        #preprocess
        df_temp = ts_i.copy()
        if preprocess_outlier:
            #ts_i = min_max_norm(remove_outliers_ts(df_temp,q=0.95)) 
            ts_i = min_max_norm(remove_outliers_ts_new(df_temp,q=0.99))  
        else:
            ts_i = min_max_norm(df_temp)
        #symbolic representation
        windows = create_windows(ts_i, interval=interval, stride=stride) 
        segments = create_segments(ts_i, windows)
        segments = check_and_fix_equal_length_segments(segments)
        #print(len(segments))
        all_segments.append(segments)
    #concat
    concat = []
    for i in range(0, len(all_segments[0])):
        embedding = []
        for k in range(0, len(all_segments)):
            all_segments_k = all_segments[k]
            embedding.extend(all_segments_k[i])
        concat.append(embedding)
    segments_iso = np.stack(concat) #creates  2d array instead of array of arrays
    isof_scores = iso_forest_no_embedding(segments_iso)
    an_scores = list(zip(windows, isof_scores))
    return an_scores
