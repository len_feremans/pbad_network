import os
import time
import pandas as pd
import numpy as np

from npbad.preprocess_timeseries import min_max_norm, remove_outliers_ts_new, check_and_fix_equal_length_segments
from npbad.symbolic_representation import create_windows, create_segments, discretise_segments_equal_distance_bins_global
from npbad.anomaly_detection import iso_forest_no_embedding
from npbad.baselines.pattern_mining_spmf import mine_closed_itemsets, mine_closed_sequential_patterns_CM_CLASP 
from npbad.baselines.fpof import create_embedding_naive_itemset
from npbad.anomaly_detection import iso_forest_no_embedding, _make_unsparse

def run_pbad(df, interval, stride, no_symbols, no_bins, preprocess_outlier=True, relative_minsup=0.01):
    print("run_pbad")
    #preprocess
    df_temp = df.copy()
    if preprocess_outlier:
        #ts_i = min_max_norm(remove_outliers_ts(df_temp,q=0.95)) 
        ts_i = min_max_norm(remove_outliers_ts_new(df_temp,q=0.99))  
    else:
        ts_i = min_max_norm(df_temp)
    #symbolic representation
    windows = create_windows(ts_i, interval=interval, stride=stride) 
    segments = create_segments(ts_i, windows)
    segments = check_and_fix_equal_length_segments(segments)
    segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins, no_symbols)
    #itemset mining
    patterns, supports = mine_closed_itemsets(segments_discretised, relative_minsup, './temp')
    patterns_df = pd.DataFrame.from_records(zip(patterns,supports), columns=['pattern','support'])
    patterns_df['rsupport'] = patterns_df['support'] / len(segments_discretised)
    patterns_df = patterns_df.sort_values(by='support', ascending=False)
    patterns_df['id'] = list(range(0,patterns_df.shape[0]))
    print(f'loading #{patterns_df.shape[0]} patterns. Min/Max rsupport :{patterns_df["rsupport"].min():.3f}/{patterns_df["rsupport"].max():.3f}')
    print(patterns_df.head())
    print(patterns_df.tail())
    #create itemset embedding
    embedding = create_embedding_naive_itemset(patterns_df, segments_discretised)
    print(segments_discretised[0],embedding[0])
    print(segments_discretised[-1],embedding[-1])
    
    #sequential pattern mining: 
    patterns, supports = mine_closed_sequential_patterns_CM_CLASP(segments_discretised, relative_minsup, './temp')
    patterns_df2 = pd.DataFrame.from_records(zip(patterns,supports), columns=['pattern','support'])
    patterns_df2['rsupport'] = patterns_df2['support'] / len(segments_discretised)
    patterns_df2 = patterns_df2.sort_values(by='support', ascending=False)
    patterns_df2['id'] = list(range(patterns_df.shape[0],patterns_df.shape[0] + patterns_df2.shape[0]))
    print(f'loading #{patterns_df2.shape[0]} patterns. Min/Max rsupport :{patterns_df2["rsupport"].min():.3f}/{patterns_df2["rsupport"].max():.3f}')
    print(patterns_df2.head())
    print(patterns_df2.tail())
    embedding2 = create_embedding_naive_sp(patterns_df2, segments_discretised)
    print(segments_discretised[0],embedding2[0])
    print(segments_discretised[-1],embedding2[-1])
    
    #concat embedding 
    embedding_f = [embedding[i] + embedding2[i] for i in range(0,len(embedding))]
    print(embedding_f[0])
    embedding_unsparse = _make_unsparse(embedding_f) #total_patterns=patterns_df.shape[0] + patterns_df2.shape[0])
    #remove 0 columns
    shape_before = embedding_unsparse[0].shape[0]
    embedding_unsparse = np.stack(embedding_unsparse)
    idx = np.argwhere(np.all(embedding_unsparse[..., :] == 0, axis=0))
    embedding_unsparse = np.delete(embedding_unsparse, idx, axis=1)
    print(f'embedding pbad: removing all-0 columns: columns before/after {shape_before} -> {embedding_unsparse[0].shape}')
    
    #concat raw values
    embedding_unsparse = embedding_unsparse.tolist()
    embedding_f = [segments[i].tolist() + embedding_unsparse[i] for i in range(0,len(embedding_unsparse))]
    print(embedding_f[0])
    
    #iso forest
    embedding_f = np.stack(embedding_f)
    isof_scores = iso_forest_no_embedding(embedding_f)
    an_scores = list(zip(windows, isof_scores))
    print(f'create anomaly scores pbad. Number of scores: {len(an_scores)}. First 5: {an_scores[0:5]}')
    return an_scores, ts_i, segments_discretised, windows



def sp_matches(pattern, segment_d):
        for token in pattern:
            try:
                idx = segment_d.tolist().index(token) 
            except ValueError:
                return False
            segment_d = segment_d[idx+1:]
        return True
    
def create_embedding_naive_sp(patterns_df, segments_discretised):
    start = time.time()
    no_transactions = len(segments_discretised)
    embeddings = []
    for segment_d in segments_discretised:
        embedding = []
        for idx, row in patterns_df.iterrows():
            pattern_idx = row['id']
            pattern = row['pattern']
            rsupport = row['rsupport']
            if sp_matches(pattern, segment_d): #only store matches
                embedding.append((pattern_idx, rsupport))
        embeddings.append(embedding)
    print(f'create_embedding_naive_sp elapsed {time.time() -start}s')
    return embeddings
