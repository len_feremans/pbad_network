import numpy as np
import pandas as pd

def eval_best_f1(an_scores, anomalies, verbose=False, point_adjust=True): 
    ''' Report maximal F1 score assuming an Oracle that select the best threshold '''
    min_score = min([score for window, score in an_scores])
    max_score = max([score for window, score in an_scores])
    bin_width = (max_score - min_score) / 10.0
    print(f'eval_best_f1: #an_scores: {len(an_scores)}, min score: {min_score: .3f}-max score: {max_score: .3f} #anomalies: {len(anomalies)}')
    if min_score == max_score:
        f1,prec,rec,TP,FP,FN,TN = eval_f1_with_threshold(an_scores, anomalies,0.0, point_adjust=point_adjust)
        return f1,prec,rec,TP,FP,FN,TN,0.0
    thresholds = np.arange(min_score,max_score,bin_width)
    maxi = (-1,-1,-1,-1,-1,-1,-1)
    for t in thresholds:
        f1,prec,rec,TP,FP,FN,TN = eval_f1_with_threshold(an_scores, anomalies,t, point_adjust=point_adjust)
        if verbose:
            print(f'eval_best_f1: optimising threshold, t: {t:.4f}, f1: {f1:.6f}, precision: {prec:.4f}, recall: {rec:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}')
        if f1 > maxi[0]:
            maxi = (f1,prec,rec,TP,FP,FN,TN, t)
    return maxi

def eval_f1_with_threshold(an_scores, anomalies, t, point_adjust=True): 
    ''' Eval precision/recall/f1 given a treshold '''
    TP = 0 #
    FP = 0
    FN = 0
    TN = 0
    for i, (window, score) in enumerate(an_scores):
        matching_anomalies = [anomaly for anomaly in anomalies if anomaly >=  window[0] and anomaly <= window[1]]
        correct =  len(matching_anomalies) > 0
        predicted = score>=t
        if point_adjust and correct and not predicted:
            my_range = 100
            #go right
            for lst in [list(range(i+1,i+my_range)), list(range(i-1,i-my_range,-1))]:
                for j in lst: 
                    if j < 0  or j >= len(an_scores):
                        break
                    window2, score2 = an_scores[j]
                    matches_other_window = len([anomaly for anomaly in matching_anomalies if anomaly >= window2[0] and anomaly <= window2[1]]) > 0
                    predicted_other = score2 >= t
                    if not matches_other_window:
                        break
                    if matches_other_window and predicted_other:
                        predicted = True
        if predicted and correct:
            TP +=1
        elif predicted and not correct:
            FP +=1
        elif not predicted and correct:
            FN += 1
        elif not predicted and not correct:
            TN += 1
    prec = TP / (TP+FP) if (TP+FP) != 0 else 0
    rec = TP / (TP+FN) if (TP+FN) !=0 else 0
    f1 = (2*prec*rec)/(prec+rec) if (prec+rec) !=0 else 0
    return f1,prec,rec,TP,FP,FN,TN
    

def eval_recall_at_k(an_scores, anomalies, threshold_top_k, K=10, verbose=False): 
    ''' Compute precision and recall @ K 
    Note: if multiple windows overlap, we create a single windows 
    '''
    an_scores = [(window, score)for window, score in an_scores] #copy
    an_scores.sort(key=lambda p: p[1], reverse=True)
    an_scores = an_scores[0:min(len(an_scores), threshold_top_k)]
    an_scores = _remove_overlapping_windows_in_anomaly_score(an_scores) #also sorts scores
    columns = ["window-start", "window-end", "anomaly-dt", "correct","precision","recall", "more anomalies"]
    records = []
    TP = 0 #
    for k, (window, score) in enumerate(an_scores):
        matching_anomalies = [anomaly for anomaly in anomalies if anomaly >= window[0] and anomaly <= window[1]]
        correct =  len(matching_anomalies) > 0
        anomaly_dt = matching_anomalies[0] if len(matching_anomalies) > 0 else None
        if correct:
            TP+=1
        precision = TP / (k+1) 
        recall = TP / len(anomalies)
        more_anomalies = matching_anomalies[1:] if len(matching_anomalies) > 1 else None
        records.append([window[0],window[1],anomaly_dt,correct, precision, recall, more_anomalies])
    eval_df = pd.DataFrame.from_records(records, columns=columns)
    max_k = min(K, eval_df.shape[0]-1)
    an_scores = an_scores[0:max_k]
    if max_k == -1: #no anomalies
        return 0,0
    if verbose:
        print(f'precision@{max_k+1}: {eval_df.iloc[max_k]["precision"]}. recall@{max_k+1}: {eval_df.iloc[max_k]["recall"]}')
    return eval_df.iloc[max_k]["precision"], eval_df.iloc[max_k]["recall"]
    
    
def _remove_overlapping_windows_in_anomaly_score(an_scores, verbose=False):
    ''' remove overlapping windows, i.e. if w1 and w2 both cover anomaly count as 1 
        e.g. if w1 and w2 both do not cover anomaly, count as 1, therefore extend window start/end for anomaly detection...
        NOTE: Changes semantics somewhat similar to point-adjust and has both advantages and disadvantages
    '''
    to_remove = set()
    for i in range(0,len(an_scores)-1):
        window1 = an_scores[i][0]
        window2 = an_scores[i+1][0]
        if window1[1] > window2[0]: #overlap
            an_scores[i] = ((window1[0], window2[1]),max(an_scores[i][1],an_scores[i+1][1]))
            to_remove.add(i+1)
            i+=1
    new_scores = [an_scores[i] for i in range(0,len(an_scores)) if not i in to_remove]
    new_scores.sort(key=lambda p: p[1], reverse=True)
    if verbose:
        print(f'scores after removing duplicates len: {len(new_scores)}, before: {len(an_scores)}')
    return new_scores


    