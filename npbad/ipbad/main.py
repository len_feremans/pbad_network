import os
import time
import numpy as np
from npbad.preprocess_timeseries import min_max_norm, remove_outliers_ts_new
from npbad.symbolic_representation import create_windows, create_segments
from npbad.symbolic_representation import discretise_segments_equal_distance_bins_global, discretise_segments_equal_distance_bins_local, discretise_segments_kmeans

from npbad.ipbad.pattern_mining_petsc import mine_patterns_and_create_embedding
from npbad.ipbad.minimum_description_length import post_process_mdl
from npbad.anomaly_detection import iso_forest, fpof

def run_ipbad(df, 
              interval, stride, no_symbols, no_bins, preprocess_outlier=True, 
              use_MDL=False, binning_method='normal', use_iso=False, no_scoring=False, verbose=False, return_patterns=False):
    #preprocess
    df_temp = df.copy()
    if preprocess_outlier:
        ts_i = min_max_norm(remove_outliers_ts_new(df_temp,q=0.99))  
    else:
        ts_i = min_max_norm(df_temp)
    #symbolic representation
    windows = create_windows(ts_i, interval=interval, stride=stride) 
    segments = create_segments(ts_i, windows)
    #discretise
    if binning_method == 'normal':
        segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins, no_symbols)
    elif binning_method == 'local':
        segments_discretised = discretise_segments_equal_distance_bins_local(segments, no_bins, no_symbols)
    elif binning_method == 'kmeans':
        segments_discretised = discretise_segments_kmeans(ts_i, segments, no_bins, no_symbols)
    else:
        print(f'error binning_method unknown: {binning_method}')
    #mine patterns    
    fname_transactions = os.path.abspath(f'./temp/transactions-{df.shape[0]}-{interval}-{no_bins}.txt')
    fname_patterns = os.path.abspath(f'./temp/output--{df.shape[0]}-{interval}-{no_bins}.txt')
    fname_embedding = os.path.abspath(f'./temp/embedding--{df.shape[0]}-{interval}-{no_bins}.txt')
    save_transactions(fname_transactions, segments_discretised)
    top_k_initial = 10000 #of 1000
    duration = 1.2
    patterns_df, embedding = mine_patterns_and_create_embedding(fname_transactions, no_transactions = len(segments_discretised), 
                                                                top_k=top_k_initial, min_size=2, max_size=no_symbols,  #was 3
                                                                duration=duration, 
                                                                patterns_fname=fname_patterns, embedding_fname=fname_embedding, verbose=verbose)
    if patterns_df is None or embedding is None or patterns_df.shape[0] == 0:
        #raise RuntimeError("ipbad: Warning: 0 patterns")
        print("Warning/ERROR: ipbad: no patterns. Returning 0 anomaly score")
        if no_scoring:
            return windows, patterns_df, embedding
        elif not return_patterns:
            return list(zip(windows, [0 for window in windows])), ts_i, segments_discretised, windows
        else:
            return list(zip(windows, [0 for window in windows])), ts_i, segments_discretised, windows, patterns_df, embedding
    #post-process MDL
    if use_MDL:
        patterns_df, embedding = post_process_mdl(patterns_df, embedding, segments_discretised, no_symbols, duration, verbose=verbose)
    
    if patterns_df.shape[0] == 0:
        print("Warning/ERROR: ipbad: no patterns after MDL. Returning 0 anomaly score") 
        if no_scoring:
            return windows, patterns_df, embedding
        elif not return_patterns:
            return list(zip(windows, [0 for window in windows])), ts_i, segments_discretised, windows
        else:
            return list(zip(windows, [0 for window in windows])), ts_i, segments_discretised, windows, patterns_df, embedding
    if patterns_df.shape[0] < 5 and patterns_df.shape[0] != 0:
        print("Warning: ipbad: fewer than 5 patterns")
    #return early (in case of heterogenous case)
    if no_scoring:
        return windows, patterns_df, embedding
    
    #2.4 compute FPOF or isolation forest    
    if not use_iso:
        #fpof_scores = fpof_edit(embedding, patterns_df,q=0.8) #keep 20% highest values... taking top_k later
        fpof_scores = fpof(embedding, patterns_df.shape[0])
        an_scores = zip(windows, fpof_scores)
    else:
        #note: not (easy) interpretable
        isof_scores = iso_forest(embedding)
        an_scores = zip(windows, isof_scores)
    an_scores = [(window,score) for window, score in an_scores] # if score > 0
    print(f'create anomaly scores ({"isolation_forest" if use_iso else "fpof"}). Number of scores: {len(an_scores)}. First 5: {an_scores[0:5]}')
    if not return_patterns: #done
        return an_scores, ts_i, segments_discretised, windows
    else:
        return an_scores, ts_i, segments_discretised, windows, patterns_df, embedding

 

def run_ipbad_mv(df,  interval, stride, no_symbols, no_bins, preprocess_outlier=True, use_MDL=False, 
                  binning_method='normal', use_iso=False, verbose=False):
    start = time.time()
    columns = [col for col in df.columns if col != 'time']
    embedding_concat = []
    start_pattern = 0 
    for k, col in enumerate(columns):    
        ts_i = df[['time', col]].copy()
        ts_i = ts_i.rename(columns={col: 'average_value'})
        if verbose:
            print(f'run-single(col={col})')
        windows, patterns_df, embedding = run_ipbad(ts_i, interval, stride, no_symbols, no_bins, 
                                                   preprocess_outlier=preprocess_outlier, use_MDL=use_MDL, binning_method=binning_method,
                                                   no_scoring=True, verbose=verbose)
        if len(embedding_concat) == 0:
            embedding_concat = embedding
        else:
            for i in range(0, len(embedding_concat)):
                embedding_concat[i] = embedding_concat[i] + [(pattern_idx + start_pattern, rsupport) for (pattern_idx, rsupport) in embedding[i]]
            if verbose:
                print(f'created concatenated embedding. Number of segments: {len(embedding_concat)}. Length first segment: {len(embedding_concat[0])}')
        if patterns_df.shape[0] == 0:
            print(f"Warning: No patterns for {col}")
        else:
            start_pattern += patterns_df['id'].max()   
        
    if verbose: 
        print(f'created concatenated embedding. Number of segments: {len(embedding_concat)}. Length first segment: {len(embedding_concat[0])}. First segment: {embedding_concat[0]}')
    no_patterns = start_pattern+1
    #scoring (copy/past ginel)
    if not use_iso:
        fpof_scores = fpof(embedding_concat, no_patterns)#Note: using fpof classic here
        fpof_scores = np.array(fpof_scores)  / max(fpof_scores) #normalize 
        an_scores = zip(windows, fpof_scores)
    else:
        isof_scores = iso_forest(embedding_concat) #set threshold to 0 for viz
        an_scores = zip(windows, isof_scores)
    an_scores = [(window,score) for window, score in an_scores if score > 0]
    if verbose:
        print(f'create anomaly scores ({"isolation_forest" if use_iso else "fpof"}). Number of scores: {len(an_scores)}. First 5: {an_scores[0:5]}')
    print(f'run_mv_hetero elapsed {time.time()-start}s')
    return an_scores        



def save_transactions(fname_transactions, segments_discretised):
    if not os.path.exists(os.path.dirname(fname_transactions)):
        os.mkdir(os.path.dirname(fname_transactions))
    '''Save sequence/transaction database in SPMF format'''
    fout = open(fname_transactions,'w')
    for segment_d in segments_discretised:
        if len(segment_d) == 1 and segment_d[0] == -2: #special case == [-2]
            continue
        for value in segment_d:
            fout.write(str(value))
            fout.write(' -1 ')
        fout.write('-2\n')
    fout.close()
    #print(f'saved #{len(segments_discretised)} sequences of length {len(segments_discretised[0])} to {fname_transactions}')

