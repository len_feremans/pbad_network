from npbad.ipbad.huffman import *
import numpy as np
import math
import collections

#See also Discovery of Meaningful Rules in Time Series by Mohammad Shokoohi-Yekta Yanping Chen Bilson Campana Bing Hu Jesin Zakaria Eamonn Keogh 


def post_process_mdl(patterns_df, embedding, segments_d, no_symbols, duration, verbose=False):
    #Compute occurences patterns, meaning occurrences from sparse embedding are are stored as column in pandas dataframe
    patterns_df = _compute_occurrences_for_pattern(patterns_df, embedding)
    patterns_df['bits_saved'] = patterns_df.apply(lambda row: 
                                              bits_saved_with_pattern_duration_not_1(row['pattern'], 
                                              [segments_d[idx] for idx in row['instances']], no_symbols, duration),
                                               axis=1)
    patterns_df = patterns_df.sort_values(by='bits_saved', ascending=False)
    patterns_df_selection = patterns_df[patterns_df['bits_saved']>0]    
    if verbose:
        print(f"Number of pattern mined {patterns_df.shape[0]}. Number of pattern with bits saved: {patterns_df_selection.shape[0]}")
        print(patterns_df_selection[['id','pattern','support','rsupport', 'bits_saved']]) #excluding 'instances'
    
    def filter_embedding(embedding, patterns_df_selection):
        pattern_ids = set(patterns_df_selection['id'].values.tolist())
        new_embedding = []
        for i, embedding_instance in enumerate(embedding):
            new_embedding_instance=[]
            for pattern,rsupport in embedding_instance:
                if pattern in pattern_ids:
                    new_embedding_instance.append((pattern, rsupport))
            new_embedding.append(new_embedding_instance)
        return new_embedding
    
    embedding = filter_embedding(embedding, patterns_df_selection)
    return patterns_df_selection, embedding
      

def bits_saved_with_pattern_duration_not_1(pattern, matching_segments, no_symbols, duration):
    #One Huffman tree for all sequences matching pattern (NOTE: more bits saved natively, than Huffman tree for all sequences)
    all = ''
    for segment_d in matching_segments:
        segment_d_str = ''.join([str(s) for s in segment_d])
        all += segment_d_str
    #print(all)
    encoding, tree = Huffman_Encoding(all)
    total_bits_segment_huffman = len(encoding)
    if total_bits_segment_huffman == 0: #e.g. if only one symbol
        total_bits_segment_huffman = len(all)
    #print(f'len before replacement: {len(all)}, bits: {total_bits_segment_huffman}')
    all = ''
    for segment_d in matching_segments:
        segment_d = substract_and_replace_duration(pattern, segment_d, duration)
        segment_d_str = ''.join([str(s) for s in segment_d])
        all += segment_d_str
    #print(all)
    #print(all)
    encoding, tree = Huffman_Encoding(all)
    total_bits_MDL = len(encoding)
    if total_bits_MDL == 0: #e.g. if only one symbol
        total_bits_MDL = len(all)
    #print(f'len after replacement: {len(all)}, bits: {total_bits_MDL}')
    total_bits_pattern = len(pattern) * math.ceil(math.log2(no_symbols))
    saved_bits = total_bits_segment_huffman -total_bits_pattern - total_bits_MDL
    return saved_bits



def substract(pattern, segment):
    new_segment = segment.copy()
    #Note: special case: short pattern might match multiple times
    for i in range(0, len(segment) - len(pattern) + 1):
        match = True
        for k in range(0, len(pattern)):
            if pattern[k] != segment[i+k]:
                match=False
                break
        if match:
            for k in range(0, len(pattern)):
                new_segment[i+k] = 0 
    return new_segment           


def substract_and_replace_duration(pattern, segment, duration):
    segment = segment.tolist()
    new_segment = segment.copy()
    max_len = int(math.floor(duration * len(pattern)))
    #Note: special case: short pattern might match multiple times
    for i in range(0, len(segment) - len(pattern) + 1):
        if segment[i] != pattern[0]:
            continue
        match = True
        match_symbols = []
        start = i
        end = min(i + max_len, len(segment))
        for k in range(0, len(pattern)):
            try:
                idx_pattern_symbol = segment.index(pattern[k], start, end)
                start = idx_pattern_symbol + 1
                match_symbols.append(idx_pattern_symbol)
            except ValueError:
                match = False
                break
        if match:
            new_segment[i] = -1
            for k in match_symbols[1:]:
                new_segment[k] = -2
    replace = []
    for i in range(0, len(new_segment)):
        if new_segment[i] >= 0:
            replace.append(new_segment[i])
        if new_segment[i] == -1:
            replace.append(0) #add "symbol" for start of pattern
    return replace

test = False
if test:
    print(substract_and_replace_duration([1,2,3], np.array([1,2,3]), duration=2))
    print(substract([0,0,0,0,0,1], [0,0,0,0,0,1,2,4,0,0,0,0,0,1]))
    print(substract_and_replace_duration([1,2,3], [1,2,2,3,4,4,4,1,2,3], duration=2))
    print(substract_and_replace_duration([0,0,0,0,0,1], [0,0,0,0,0,1,2,4,0,0,0,0,0,1], duration=1))
    print(substract_and_replace_duration([0,0,0,0,0,1], [0,0,0,8,0,0,1,2,4,0,0,0,0,0,0,9,9,1], duration=1.5))

def _compute_occurrences_for_pattern(patterns_df, embedding):
    '''
    Compute occurences patterns, meaning occurrences from sparse embedding are are stored as column in pandas dataframe 
    Usefull for datamining
    '''
    d = collections.defaultdict(list)
    for j, embedding_inst in enumerate(embedding):
        for pattern_idx, rsupport in embedding_inst:
            d[pattern_idx].append(j)
    instances_per_pattern = [d.get(pattern_idx,[]) for pattern_idx in range(0,patterns_df.shape[0])]
    patterns_df['instances'] = instances_per_pattern
    return patterns_df
