import pandas as pd
import re
import os
import time

from npbad.utils import run_cmd

CLASSPATH =  os.path.join(os.path.dirname(__file__), '../../lib/petsc-miner-pbad-0.1.0-with-dependencies.jar')
DEFAULT_MAX_HEAP_SIZE = '-Xmx16G'


#PETSC MINER 
def mine_patterns_and_create_embedding(transactions_file, no_transactions, 
                                       top_k, min_size, max_size, duration, 
                                       patterns_fname, embedding_fname, verbose=False):
    '''Find top-K sequential patterns relative to the constraint on duration 
       Calls Java code based on pattern mining algorithms for time series from PETSC: Pattern-based Embedding for Time Series Classification
       (see https://bitbucket.org/len_feremans/petsc) 
       
       Parameters: 
       @transaction_file: (input) sequence database in SPMF format
       @no_transaction: size of sequence database
       @top_k: Number of sequential patterns to discover (good default is 10.000)
       @min_size: Minimum length of sequential pattern (good default is 2 or 3 or |S|/3)
       @max_size: Maximum length of sequential pattern, usually set to maximal value of sequence database after transformation using SAX variant
       @duration: Maximum relative duration of pattern occurrence, i.e if 1.0 than pattern symbols should occur consecutively, else |X| * duration gaps are allowed.
       @patterns_fname: (output) filename of temporary filename for mining
       @embedding_fname: (output) filename of temporary filename to store embedding
       @verbose: If True print more information
       
    '''
    if os.path.exists(patterns_fname):
        os.remove(patterns_fname)
    if os.path.exists(embedding_fname):
        os.remove(embedding_fname)
    if not os.path.exists(transactions_file):
        print("error mine_patterns_and_create_embedding: no transaction_file found")
        return None, None
    if not os.path.exists(CLASSPATH):
        raise RuntimeError(f"Compiled java code {CLASSPATH} not found. Change CLASSPATH to correct location in anomaly_detection.py or compile Java code using `mvn clean install assembly:single`")
    
    start = time.time()
    cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 'be.uantwerpen.mining.MineTopKSequentialPatternsNoDiscretisation',
           '-input_transactions', transactions_file,
           '-min_size', str(min_size), '-max_size', str(max_size), '-duration', str(duration),
           '-top_k', str(top_k),
           '-output_patterns', patterns_fname,
           '-output_occurrences', embedding_fname]
    if verbose:
        print(cmd)
    run_cmd(cmd, embedding_fname, verbose=verbose)
    if not os.path.exists(patterns_fname) or not os.path.exists(embedding_fname):
        print("error mine_patterns_and_create_embedding: no embedding or patterns file found")
        return None, None
    if verbose:
        print(f'saved embedding to {embedding_fname} elapsed {time.time() -start}s')
    patterns_df = _read_patterns_petsc(patterns_fname, no_transactions, verbose=verbose)
    embedding = _read_embedding_petsc_unsparse(embedding_fname, patterns_df, verbose=verbose)
    return patterns_df, embedding

def _read_patterns_petsc(patterns_fname, no_transactions, verbose=False):
    ''' Read discovered patterns from file. Used by mine_patterns_and_create_embedding
    For instance: (seq. pattern);support 
    6,5,6;13
    5,6,6;12
    ...'''
    fin = open(patterns_fname)
    lines = fin.readlines()
    patterns = []
    supports = []
    for line in lines:
        tokens = re.split(';|,', line.strip())
        items = []
        for i in range(0, len(tokens)-1):
            item = int(tokens[i]) #assume single item in each pattern
            items.append(item)
        support = int(tokens[-1])
        patterns.append(items)
        supports.append(support)
    df = pd.DataFrame.from_records(zip(patterns,supports), columns=['pattern','support'])
    df['rsupport'] = df['support'] / no_transactions
    df = df.sort_values(by='support', ascending=False)
    df['id'] = list(range(0,df.shape[0]))
    if verbose:
        print(f'loading #{df.shape[0]} patterns. Min/Max rsupport :{df["rsupport"].min():.3f}/{df["rsupport"].max():.3f}')
    return df


def _read_embedding_petsc_unsparse(embedding_fname, patterns_df, verbose=False):
    ''' Read embedding from file and return non-sparse representation including rsupport. Used by mine_patterns_and_create_embedding
    For instance:
    0,0,0,0,0,0,0,0,0,0,0,0,...,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,...
    0,0,0,0,0,0,0,0,0,0,0,0,...,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,...
    Returns:
    [(101, 0.3), (130, 0.1)], i.e. pattern 101 and 130 occurs in window 0. Relative support of 101 is 0.3 and for 130 is 0.1
    '''
    pattern_rsupport_dict = {}
    i = 0
    for idx, row in patterns_df.iterrows():
        pattern_rsupport_dict[i] = row['rsupport']
        i+=1
    #load file
    fin = open(embedding_fname)
    lines = fin.readlines()
    embedding_unsparse = []
    for line in lines:
        tokens = re.split(',', line.strip())
        embedding_segment = []
        for i in range(0, len(tokens)):
            item = int(tokens[i]) 
            if item != 0:
                embedding_segment.append((i, pattern_rsupport_dict[i]))
        embedding_unsparse.append(embedding_segment)
    mini = min([len(embedding_segment) for embedding_segment in embedding_unsparse])
    maxi = max([len(embedding_segment) for embedding_segment in embedding_unsparse])
    if verbose:
        print(f'loading embedding #{len(embedding_unsparse)}. Min/Max patterns in segment :{mini}/{maxi}. First: {embedding_unsparse[0]}. Last: {embedding_unsparse[-1]}')
    return embedding_unsparse