import os
import xml.etree.ElementTree as et 
import pandas as pd
from tqdm import tqdm
import numpy as np
import sys
import json
import time

from npbad.preprocess_timeseries import fill_missing, load_ts
from npbad.visualisation import plot_ts, plot_ts_fast
### SKYLINE


#1.1) Load metadata skyline
def parse_protocol(file):
  xtree = et.parse(file)
  xroot = xtree.getroot() 
  namespaces = {'d': 'http://www.skyline.be/protocol'}
  '''
  <Name>Aperi Chassis</Name>
  <Description>Aperi Chassis</Description>
  <Version>1.0.4.8</Version>
  <IntegrationID>DMS-DRV-5523</IntegrationID>
  <Provider>Skyline Communications</Provider>
  <Vendor>Aperi</Vendor>
  <VendorOID>1.3.6.1.4.1.8813.2.647</VendorOID>
  <DeviceOID>3</DeviceOID>
  <ElementType>Platform</ElementType>
  ...
    <Param id="63112" trending="false">
      <Name>Controlplane_VLANSelectionList</Name>
      <Description>Controlplane VLAN Selection List</Description>
      <Type>read</Type>
      <Interprete>
        <RawType>other</RawType>
        <LengthType>next param</LengthType>
        <Type>string</Type>
      </Interprete>
      <Display>
        <RTDisplay>true</RTDisplay>
      </Display>
    </Param>
    ...
    <Param id="200" trending="false">
      <Name>ipFlowsTxTable</Name>
      <Description>IP Flows Tx Table</Description>
      <Type>array</Type>
      <ArrayOptions index="0">
        <ColumnOption idx="0" pid="201" type="retrieved" options="" />
        <ColumnOption idx="1" pid="202" type="retrieved" options="" />
        <ColumnOption idx="2" pid="203" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="3" pid="204" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="4" pid="205" type="retrieved" options="" />
        <ColumnOption idx="5" pid="206" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="6" pid="207" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="7" pid="208" type="retrieved" options="" />
        <ColumnOption idx="8" pid="209" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="9" pid="210" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="10" pid="211" type="retrieved" options="" />
        <ColumnOption idx="11" pid="212" type="retrieved" options=";disableHeaderSum;enableHistogram;enableHeatmap" />
        <ColumnOption idx="12" pid="213" type="retrieved" options=";save;foreignKey=500" />
        <ColumnOption idx="13" pid="254" type="custom" options="" />
      </ArrayOptions>
      <Information>
        <Subtext>Shows generic IP flow data on the transmission side of communication.</Subtext>
      </Information>
      <Interprete>
        <RawType>other</RawType>
        <LengthType>next param</LengthType>
        <Type>double</Type>
      </Interprete>
      <Display>
        <RTDisplay>true</RTDisplay>
        <Positions>
          <Position>
            <Page>IP Flows</Page>
            <Row>0</Row>
            <Column>0</Column>
          </Position>
        </Positions>
  '''
  def _txt(node, xpath):
    subnode = node.find(xpath, namespaces)
    if not subnode is None:
      return subnode.text
    else:
      return None
  #parse protocol metadata
  _pname = _txt(xroot, "d:Name")
  _pdesc = _txt(xroot, "d:Description")
  _ptype = _txt(xroot, "d:ElementType")
  _pdevice = _txt(xroot, "d:DeviceOID")
  dct_prot = {"name": _pname, "description": _pdesc,  "type":_ptype, "device": _pdevice}
  #parse parameters
  output = []
  columns = ['ID', 'Trending', 'Name', 'Description', 'Type','RawType', 'Display', "Units", "PartId"]
  for node in xroot.findall("./d:Params/d:Param", namespaces):
    _id = node.attrib.get("id")
    _trending = node.attrib.get("trending")
    _name= _txt(node, "d:Name")
    _desc= _txt(node, "d:Description")
    _type = _txt(node, "d:Type")
    _rawtype = _txt(node, "./d:Interprete/d:RawType")
    _display = _txt(node, "./d:Display/d:RTDisplay")
    _units = _txt(node, "./d:Display/d:Units")
    #parse table-parameters
    if _type == "array":
        for option_node in node.findall("./d:ArrayOptions/d:ColumnOption", namespaces):
           _pid = option_node.attrib.get("pid")
           output.append([_id,_trending, _name, _desc, _type, _rawtype, _display, _units, _pid])
    else:
      output.append([_id,_trending, _name, _desc, _type, _rawtype, _display, _units, None])
  df_params = pd.DataFrame(output,columns=columns)
  #add link to parent
  res = pd.merge(df_params, df_params[['ID','PartId','Name']], how='left', left_on="ID", right_on="PartId", suffixes=('','_parent'))
  res = res.drop(columns=['PartId_parent'])
  res['ID'] = res['ID'].astype('int32') 
  #res['PartId'] = res['PartId'].astype('int32') 
  #res['ID_parent'] = res['ID_parent'].astype('int32') 
  return dct_prot, res
  
def get_protocols(protocol_dir):
    df_protocols = None
    for file in os.listdir(protocol_dir):
        if file.endswith(".xml"):
            absfile = os.path.join(protocol_dir + file)
            dct_prot, df_params = parse_protocol(absfile)
            #display
            dct_prot['file'] = absfile
            pd.set_option('display.max_colwidth',None)
            #print(f"-----------{dct_prot}-------------")
            #print(df_params.head())
            #concat
            df_params['protocol_name'] = dct_prot['name']
            df_params['protocol_type'] = dct_prot['type']
            df_params['protocol_description'] = dct_prot['description']
            if df_protocols is None:
                df_protocols = df_params
            else:
                df_protocols = pd.concat([df_protocols, df_params])
    return df_protocols

def get_trendata_file_info(trend_dir):
  columns = ['Element', 'Parameter', 'Instance', 'Filesize', 'Filename']
  output = []
  files = list(os.listdir(trend_dir))
  for file in tqdm(files, total=len(files)):
    if file.endswith(".csv"):
      absfile = os.path.join(trend_dir + file)
      filename = file.replace(".csv","")
      components = filename.split('_')
      element = components[1]
      parameter = components[2]
      instance = '_'.join(components[3:])
      filesize = os.stat(absfile).st_size / 1000
      output.append([element, parameter, instance, filesize, file])
  df_fileinfo = pd.DataFrame(output,columns=columns)
  df_fileinfo = df_fileinfo.sort_values(by=['Element','Parameter','Instance'])
  df_fileinfo['Element'] = df_fileinfo['Element'].astype('int32')
  df_fileinfo['Parameter'] = df_fileinfo['Parameter'].astype('int32')
  return df_fileinfo

def get_element_info(fname):
    df_element_info = pd.read_csv(fname)
    df_element_info['Element'] = df_element_info['elementKey'].apply(lambda k: np.int32(k.split('/')[1]))
    df_element_info = df_element_info.sort_values(by='Element')
    return df_element_info

def get_device_data(element_id, df_protocols, df_element_info, df_fileinfo):
    protocol_name = df_element_info[df_element_info['Element'] == element_id]['protocolName'].values[0]
    current_device = df_fileinfo[df_fileinfo['Element'] == element_id]
    current_protocol = df_protocols[df_protocols['protocol_name'] == protocol_name]
    current_device = pd.merge(current_device, current_protocol , how='left', left_on='Parameter', right_on='ID', suffixes=('','_proto'))
    current_device = current_device.sort_values(by=['ID_parent','Instance', 'Parameter'])
    selected_cols_device = ['protocol_name', 'protocol_type', 'Element', 'Parameter', 'Instance', 'Filesize', 'Filename','Name', 'Description', 'Units', 'ID_parent', 'Name_parent']
    current_device = current_device[selected_cols_device]
    return current_device

def get_alarm_data(fname):
    df_alarms = pd.read_csv(fname, sep=';')
    df_alarms['element_id'] = df_alarms['element_id'].astype('int32')
    df_alarms['parameter_id'] = df_alarms['parameter_id'].astype('int32')
    df_alarms['time_of_arrival']= pd.to_datetime(df_alarms['time_of_arrival'])
    df_alarms['table_idx_pk'] = df_alarms['table_idx_pk'].apply(lambda s: str(s).upper() if isinstance(s,str) else s)
    return df_alarms

#new, since notebook
def get_all_device_data(df_protocols, df_element_info, df_fileinfo):
    devices = pd.merge(df_fileinfo, df_element_info, how='left', on='Element')
    devices = pd.merge(devices, df_protocols , how='left', left_on=['Parameter', 'protocolName'], right_on=['ID','protocol_name'], suffixes=('','_proto'))
    devices = devices.sort_values(by=['ID_parent','Instance', 'Parameter'])
    selected_cols_device = ['protocol_name', 'protocol_type', 'Element', 'Parameter', 'Instance', 'Filesize', 'Filename','Name', 'Description', 'Units', 'ID_parent', 'Name_parent']
    devices = devices[selected_cols_device]
    return devices



#1.3) Read time series, uncompress, split in windows and transform to SAX
#fill in missing values 
def fix_ts_skyline(df_ts, col='average_value'):
  #1) Remove outlier at 2021-01-26 10:15:46 -> is everywhere
  #DONE: DROP ALL VALUES where status !=0
  #df_ts = df_ts[(df_ts['time'] < np.datetime64('2021-01-26 10:15:00')) | (df_ts['time'] >= np.datetime64('2021-01-26 10:16:00'))]
  df_ts = df_ts[df_ts['status']>=0] #0 or 5 normal, -1,-2... different meaning 
  df_ts = df_ts[df_ts['average_value'].notnull()]
  df_ts = df_ts[~df_ts['average_value'].apply(lambda v: str(v) == 'NaN')]
  #TODO: encode status in "event-like" stream
  #2) Impute missing values every 5 minutes
  df_ts_new = fill_missing(df_ts, gap_in_minute=5) 
  return df_ts_new
  
def fix_ts_skyline2(df_ts, col='average_value'):
  #1) Remove outlier at 2021-01-26 10:15:46 -> is everywhere
  #DONE: DROP ALL VALUES where status !=0
  df_ts = df_ts[df_ts['status']>=0] #0 or 5 normal, -1,-2... different meaning 
  df_ts = df_ts[df_ts['average_value'].notnull()]
  df_ts = df_ts[~df_ts['average_value'].apply(lambda v: str(v) == 'NaN')]
  return df_ts

def plot_alarms(axs, axs_i, ts_alarms):
    axs[axs_i].scatter(ts_alarms['time_of_arrival'].values, [1 for value in ts_alarms['severity_id'].values], color='red', marker='x')
    for idx, row in ts_alarms.iterrows():
        axs[axs_i].text(row['time_of_arrival'], 1, f'{row["parameter_key"]} -> {row["severity_id"]}')
                
    
def load_and_plot_row(row, fast=True):
   #Note: Parameter was ID in notebook
   title=f'Table {row["Name_parent"]}({row["ID_parent"]}) - Param {row["Description"]} ({row["Parameter"]}))'
   xlabel=f'{row["Description"]} ({row["Units"]})'
   df_ts = load_ts(row['Filename'])
   if fast: 
      plot_ts_fast(df_ts,title,xlabel)
   else:
      plot_ts(df_ts,title,xlabel)

def show_timeseries(current_device):
    start = time.time()
    for idx, row in tqdm(current_device.iterrows(), total=current_device.shape[0]):
        load_and_plot_row(row, fast=True)
    print('Elapsed {:1f}'.format(time.time() - start))  