import math
import numpy as np
import time
import matplotlib.pyplot as plt
from collections import defaultdict
from tqdm import tqdm
import seaborn as sns

from npbad.ts_simil.fingerprinting import _d_to_arr, distance_m

#for data with in-equal sampling due to compression (i.e. only storing values that are different), use this
def paa_time_based(ts, min_date_c, max_date_c, interval_hours=24):
   paa_vals = []
   m = ts['average_value'].mean()
   while min_date_c < max_date_c:
     next = min_date_c + np.timedelta64(interval_hours,'h')
     day = ts[(ts['time'] >= min_date_c) & (ts['time'] < next)]
     values_day = day['average_value'].values
     if len(values_day) == 0 & len(paa_vals) == 0:
       paa_vals.append(m)
     elif len(values_day) == 0:
       paa_vals.append(paa_vals[-1])
     else:
       paa_vals.append(np.mean(values_day))
     min_date_c = next
   return paa_vals


def find_similarities_paa(data_ts_paa, q_paa=0.01):
   start = time.time() 
   dist_m = defaultdict(dict)
   pairs = []
   for i in range(0, len(data_ts_paa)):
     for j in range(i+1, len(data_ts_paa)):
       pairs.append([i,j])
   for i,j in tqdm(pairs, total=len(pairs)):
       dist = distance_m(np.array(data_ts_paa[i]),np.array(data_ts_paa[j]))
       if np.isnan(dist):
           continue
       dist_m[i][j] = dist
       dist_m[j][i] = dist #store both directions
   #apply threshold:
   t1 =  np.quantile(_d_to_arr(dist_m, len(data_ts_paa)),q=q_paa)
   print(f'threshold paa: {t1:.3f}')
   dist_t = defaultdict(dict)
   for i,j in tqdm(pairs, total=len(pairs)):
       if i in dist_m and j in dist_m[i] and dist_m[i][j]  <= t1:
          dist_t[i][j] = dist_t[j][i] = dist_m[i][j]
   total_edges = []
   for i in  tqdm(dist_t.keys(), total=len(dist_t.keys())):
      nearest = sorted(list(dist_t[i].items()), key=lambda p: p[1])
      total_edges.append(len(nearest))
      dist_t[i] = nearest
   print(f'On average {np.mean(total_edges)} edges between time series. Total relations: {np.sum(total_edges)}')
   print(f'find similarities paa: elapsed: {time.time() -start}s') 
   return dist_t 

  
''' plot utilities '''

def plot_taa(df_ts, ts_paa, min_date_c, max_date_c, title='', xlabel='', interval_hours=24):
  windows = []
  while min_date_c < max_date_c:
     windows.append(min_date_c +  np.timedelta64(interval_hours//2,'h'))
     min_date_c = min_date_c + np.timedelta64(interval_hours,'h')
  fig, ax = plt.subplots(figsize=(10,1.25))
  plt.title(title)
  ax.set_xlabel(xlabel)
  plt.xticks(rotation=90)
  if not df_ts is None:
    sns.lineplot(ax=ax, x="time", y="average_value", data=df_ts, color='blue', alpha=0.5,  markers= ["o"])
  plt.plot(windows[0:len(ts_paa)], ts_paa, 'o--', label='PAA', color='orange')
  plt.show()

def plot_taa2(ts_paa, ts_paa2, min_date_c, max_date_c, title='', xlabel='', interval_hours=24):
  windows = []
  while min_date_c < max_date_c:
     windows.append(min_date_c +  np.timedelta64(interval_hours//2,'h'))
     min_date_c = min_date_c + np.timedelta64(interval_hours,'h')
  fig, ax = plt.subplots(figsize=(10,1.25))
  plt.title(title)
  ax.set_xlabel(xlabel)
  plt.xticks(rotation=90)
  plt.plot(windows[0:len(ts_paa)], ts_paa, 'o--', label='PAA', color='orange')
  plt.plot(windows[0:len(ts_paa)], ts_paa2, 'o--', label='PAA', color='red')
  plt.show()
  
  
    