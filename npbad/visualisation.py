import numpy as np
import pandas as pd
import math
import time
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.dates as mdates
import seaborn as sns

from npbad.anomaly_detection import fpof
from npbad.preprocess_timeseries import sample_ts

def plot_ts(df_ts, title='', xlabel='', fname=None):
  fig, ax = plt.subplots(figsize=(10,1))
  plt.title(title)
  plt.xlabel(xlabel)
  plt.xticks(rotation=90)
  if 'max_value' in df_ts.columns:
    sns.lineplot(ax=ax, x="time", y="max_value", data=df_ts, color='grey', alpha=0.3)
  if 'min_value' in df_ts.columns:
    sns.lineplot(ax=ax, x="time", y="min_value", data=df_ts, color='lightgrey', alpha=0.3)
  if 'average_value' in df_ts.columns:
    sns.lineplot(ax=ax, x="time", y="average_value", data=df_ts, color='blue', alpha=0.5,  markers= ["o"])
  if fname is None:
      plt.show(block=False)
  else:
      plt.axis('off')
      plt.savefig(fname, bbox_inches='tight')
      plt.close()
      
def plot_ts_fast(df_ts, title='', xlabel='', values=5000):
  if df_ts.shape[0] > values:
    plot_ts(sample_ts(df_ts, values=values), title=title, xlabel=xlabel)
  else:
    plot_ts(df_ts, title=title, xlabel=xlabel)

def plot_ts2(df_ts, df_ts2, title='', xlabel=''):
  fig, ax = plt.subplots(figsize=(20,3))
  plt.title(title)
  plt.xlabel(xlabel)
  plt.xticks(rotation=90)
  if 'average_value' in df_ts.columns:
    sns.lineplot(ax=ax, x="time", y="average_value", data=df_ts, color='blue', alpha=0.3,markers=['v'])
  if 'average_value' in df_ts2.columns:
    sns.lineplot(ax=ax, x="time", y="average_value", data=df_ts2, color='red', alpha=0.3, dashes=[(2, 2)], markers=['o'])
  plt.show()

def plot_ts2_fast(df_ts, df_ts2, title='', xlabel='', values=5000):
  if df_ts.shape[0] > values:
    plot_ts2(sample_ts(df_ts, values=values), sample_ts(df_ts2, values=values), title=title, xlabel=xlabel)
  else:
    plot_ts2(df_ts, df_ts2, title=title, xlabel=xlabel)

def plot_data(ax, df, anomalies):
    ax.plot(df['time'], df['average_value'], color='blue',alpha=0.5)
    #show day/night
    def is_midnight(ts):
        return ts.hour == 0 and ts.minute == 0 and ts.second == 0
    day_changes = df[df['time'].apply(is_midnight)]
    #google "show vertical line matplotlib"
    ax.vlines(day_changes['time'], ymin=df['average_value'].min(), ymax=df['average_value'].max(), color='grey', alpha=0.1)
    plot_anomalies(ax, df, anomalies)
    
def plot_anomalies(ax, df, anomalies, column='average_value'):
    #show average_value in timeseries at which anomaly occurs
    #for colruyt anomalies not exact on timestamp
    def near_value(anomaly_ts, df):
        gap = (df.iloc[1]['time'] - df.iloc[0]['time']).seconds
        selection = df[(df['time'] > anomaly_ts - np.timedelta64(gap,'s')) & (df['time'] < anomaly_ts + np.timedelta64(gap,'s'))]
        if selection.shape[0] == 0:
            return 1.0
        return selection.iloc[0][column]
    ax.scatter(anomalies, [near_value(anomaly, df) for anomaly in anomalies], color='red', marker='X', alpha=0.8, s=50)
    #show red rectangle around anomaly
    for anomaly in anomalies:    
        sampling_width = df.iloc[1]['time'] - df.iloc[0]['time']
        start = mdates.date2num(anomaly - sampling_width*15) #google "show Rechtangle on datetime axes"
        width = (mdates.date2num(df.iloc[1]['time']) - mdates.date2num(df.iloc[0]['time'])) * 30
        ymin = 0
        ymax = 1
        ax.add_patch(Rectangle((start, ymin), width=width, height=ymax-ymin,
             facecolor = 'red',
             fill=True,
             alpha=0.4))

def average_timestamps(timestamp1, timestamp2):
    diff_s = (timestamp2 - timestamp1).total_seconds()/2
    return timestamp1 + np.timedelta64(int(diff_s), 's')
    
def plot_mv_data(datasetname, df, anomalies, an_scores_computed=None, figname=None, threshold=None):
    #start = time.time()
    def show_anomalies(ax, min=0, max=1):
        ax.scatter(anomalies, [1.0 for anomaly in anomalies], color='red', marker='X', alpha=0.5, s=5)
        ax.vlines(anomalies, ymin=0, ymax=1, color='red', alpha=0.1)
    
    no_metrics = len(df.columns) -1
    fig, axs = plt.subplots(no_metrics+1, figsize=(20,2*no_metrics), sharex=True)
    cols = [col for col in df.columns if col != 'time']
    def is_midnight(ts):
            return ts.hour == 0 and ts.minute == 0 and ts.second == 0
    day_changes = df[df['time'].apply(is_midnight)]
    for i, col in enumerate(cols):
        ax = axs[i]
        color = tableau10blind[i % 10]
        if '_C' in color:
            color = tableau10blind[int(col[col.index('_C') + 2:])]
        #sample = sample_ts(df, col=col, values=2000)
        ax.plot(df['time'], df[col], color=color,alpha=0.5)
        #ax.vlines(day_changes['time'], ymin=df[col].min(), ymax=df[col].max(), color='grey', alpha=0.1)
        ax.vlines(day_changes['time'], ymin=0, ymax=1, color='grey', alpha=0.1)
        ax.text(df['time'].min(), 0, f'{col}', fontsize=12)
        show_anomalies(ax)
    #show anomalies:
    if not an_scores_computed is None:
        #timestamps = [window[1] for window, score in an_scores_computed]
        timestamps = [average_timestamps(window[0],window[1]) for window, score in an_scores_computed]
        #[window[0] + np.timedelta64((window[1]-window[0]).seconds,'s') for window, score in an_scores_computed]
        scores = [score for window, score in an_scores_computed]
        ax = axs[no_metrics]
        ax.vlines(day_changes['time'], ymin=min(scores), ymax=max(scores), color='grey', alpha=0.1)
        ax.plot(timestamps, scores , color='orange', marker='x', alpha=0.5, markersize=5)
        if not threshold is None:
            ax.hlines(y=threshold, color='grey', xmin=timestamps[0], xmax=timestamps[-1], linestyle='--')
    #show
    if not figname is None:
        plt.title(datasetname)
        plt.savefig(figname, dpi=300, bbox_inches='tight',pad_inches = 0) #f'{datasetname}.png'
    else:
        plt.title(datasetname)
        plt.show()
    #print(f'Elapsed plot_mv_data: {time.time()-start}s') 
    
        
def plot_discrete(ax, segments_discretised, windows, interval_width, stride, no_symbols, no_bins):
    #remove overlapping segments:
    substr = 0
    if interval_width == stride:
        substr = no_symbols
    else: #overlap, i.e. stride is 12 and window is 24 -> add "half" of discrete representation
        substr = int(math.ceil(stride/interval_width * no_symbols))
    #plot discretised (assume non-overlapping)
    s = []
    s_text = []
    for segment_d in segments_discretised:
        s.extend([symbol / (no_bins -1) + 0.1 for symbol in segment_d[0:substr]]) #normalize to 0-1
        s_text.extend(segment_d[0:substr])
    w = []
    for window in windows: #compute 'timestamps' for each symbol
        stride_symbolic = int(interval_width * 60 / float(no_symbols)) #i.e. 24 hours -> 10 symbols = 2.4 hour per symbol
        timestamps = [np.datetime64(window[0]) + np.timedelta64(i * stride_symbolic,'m') for i in range(0, substr)]
        w.extend(timestamps)    
    for timestamp, symbol_value, symbol in zip(w,s,s_text):
        ax.text(timestamp - np.timedelta64(30,'m'), symbol_value + 0.01, symbol, fontsize=12, color='black', alpha=0.5)
    #show windows
    ax.vlines([windows[0][0],windows[0][1]], ymin=0, ymax=1, color='yellow', alpha=1)
    _plot_days_vlines(ax, windows)
    print(f'discrete plot len w {len(w)}, len s{len(s)}')
    ax.plot(w[0:len(s)], s, color='black', alpha=0.5) #TODO: hack
    
def _plot_days_vlines(ax, windows):
    days = []
    start = windows[0][0]
    start.replace(hour=0)
    start.replace(minute=0)
    start.replace(second=0)
    end = windows[-1][0]
    while start < end:
        days.append(start)
        start = start + np.timedelta64(1,'D')
    ax.vlines(days, ymin=0, ymax=1, color='grey', alpha=0.1)


def plot_pattern(ax, pattern, segments_discretised, windows, interval_width, stride, no_symbols):
    stride_symbolic = int(interval_width * 60 / float(no_symbols)) #i.e. 24 hours -> 10 symbols = 2.4 hour per symbol
    values_pattern = []
    text_pattern_raw = []
    timestamps_pattern = []
    instances = pattern['instances']
    pattern_symbols = pattern['pattern']
    ax.set_ylim([0, 1])
    ax.text(windows[0][0] -np.timedelta64(1,'D'), 0.5, pattern_symbols, fontsize=10)
    #ax.set_ylabel(pattern_symbols)
    _plot_days_vlines(ax, windows)
    ax.get_yaxis().set_ticks([])
    ax.get_yaxis().set_ticklabels([])
    for instance_i in instances:
        timestamps = [np.datetime64(windows[instance_i][0]) + np.timedelta64(i * stride_symbolic,'m') for i in range(0, len(pattern_symbols))]
        values =  [(symbol / float(no_symbols -1)) + 0.1 for symbol in pattern_symbols]
        timestamps_pattern.extend(timestamps)
        values_pattern.extend(values)
        text_pattern_raw.extend(pattern_symbols)
        ax.plot(timestamps, values, color='blue', alpha=0.6,  marker='o')
    for timestamp, value, symbol in zip(timestamps_pattern,values_pattern, text_pattern_raw):
        ax.text(timestamp - np.timedelta64(30,'m'), value + 0.01, symbol, fontsize=12, color='grey')


def plot_patterns(ts_i, anomalies, segments_d, windows, patterns_df, embedding, interval_width=24, stride=24, no_symbols=10, no_bins=5):
  patterns_df = patterns_df.copy()
  patterns_df['id'] = [i for i in range(0,patterns_df.shape[0])]
  timestamps = [window[0] + np.timedelta64(12,'h') for window in windows]
  #plot
  fig, axs = plt.subplots(patterns_df.shape[0]+4, figsize=(30,15), sharex=True, gridspec_kw={'height_ratios': [3,3,2,2] + [1]*patterns_df.shape[0]})
  #0_show continuous
  plot_data(axs[0], ts_i,anomalies)
  #1_show discrete
  plot_discrete(axs[1], segments_d, windows, interval_width=interval_width, stride=stride, no_symbols=no_symbols, no_bins=no_bins)
  plot_anomalies(axs[1], ts_i, anomalies)
  #2_show no patterns
  axs[2].set_ylabel('no patterns')
  q_05 = np.quantile([len(embedding_i) for embedding_i in embedding], q=0.05)
  for timestamp, embedding_i in zip(timestamps,embedding):
    axs[2].text(timestamp, 0.5, len(embedding_i), fontsize=13, color='green' if len(embedding_i)>q_05 else 'red') 
  #3_show fpof
  axs[3].set_ylabel('fpof')
  an_scores = fpof(embedding, patterns_df.shape[0])
  q_95 = np.quantile(an_scores, q=0.95)
  for timestamp, an_score in zip(timestamps,an_scores):
    axs[3].text(timestamp, an_score-0.3, f'{an_score:.2f}', fontsize=11, color='green' if an_score<q_95 else 'red') 
  #4_N show patterns
  for idx, pattern_row in patterns_df.iterrows():
    pattern = pattern_row['pattern']
    pattern_id = pattern_row['id']+4
    plot_pattern(axs[pattern_id], pattern_row, segments_d, windows, interval_width=interval_width, stride=stride, no_symbols=no_symbols)


def plot_no_patterns_embedding_concat(axs, filename_idx, embedding_concat, first_windows, no_patterns, method='rsupport'):
    no_matching_patterns  = []
    for segment in embedding_concat:
        pattern_occ_count = 0
        for pattern_idx, rsupport in segment:
            if pattern_idx > (0 + filename_idx * no_patterns) and pattern_idx < ((filename_idx+filename_idx) * no_patterns):
                if method == 'rsupport':
                    pattern_occ_count+= rsupport #or plus 1 -> What's the difference? flat regions...
                else:
                    pattern_occ_count+=1    
        no_matching_patterns.append(pattern_occ_count)
    maxi = max(no_matching_patterns)
    if maxi == 0:
        maxi = 1
    no_matching_patterns =  [pattern_occ_count/float(maxi) for pattern_occ_count in no_matching_patterns]
    axs[filename_idx].plot([window[0] for window in first_windows], no_matching_patterns, color='green', alpha=0.5)

def plot_no_patterns_embedding(ax, embedding, windows, method='rsupport'):     
    no_matching_patterns  = []
    for segment in embedding:
        pattern_occ_count = 0
        for pattern_idx, rsupport in segment:
            if method == 'rsupport':
                pattern_occ_count+=rsupport #or plus 1 -> What's the difference? flat regions...
            else:
                pattern_occ_count+=1    
        no_matching_patterns.append(pattern_occ_count)
    maxi = max(no_matching_patterns)
    if maxi == 0:
        maxi = 1
    no_matching_patterns =  [pattern_occ_count/float(maxi) for pattern_occ_count in no_matching_patterns]
    ax.plot([window[0] for window in windows], no_matching_patterns, color='orange', alpha=0.25)



tableau10blind = [(0, 107, 164), (255, 128, 14), (171, 171, 171), (89, 89, 89),
             (95, 158, 209), (200, 82, 0), (137, 137, 137), (163, 200, 236),
             (255, 188, 121), (207, 207, 207)]
for i in range(len(tableau10blind)):  
    r, g, b = tableau10blind[i]  
    tableau10blind[i] = (r / 255., g / 255., b / 255.)               