from setuptools import setup, find_packages

setup(
    name="npbad",
    version="0.1.0",
    python_requires=">=3.7",
    include_package_data=True,
    packages=find_packages(),
    install_requires=[
        "numpy>=1.21.5",
        "matplotlib>=3.2.2",
        "pandas>=1.1.5",
        "Pillow>=7.1.2",
        "scikit-learn>=0.23.2",
        "scipy>=1.4.1",
        "seaborn>=0.11.2",
        "pyts>=0.12.0",
        "tqdm>=4.62.3"
    ],
)