package be.uantwerpen;

import net.seninp.jmotif.sax.SAXException;
import net.seninp.jmotif.sax.TSProcessor;
import net.seninp.jmotif.sax.alphabet.Alphabet;
import net.seninp.jmotif.sax.alphabet.NormalAlphabet;

/** 
 * Use SAX for discretisation
 * 
 * @author lfereman
 *
 */
public class DiscretiseNew {

	TSProcessor tsp = new TSProcessor();
	Alphabet na = new NormalAlphabet();
	
	public void zNormaliseInPlace(TimeSeriesDataset ts, double threshold) {
		for(int i=0; i< ts.features.length; i++) {
			double[] current = ts.features[i];
			double[] transformed = tsp.znorm(current, threshold);
			ts.features[i] = transformed;
		}
	}
	
	public void zNormaliseInPlace(TimeSeriesDataset ts) {
		zNormaliseInPlace(ts,0.01);
	}

	public void piecewiseAggregateInPlace(TimeSeriesDataset ts, int paaSize) throws SAXException {
		for(int i=0; i< ts.features.length; i++) {
			double[] current = ts.features[i];
			double[] transformed = tsp.paa(current, paaSize);
			ts.features[i] = transformed;
		}
	}

	//convert PAA to symbols
	public void paa2symbols(TimeSeriesDataset ts, int alphabetSize) throws SAXException {		
		for(int i=0; i< ts.features.length; i++) {
			double[] current = ts.features[i];
			int[] currentString;
			try {
				currentString = tsp.ts2Index(current, na.getCuts(alphabetSize));
			} catch (Exception e) {
				throw new SAXException("paa2symbols error", e);
			}
			double[] currentStringAsFloat = new double[currentString.length];
			for(int j=0; j<currentString.length;j++) {
				currentStringAsFloat[j] = (float)currentString[j];
			}
			ts.features[i] = currentStringAsFloat;
		}
	}
}
