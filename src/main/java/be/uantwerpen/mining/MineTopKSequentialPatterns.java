package be.uantwerpen.mining;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.DiscreteTimeSeriesDataset;
import be.uantwerpen.DiscretiseNew;
import be.uantwerpen.PETS;
import be.uantwerpen.PatternWithSupport;
import be.uantwerpen.TimeSeriesDataset;
import be.uantwerpen.util.Utils;
import net.seninp.jmotif.sax.SAXException;

/**
 * New pattern mining algorithm
 * 
 * Use:
 *  - mine top-k frequent patterns 
 *  - support(A) == sliding window frequency
 *  - constraints are:
 *  		- duration
 *  		- min_size
 *  		- max_size
 *  
 * Note: Assumes sliding window during pre-processing as we only count the "first occurrence" w.r.t. gap constraints 
 * in each window. E.g. assume  [a,a,b,c, .... a,b,c]
 * With sliding window of 4, this becomes [a,a,b,c], [a,b,c,_] ... [a,b,c]
 * w.r.t. duration =1.0, the first occurrence will not matche [a,b,c] but the second and last will
 * 
 * @author lfereman
 *
 */
public class MineTopKSequentialPatterns {

	private DiscreteTimeSeriesDataset ts;
	private int k;
	private int minsize;
	private int maxsize;
	private double duration; //relative to maxsize, e.g. *2 or *1,1
	boolean saveOccurrences = false; //save occurences and ts
	private List<PatternQueueItem> occurrences;
	
	public static boolean TRACE = false;

	
	public static class Projection{
		public Map<Integer,Occurrence> projection = new HashMap<>(); //only keep first occurrence
	}

    class QueueItem implements Comparable<QueueItem>{
		List<Integer> pattern;
		Projection projection;
		Set<Integer> candidateItems;
		Integer support = null;
		
		@Override
		public int compareTo(QueueItem o) {
			return o.support.compareTo(support);
		}
	}
    
    class PatternQueueItem implements Comparable<PatternQueueItem>{
    		List<Integer> pattern;
    		Integer support = null;
    		Projection projection = null; //keep is saveOccurrences is True
    		
    		@Override
    		public int compareTo(PatternQueueItem o) { //sort on descending support, e.g. remove() will return pattern with lowest supportt
    			return support.compareTo(o.support);
    		}
    }
	
	public List<PatternWithSupport>  mine(DiscreteTimeSeriesDataset ts, int k, int minsize, int maxsize, double duration) {		
		this.ts = ts;
		this.minsize = minsize;
		this.maxsize = maxsize;
		this.duration = duration;
		this.k = k;
		//patterns
		PriorityQueue<PatternQueueItem> patternSet = new PriorityQueue<>();
		//level 1: singletons
		//queue initialisation
		Set<Integer> items = ts.unique(); //excludes "-1" which is a special symbol
		PriorityQueue<QueueItem> queue = new PriorityQueue<>(items.size());
		for(Integer item: items) {
			QueueItem queueItem = new QueueItem();
			queueItem.projection = computeProjectionSingleton(item);
			queueItem.pattern = new ArrayList<Integer>();
			queueItem.pattern.add(item);
			queueItem.candidateItems = candidateItems(queueItem.projection, queueItem.pattern);
			queueItem.support = queueItem.projection.projection.size();
			queue.add(queueItem);
		}
		int iterations=0;
		//levels n+1:
		while(!queue.isEmpty()) { 
			iterations++;
			QueueItem currentItem =  queue.remove();
			if(TRACE) {
				String indent = "";
				for(int i=0; i<currentItem.pattern.size()-1; i++) {
					indent += "\t";
				}
				System.out.println(indent + "pattern:" + Arrays.toString(currentItem.pattern.toArray()) + " support:" + currentItem.support);
			}
			if(iterations % 100000 == 0) {
				System.out.println("nr patterns: " + patternSet.size());
			}
			//no candidate items and size < minsize
			if(currentItem.candidateItems.size() == 0 && currentItem.pattern.size() < minsize) {
				continue;
			}
			//no candidate items and size > minsize OR pattern found with maxsize
			if(currentItem.candidateItems.size() == 0 || (currentItem.candidateItems.size() > 0 && currentItem.pattern.size() == maxsize)) {
				PatternQueueItem pattern = new PatternQueueItem();
				pattern.pattern = currentItem.pattern;
				pattern.support = currentItem.projection.projection.size(); //ok
				if(saveOccurrences) {
					pattern.projection = currentItem.projection;
				}
				if(TRACE) {
					System.out.print(">>Adding pattern " + Arrays.toString(currentItem.pattern.toArray()) + "?");
				}
				if(patternSet.size() < k) {
					patternSet.add(pattern);
					if(TRACE) {
						System.out.println("yes (heap < k)");
					}
				}
				else if(pattern.support > patternSet.peek().support) {
					patternSet.remove();
					patternSet.add(pattern);
					if(TRACE) {
						System.out.println("yes (support > min_heap)");
					}
				}
				else {
					if(TRACE) {
						System.out.println("no");
					}
				}
				continue;
			}
			//prune
			if(patternSet.size() == this.k && currentItem.support < patternSet.peek().support) {
				if(TRACE) {
					System.out.println(">>pruning " +  Arrays.toString(currentItem.pattern.toArray()) + " support: " + currentItem.support);
				}
				continue;
			}
			//------------------------ recursion --------------------
			//get next item
			int newItem = currentItem.candidateItems.iterator().next();
			//generate pattern with +1 length
			QueueItem nextQueueItem = new QueueItem();
			nextQueueItem.projection = computeProjectionIncremental(currentItem.projection, currentItem.pattern, newItem);
			List<Integer> pattern = new ArrayList<Integer>(currentItem.pattern);
			pattern.add(newItem);
			nextQueueItem.pattern = pattern;
			nextQueueItem.candidateItems = candidateItems(nextQueueItem.projection, pattern);
			nextQueueItem.support = nextQueueItem.projection.projection.size();
			queue.add(nextQueueItem);
			//add current
			currentItem.candidateItems.remove(newItem);
			queue.add(currentItem);
		}
		//sort on support
		ArrayList<PatternWithSupport> patternsSorted = new ArrayList<>();
		if(saveOccurrences) {
			occurrences = new ArrayList<>();
		}
		while(!patternSet.isEmpty()){
			PatternQueueItem patternInQueue = patternSet.remove();
			patternsSorted.add(new PatternWithSupport(patternInQueue.pattern, patternInQueue.support));
			//save occurrences
			if(saveOccurrences) {
				occurrences.add(patternInQueue);
			}
		}
		Collections.reverse(patternsSorted);
		return patternsSorted;
	}

	//compute sequences and first occurrence into each sequence where singleton occurs
	Projection computeProjectionSingleton(int item) {
		Projection p = new Projection();
		for(int ts_idx=0; ts_idx<ts.features.length; ts_idx++) {
			//index of item in sequence
			int[] current_ts = ts.features[ts_idx];
			Occurrence occ = null;
			for(int j=0; j<current_ts.length - this.minsize + 1; j++) {
				if(current_ts[j] == item) {
					occ = new Occurrence(j,j+1);
					break;
				}
			}
			if(occ != null) {
				p.projection.put(ts_idx, occ);
			}
		}
		return p;
	}
	
	//compute projection of <a,b,c> based on <a,b>
	Projection computeProjectionIncremental(Projection projection, List<Integer> pattern, int newItem) {
		Projection p = new Projection();
		for(Entry<Integer,Occurrence> entry: projection.projection.entrySet()) {
			int[] ts_current = ts.features[entry.getKey()];
			Occurrence occ = entry.getValue();
			//maxsize & duration constraint
			//remark: is global constraint on projection windows -> for actual occurrences we need a |pattern_size| * duration constraint!
			//thus filtering needed when computing support
			int max_size_within_duration = (int)(this.maxsize * this.duration);
			int current_max_size = occ.start + max_size_within_duration; 
			//also a gap constraint, e.g. max_size = 6 and duration=1 -> no gaps... or
			//or max_size = 10 and duration 1.1 -> at most 1 gap
			int maximumNumberOfGaps = max_size_within_duration - this.maxsize;
			int currentGaps = (occ.end - occ.start) - pattern.size();
			int remainingGaps = maximumNumberOfGaps - currentGaps;
			Occurrence occNew = null;
			for(int j=occ.end; j < ts_current.length && j < current_max_size && j < occ.end + remainingGaps + 1; j+=1) {
				if(ts_current[j] == newItem) {
					occNew = new Occurrence(occ.start, j+1);
				}
			}
			if(occNew != null) {
				p.projection.put(entry.getKey(), occNew);
			}
		}
		return p;
	}
	
	//compute candidates with respect to gap constraints
	Set<Integer> candidateItems(Projection projection, List<Integer> pattern){
		Set<Integer> items = new TreeSet<>(); 
		for(Entry<Integer,Occurrence> entry: projection.projection.entrySet()) {
			int[] ts_current = ts.features[entry.getKey()];
			Occurrence occ = entry.getValue();
			//maxsize & duration constraint
			int max_size_within_duration = (int)(this.maxsize * this.duration);
			int current_max_size = occ.start + max_size_within_duration;
			//also a gap constraint, e.g. max_size = 6 and duration=1 -> no gaps... or
			//or max_size = 10 and duration 1.1 -> at most 1 gap
			int maximumNumberOfGaps = max_size_within_duration - this.maxsize;
			int currentGaps = (occ.end - occ.start) - pattern.size();
			int remainingGaps = maximumNumberOfGaps - currentGaps;
			for(int j=occ.end; j < ts_current.length && j < current_max_size && j < occ.end + remainingGaps + 1; j+=1) {
				items.add(ts_current[j]);
			}
		}
		items.remove(-1); //-1 is a special item, e.g. a "gap" or "null" item
		return items;
	}

	//save embedding
	//Note: time series is parameter as can be test time series in classification task
	public DiscreteTimeSeriesDataset createEmbeddingWithWindows(DiscreteTimeSeriesDataset ts,  List<PatternWithSupport> patterns){
		//important for projectionPattern! 
		this.ts = ts;
		//create mapping
		Set<Integer> numberOfOriginalTimeseries = new HashSet<>();
		for(int i=0; i<ts.features.length; i++) {
			int original_idx = ts.originalTimeseriesIdx[i];
			numberOfOriginalTimeseries.add(original_idx);
		}
		DiscreteTimeSeriesDataset result = new DiscreteTimeSeriesDataset();
		result.features = new int[numberOfOriginalTimeseries.size()][];
		for(int i=0; i<numberOfOriginalTimeseries.size(); i++) {
			result.features[i] = new int[patterns.size()];
		}
		result.labels = new int[numberOfOriginalTimeseries.size()];
		for(int p=0; p<patterns.size(); p++) {
			Projection projectionP = projectionPattern(patterns.get(p));
			for(int window_ts_idx:projectionP.projection.keySet()) {
				int series_ts_idx = this.ts.originalTimeseriesIdx[window_ts_idx];
				result.features[series_ts_idx][p]+=1;
			}
		}
		return result;
	}
	
	public DiscreteTimeSeriesDataset createEmbeddingWithWindowsSoft(DiscreteTimeSeriesDataset ts,  List<PatternWithSupport> patterns, double tau){
		//important for projectionPattern! 
		this.ts = ts;
		//create mapping
		Set<Integer> numberOfOriginalTimeseries = new HashSet<>();
		for(int i=0; i<ts.features.length; i++) {
			int original_idx = ts.originalTimeseriesIdx[i];
			numberOfOriginalTimeseries.add(original_idx);
		}
		DiscreteTimeSeriesDataset result = new DiscreteTimeSeriesDataset();
		result.features = new int[numberOfOriginalTimeseries.size()][];
		for(int i=0; i<numberOfOriginalTimeseries.size(); i++) {
			result.features[i] = new int[patterns.size()];
		}
		result.labels = new int[numberOfOriginalTimeseries.size()];
		for(int p=0; p<patterns.size(); p++) {
			for(int window_idx=0; window_idx < ts.features.length; window_idx++) {
				int countSoft = countSoft(ts.features[window_idx], patterns.get(p).pattern, tau);
				int series_ts_idx = this.ts.originalTimeseriesIdx[window_idx];
				result.features[series_ts_idx][p]+=countSoft;
			}
		}
		return result;
	}
	
	//assuming rdur of 1.0
	private int countSoft(int[] window, List<Integer> pattern, double tau) {
		for(int i=0; i < window.length - pattern.size() +1; i++) {
			double dist = 0.0;
			for(int j=0; j < pattern.size(); j++) {
				dist += Math.pow(window[i+j] - pattern.get(j),2);
				//could prune, e.g. if dist > tau^2
			}
			dist = Math.sqrt(dist) / (double)pattern.size();
			if(dist < tau) {
				return 1;
			}
		}
		return 0;
	}
	
	
	//"Feels" like this can be faster, still now it is reasonable
	public Projection projectionPattern(PatternWithSupport pattern) {
		Projection projectionCurrent = computeProjectionSingleton(pattern.pattern.get(0));
		List<Integer> patternCurrent = new ArrayList<>();
		patternCurrent.add(pattern.pattern.get(0));
		for(int i=1; i<pattern.pattern.size(); i++) {
			Projection next = computeProjectionIncremental(projectionCurrent, patternCurrent, pattern.pattern.get(i));
			projectionCurrent = next;
			patternCurrent.add(pattern.pattern.get(i));
		}
		return projectionCurrent;
	}
	
	public static void main(String[] args) throws IOException, SAXException {
		System.out.println(">>" + Arrays.toString(args));
		Map<String,String> paramTypes = new HashMap<String,String>();
		paramTypes.put("input", "file");  //input
		paramTypes.put("input_test", "file");  
		paramTypes.put("output", "file"); //output
		paramTypes.put("output_test", "file"); 
		paramTypes.put("output_patterns", "file"); //for visualisation
		paramTypes.put("output_discretisation_train", "file");
		paramTypes.put("output_discretisation_test", "file");
		paramTypes.put("bins", "int"); //preprocessing
		paramTypes.put("paa_win", "int"); 
		paramTypes.put("window", "int"); 
		paramTypes.put("stride", "int"); //new
		paramTypes.put("max_size", "int");//mining
		paramTypes.put("min_size", "int");
		paramTypes.put("duration", "float"); 
		paramTypes.put("sort-alpha", "bool"); //for visualisation of embedding
		paramTypes.put("soft", "bool");//for PETSC-SOFT
		paramTypes.put("tau","float");
		paramTypes.put("output_occurrences","file"); //for visualisation of embedding, save occurrences
		//save patterns
		paramTypes.put("k", "int");
		if(args.length < 4) {
			System.err.println("Expected: >>be.uantwerpen.mining.MineTopKSequentialPatterns -input ./data/mytimeseries.ts -bins 10 -paa_win 100"
					+ " -min_size 2 -max_size 10 -duration 1.1 -k 1000 -output_test embedding.txt");
		}
		Map<String,Object> values = Utils.getParameters(args, paramTypes);
		if((Integer)values.get("min_size") > (Integer)values.get("paa_win")) {
			throw new RuntimeException("Min size can not be smaller than paa_win");
		}
		Double tau = values.get("tau") != null? (Float)values.get("tau"): 1f/(2d * (Integer)values.get("bins"));
		//pre-processing
		PETS pats = new PETS();
		SlidingWindow sw = new SlidingWindow();
		DiscretiseNew discretise = new DiscretiseNew();
		TimeSeriesDataset ts = pats.readTS(new File((String)values.get("input")));
		int stride = 1;
		if(values.get("stride") != null) {
			stride = (Integer) values.get("stride");
		}
		ts = sw.transform(ts, (Integer)values.get("window"), stride);
		discretise.zNormaliseInPlace(ts, 0.01);
		discretise.piecewiseAggregateInPlace(ts, (Integer)values.get("paa_win"));
		discretise.paa2symbols(ts, (Integer)values.get("bins"));
		DiscreteTimeSeriesDataset ts_d = ts.discrete();
		if(values.get("output_discretisation_train") != null) {
			ts_d.save(new File((String)values.get("output_discretisation_train")), false, true);
		}
		System.out.println("Discretised: shape:" + ts_d.features.length + "," + ts_d.features[0].length);
		System.out.println("After pre-processing: " + Arrays.toString(ts_d.features[0]));
		//mine sequential patterns
		long start = System.currentTimeMillis();
		MineTopKSequentialPatterns miner = new MineTopKSequentialPatterns();
		Boolean saveOccurrences = values.get("output_occurrences") !=null? true: false;
		miner.saveOccurrences = saveOccurrences;
		System.out.println(values.entrySet());
		List<PatternWithSupport> patterns = miner.mine(ts_d,  (Integer)values.get("k"), 
						(Integer)values.get("min_size"),(Integer)values.get("max_size"), (Float)values.get("duration"));
		long finish = System.currentTimeMillis();
		System.out.println("Found " + patterns.size() + " patterns. Elapsed: " + (finish-start)/1000.0);		
		for(PatternWithSupport p: patterns.subList(0, Math.min(5, patterns.size()))) {
			System.out.println(p);
		}
		System.out.println("...");
		if(patterns.size() == 0) {
			return;
		}
		if(values.get("output_patterns") != null) {
			PatternWithSupport.save(new File((String)values.get("output_patterns")), patterns);
		}
		//save embedding
		if(values.get("sort-alpha") != null) {
			System.out.println("Sorting patterns alphanumeric");
			Collections.sort(patterns, (p1,p2) -> Arrays.toString(p1.pattern.toArray()).compareTo(Arrays.toString(p2.pattern.toArray())));
		}
		DiscreteTimeSeriesDataset ts_embedding = null;
		start = System.currentTimeMillis();
		if(values.get("soft") == null) {
			ts_embedding = miner.createEmbeddingWithWindows(ts_d, patterns);
		}
		else {
			ts_embedding = miner.createEmbeddingWithWindowsSoft(ts_d, patterns, tau);
		}
		finish = System.currentTimeMillis();
		System.out.println("Embedding: shape: " + ts_embedding.features.length + "," + ts_embedding.features[0].length + ". Elapsed: " + (finish-start)/1000.0);
		ts_embedding.save(new File((String)values.get("output")), false, false);
		if((String)values.get("input_test") != null) {
			start = System.currentTimeMillis();
			//preprocess input_test
			TimeSeriesDataset ts_test = pats.readTS(new File((String)values.get("input_test")));
			ts_test = sw.transform(ts_test, (Integer)values.get("window"), stride);
			discretise.zNormaliseInPlace(ts_test, 0.01);
			discretise.piecewiseAggregateInPlace(ts_test, (Integer)values.get("paa_win")); 
			discretise.paa2symbols(ts_test, (Integer)values.get("bins"));
			DiscreteTimeSeriesDataset ts_test_d = ts_test.discrete();
			if(values.get("output_discretisation_test") != null) {
				ts_test_d.save(new File((String)values.get("output_discretisation_test")), false, true);
			}
			//save embedding input
			DiscreteTimeSeriesDataset ts_embedding_test = null;
			if(values.get("soft") == null) {
				ts_embedding_test = miner.createEmbeddingWithWindows(ts_test_d, patterns);
			}
			else {
				ts_embedding_test = miner.createEmbeddingWithWindowsSoft(ts_test_d, patterns,tau);
			}
			finish = System.currentTimeMillis();
			System.out.println("Embedding: shape: " + ts_embedding_test.features.length + "," + ts_embedding_test.features[0].length +  ". Elapsed: " + (finish-start)/1000.0);
			ts_embedding_test.save(new File((String)values.get("output_test")), false, false);
		}
		//save occurrences
		if((String)values.get("output_occurrences") != null) {
			List<PatternQueueItem> patternsWithOccurrences = miner.occurrences;
			File outputOccurrencesFile = new File((String)values.get("output_occurrences"));
			if(!outputOccurrencesFile.getParentFile().exists()) {
				outputOccurrencesFile.getParentFile().mkdirs();
			}
			FileWriter writer = new FileWriter(outputOccurrencesFile);
			//save pattern, support, all matching time series windows
			writer.write("pattern;global_support;ts_org_id;ts_org_label;window_id;window_values\n");
			for(PatternQueueItem patternWithOcc: patternsWithOccurrences) {
				List<Integer> time_series_ids = new ArrayList<Integer>(patternWithOcc.projection.projection.keySet());
				Collections.sort(time_series_ids);
				for(Integer ts_idx: time_series_ids) {
					List<Integer> pattern = patternWithOcc.pattern;
					int support = patternWithOcc.support;
					int originalTimeseriesId = miner.ts.originalTimeseriesIdx[ts_idx];
					int label  = miner.ts.labels[ts_idx];
					int[] window_values = miner.ts.features[ts_idx];
					//write pattern
					for(int j=0; j < pattern.size()-1; j++) {
						writer.write(String.format("%d,", pattern.get(j)));
					}
					writer.write(String.format("%d;",pattern.get(pattern.size()-1)));
					writer.write(String.format("%d;",support));
					writer.write(String.format("%d;",originalTimeseriesId));
					writer.write(String.format("%d;",label));
					writer.write(String.format("%d;",ts_idx));
					for(int j=0; j < window_values.length-1; j++) {
						writer.write(String.format("%d,", window_values[j]));
					}
					writer.write(String.format("%d",window_values[window_values.length -1]));
					writer.write("\n");
				}
			}
			writer.close();
			System.out.println("Saved " + outputOccurrencesFile.getName());
		}
	}
	
}
