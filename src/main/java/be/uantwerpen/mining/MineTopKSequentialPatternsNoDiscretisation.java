package be.uantwerpen.mining;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.uantwerpen.DiscreteTimeSeriesDataset;
import be.uantwerpen.PatternWithSupport;
import be.uantwerpen.mining.MineTopKSequentialPatterns.Projection;
import be.uantwerpen.util.Utils;

//Added for PBAD in networks
//Does not perform discretisation using SAX
//No separation between train/test
//Saves patterns and embedding by default
public class MineTopKSequentialPatternsNoDiscretisation {

	
	public static void main(String[] args) throws IOException {
			System.out.println(">>" + Arrays.toString(args));
			Map<String,String> paramTypes = new HashMap<String,String>();
			paramTypes.put("input_transactions", "file");  //input in SPMF format
			paramTypes.put("max_size", "int");//mining
			paramTypes.put("min_size", "int");
			paramTypes.put("top_k", "int");
			paramTypes.put("duration", "float"); 
			paramTypes.put("output_patterns", "file"); //save patterns
			paramTypes.put("output_occurrences","file"); //save occurrences
			//read cmd-line parameters
			if(args.length != 14) {
				System.err.println("Expected: >>be.uantwerpen.mining.MineTopKSequentialPatternsNoDiscretisation"
						+ " -input_transactions ./data/transactions.txt"
						+ " -min_size 2 -max_size 10 -duration 1.1 -top_k 1000"
						+ " -output_patterns pattern.txt -output_occurrences embedding.txt");
			}
			Map<String,Object> values = Utils.getParameters(args, paramTypes);
			int top_k = (Integer)values.get("top_k");
			int max_size = (Integer)values.get("max_size");
			int min_size = (Integer)values.get("min_size");
			float duration = (Float)values.get("duration");
			File input_transactions = new File((String)values.get("input_transactions"));
			File output_patterns = new File((String)values.get("output_patterns"));
			File output_occurrences = new File((String)values.get("output_occurrences"));
			
			//mine sequential patterns with constraints
			DiscreteTimeSeriesDataset ts_d = loadTransactions(input_transactions);
			System.out.println("Discretised: shape:" + ts_d.features.length + "," + ts_d.features[0].length);
			long start = System.currentTimeMillis();
			MineTopKSequentialPatterns miner = new MineTopKSequentialPatterns();
			miner.saveOccurrences = true;
			System.out.println(values.entrySet());
			List<PatternWithSupport> patterns = miner.mine(ts_d, top_k, min_size, max_size, duration);
			Collections.sort(patterns, (a,b) -> b.support - a.support); //sort on support descending
			long finish = System.currentTimeMillis();
			System.out.println("Found " + patterns.size() + " patterns. Elapsed: " + (finish-start)/1000.0);		
			for(PatternWithSupport p: patterns.subList(0, Math.min(5, patterns.size()))) {
				System.out.println(p);
			}
			System.out.println("...");
			if(patterns.size() == 0) {
				return;
			}
			//save patterns
			PatternWithSupport.save(output_patterns, patterns);
			//save embedding
			DiscreteTimeSeriesDataset ts_embedding = null;
			start = System.currentTimeMillis();
			ts_embedding = createEmbeddingSegments(miner, ts_d, patterns);
			//optionally: ts_embedding = miner.createEmbeddingWithWindowsSoft(ts_d, patterns, tau);
			finish = System.currentTimeMillis();
			System.out.println("Embedding: shape: " + ts_embedding.features.length + "," + ts_embedding.features[0].length + ". Elapsed: " + (finish-start)/1000.0);
			ts_embedding.save(output_occurrences, false, false);
	}

	//assumes equal-length transactions consisting of single items stored in SPMF format
	//for instance:
	//0 -1 0 -1 0 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 -2
	//1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1 -1 -2
	//..
	public static DiscreteTimeSeriesDataset loadTransactions(File inputTransactions) throws IOException {
		if(!inputTransactions.canRead()) {
			throw new RuntimeException("Can not read input transaction file " + inputTransactions);
		}
		BufferedReader reader = new BufferedReader(new FileReader(inputTransactions));
		String line = reader.readLine();
		List<List<Integer>> transactions = new ArrayList<>();
		while(line != null) {
			ArrayList<Integer> transaction = new ArrayList<>();
			String[] tokens = line.split(" -1 ");
			for(String token: tokens) {
				Integer tokenI =  Integer.valueOf(token);
				if(tokenI == -2)
					continue;
				transaction.add(tokenI);
			}
			transactions.add(transaction);
			line = reader.readLine();
		}
		//Create DiscreteTimeSeriesDataset (array of arrays for performance)
		DiscreteTimeSeriesDataset ts = new DiscreteTimeSeriesDataset();
		ts.features = new int[transactions.size()][];
		for(int i=0; i<transactions.size(); i++) {
			List<Integer> transaction = transactions.get(i);
			ts.features[i] = new int[transaction.size()];
			for(int j=0; j< transaction.size(); j++) {
				ts.features[i][j] = transaction.get(j);
			}
		}
		return ts;
	}
	
	//In PETSC we output an embedding per time series.... but for anomaly detection we want an embedding per segment
	public static DiscreteTimeSeriesDataset createEmbeddingSegments(MineTopKSequentialPatterns miner, DiscreteTimeSeriesDataset ts_input, List<PatternWithSupport> patterns){
		DiscreteTimeSeriesDataset result = new DiscreteTimeSeriesDataset();
		int number_of_features = ts_input.features.length;
		result.features = new int[number_of_features][];
		for(int i=0; i<number_of_features; i++) {
			result.features[i] = new int[patterns.size()];
		}
		for(int p=0; p<patterns.size(); p++) {
			Projection projectionP = miner.projectionPattern(patterns.get(p));
			for(int window_ts_idx:projectionP.projection.keySet()) {
				result.features[window_ts_idx][p]+=1;
			}
		}
		return result;
	}
}
