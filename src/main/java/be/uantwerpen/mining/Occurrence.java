package be.uantwerpen.mining;
public class Occurrence{
	public int start = 0;
	public int end = 0;

	public Occurrence(int start, int end) {
		super();
		this.start = start;
		this.end = end;
	}

	public int size() {
		return end-start;
	}

	public String toString() {
		return String.format("%d-%d", start, end);
	}
}
