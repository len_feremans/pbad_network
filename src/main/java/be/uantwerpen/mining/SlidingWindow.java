package be.uantwerpen.mining;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.uantwerpen.TimeSeriesDataset;
import be.uantwerpen.util.Pair;

public class SlidingWindow {

	public TimeSeriesDataset transform(TimeSeriesDataset ts, int windowSize, int stride) {
		List<double[]> newTimeSeries = new ArrayList<>(ts.features.length);
		List<Integer> labels = new ArrayList<>();
		List<Integer> mapping = new ArrayList<>();
		for(int i=0; i< ts.features.length; i++) {
			double[] tsI = ts.features[i];
			if(windowSize > tsI.length) {
				throw new RuntimeException("Window greater than time series length");
			}
			int label = ts.labels[i];
			for (int j = 0; j <= tsI.length - windowSize; j+=stride) {
				double[] subsection = Arrays.copyOfRange(tsI, j, j + windowSize);
				newTimeSeries.add(subsection);
				labels.add(label);
				mapping.add(i); //store idx of unwindowed, original time series
			}
		}
		TimeSeriesDataset newTs = new TimeSeriesDataset();
		newTs.features = newTimeSeries.toArray(new double[newTimeSeries.size()][]);
		newTs.labels = new int[labels.size()];
		newTs.originalTimeseriesIdx = new int[mapping.size()];
		for(int i=0; i< labels.size(); i++) {
			newTs.labels[i] = labels.get(i);
			newTs.originalTimeseriesIdx[i] = mapping.get(i);
		}
		return newTs;
	}
	
	//for visualisation patterns a mapping from each window occurrence to the original time series is is needed
	public List<Pair<Integer,Integer>> computeSlidingWindowRanges(TimeSeriesDataset originalTimeSeries, int windowSize) {
		List<Pair<Integer,Integer>> result = new ArrayList<>();
		for(int i=0; i< originalTimeSeries.features.length; i++) {
			double[] tsI = originalTimeSeries.features[i];
			for (int j = 0; j <= tsI.length - windowSize; j++) {
				Pair<Integer,Integer> pair = new Pair<>(j,j+windowSize);
				result.add(pair);
				//double[] subsection = Arrays.copyOfRange(tsI, j, j + windowSize);
				//newTimeSeries.add(subsection);
			}
		}
		return result;
	}
}
