package be.uantwerpen.util;

@SuppressWarnings("rawtypes")
public class Pair<F,S>{
    private F first; 
    private S second;

    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }
    
    public void setFirst(F first) {
        this.first = first;
    }

    public void setSecond(S second) {
        this.second = second;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }
    
    @Override
    public String toString()
    {
    	return String.format("%s:%s", first, second);
    }
    
    /*
    @SuppressWarnings("unchecked")
	@Override
    public int compareTo(Pair<F,S> otherPair)
    {
    	return first.compareTo(otherPair.first);
    }
	*/
}