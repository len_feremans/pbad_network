import pandas as pd
import numpy as np
import math
import time

import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest

from loading import *
from timeseries import *, fix_ts_skyline
from symbolic_representation import *
from anomaly_detection import *
from visualisation import *
from mdl import *
from mdl_2 import *

def load_timeseries(filenames):
    all_time_series = []
    for ts_filename in filenames:
        ts_i = min_max_norm(fix_ts_skyline(load_ts(trend_dir + ts_filename)))
        print(ts_i.head(5))
        all_time_series.append(ts_i)
    return all_time_series

def create_single_representation_sequences(all_time_series, interval, stride, no_bins, no_symbols):
    all_windows = []
    all_segments = []
    all_segments_d = []
    split_indexes = []
    index_start = 0
    for ts_i in all_time_series:
        windows = create_windows(ts_i, interval=interval, stride=stride)
        segments = create_segments(ts_i, windows)
        segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
        #append
        all_windows.append(windows)
        all_segments.extend(segments)
        all_segments_d.extend(segments_discretised)
        index_start = index_start + len(segments_discretised)
        split_indexes.append(index_start)
    print(f'Number of time series: {len(all_time_series)}. Length first: {len(all_time_series[0])}')
    print(f'Number of windows: {len(all_windows)}. Length first: {len(all_windows[0])}')
    print(f'Number of segments: {len(all_segments)}. Length first: {len(all_segments[0])}. Expected length: {len(all_time_series) * (len(all_windows[0]) + 1)})')  
    return all_windows, all_segments, all_segments_d, split_indexes
        


def run_homogenous_multivariate_time_series_mdl(alarm_events, filenames, titles):
    all_time_series = load_timeseries(filenames)
    
    #1.3) pre-process and convert to symbolic representations
    grid={"interval": list(range(12,24*3,12)), 
          "no_symbols": list(range(6,20,2)), 
          "no_bins" : list(range(3,25,2))} 
    
    #New: optimise parameters for symbolic representation automatically...  
    best_score = 1000000
    best_interval = None
    for interval in grid["interval"]:
        stride = interval
        all_windows, all_segments, all_segments_d, split_indexes = create_single_representation_sequences(
            all_time_series, interval, interval, 5, 10) #stride == interval
        score = eval_diff_simil(all_segments, all_segments_d)
        if score < best_score:
            best_score = score
            best_interval = interval
            print(f'best_score:{score:0.3f} interval:{interval}')
    best_score = 1000000
    best_no_symbols = None
    for no_symbols in grid["no_symbols"]:
        all_windows, all_segments, all_segments_d, split_indexes = create_single_representation_sequences(
            all_time_series, best_interval, best_interval, 5, no_symbols)
        score = eval_diff_simil(all_segments, all_segments_d)
        if score < best_score:
            best_score = score
            best_no_symbols = no_symbols
            print(f'best_score:{score:0.3f} no_symbols:{no_symbols}')
    best_score = 1000000
    best_no_bins = None
    for no_bins in grid["no_bins"]:
        all_windows, all_segments, all_segments_d, split_indexes = create_single_representation_sequences(
            all_time_series, best_interval, best_interval, no_bins, best_no_symbols)
        score = eval_diff_simil(all_segments, all_segments_d)
        if score < best_score:
            best_score = score
            best_no_bins = no_bins
            print(f'best_score:{score:0.3f} no_bins:{no_bins}')
    #final
    print(f'best_score:{best_score:0.3f} interval:{best_interval} no_symbols:{best_no_symbols} no_bins:{best_no_bins} ')
    default_stride = best_interval
    all_windows, all_segments, all_segments_d, split_indexes = create_single_representation_sequences(
            all_time_series, best_interval, default_stride, best_no_symbols, best_no_symbols)   #changed from best_interval//3 to 1
    
    #2.1-2.2) mine patterns    
    fname_transactions = os.path.abspath('transactions.txt')
    fname_patterns = os.path.abspath('output.txt')
    fname_embedding = os.path.abspath('embedding.txt')
    save_transactions(fname_transactions, all_segments_d)
    top_k_initial = 10000
    duration = 1.2
    patterns_df, embedding = mine_patterns_and_create_embedding(fname_transactions, no_transactions = len(all_segments_d), 
                                                                top_k=top_k_initial, min_size=2, max_size=best_no_symbols, 
                                                                duration=duration, 
                                                                patterns_fname=fname_patterns, embedding_fname=fname_embedding)
    
    #New: select patterns using MDL
    patterns_df = compute_occurrences_for_pattern(patterns_df, embedding)
    #Compute bits saved
    patterns_df['bits_saved'] = patterns_df.apply(lambda row: 
                                              bits_saved_with_pattern_duration_not_1(row['pattern'], 
                                              [all_segments_d[idx] for idx in row['instances']], best_no_symbols, duration),
                                               axis=1)
    patterns_df = patterns_df.sort_values(by='bits_saved', ascending=False)
    patterns_df_selection = patterns_df[patterns_df['bits_saved']>0]    
    print(f"Number of pattern mined {patterns_df.shape[0]}. Number of pattern with bits saved: {patterns_df_selection.shape[0]}")
    print_full(patterns_df_selection[['id','pattern','support','rsupport', 'bits_saved']]) #excluding 'instances'
    
    def filter_embedding(embedding, patterns_df_selection):
        pattern_ids = set(patterns_df_selection['id'].values.tolist())
        new_embedding = []
        for i, embedding_instance in enumerate(embedding):
            new_embedding_instance=[]
            for pattern,rsupport in embedding_instance:
                if pattern in pattern_ids:
                    new_embedding_instance.append((pattern, rsupport))
            new_embedding.append(new_embedding_instance)
        return new_embedding
    embedding = filter_embedding(embedding, patterns_df_selection)
    
    #2.4 compute FPOF    
    fpof_scores = fpof_edit(embedding, patterns_df_selection)
    print(f'create fpof scores. Number of scores: {len(fpof_scores)}')
    
    #split
    def split(lst, split_indexes):
        prev_index = 0
        splitted = []
        for split_index in split_indexes:
            splitted.append(lst[prev_index:split_index])
            prev_index = split_index
        return splitted
    segments_splitted = split(all_segments_d, split_indexes)
    embedding = split(embedding, split_indexes)
    fpof_scores = split(fpof_scores, split_indexes)
    for i in range(0, len(embedding)):
        print(f'{i}: |ts|={all_time_series[i].shape[0]} |segments|={len(segments_splitted[i])} |embedding|={len(embedding[i])} |fpof|={len(fpof_scores[i])}')
    print(f'segments: #{len(segments_splitted)}, embedding: #{len(embedding)}, windows: #{len(all_windows)}, fpof_scores: #{len(fpof_scores)}')
    print(f' first embedding: #{len(embedding[0])}, first 10: {embedding[0][0:10]}')
    print(f' first fpof: #{len(fpof_scores[0])}, first 10: {fpof_scores[0][0:10]}')
    
    #plot
    fig, axs = plt.subplots(len(filenames)+1, figsize=(20,3*len(filenames)))
    for i in range(0, len(embedding)):
        ts_i_paa = paa_time_based(min_max_norm(all_time_series[i]), interval_minutes=int(best_interval/best_no_symbols * 60))
        axs[i].plot(ts_i_paa['time'], ts_i_paa['average_value'], color=tableau10blind[i%10])
        axs[i].plot([window[0] for window in all_windows[i]], fpof_scores[i], color='red', marker='x')
        plot_no_patterns_embedding(axs[i], embedding[i], all_windows[i])
        plot_discrete(axs[i], segments_splitted[i], all_windows[i], best_interval, default_stride, best_no_symbols)
        axs[i].text(all_windows[i][0][0], 0, titles[i], fontsize=12)
        ts_alarms = alarm_events[alarm_events['Filename'] == filenames[i]]
        if ts_alarms.shape[0] > 0:
            plot_alarms(axs, i, ts_alarms)
    plt.savefig(f'{filenames[0]}_results_multi_mdl.png', dpi=600)
    plt.show(block=True)
    

run_multi_home = True
if run_multi_home:
    
    #### LOADING #####
    data_dir = '/Users/lfereman/git2/conscious/data/skyline_data/CONSCIOUS_DATA/BroadcastMonitoring'
    protocol_dir = data_dir + '/protocols/'
    trend_dir = data_dir + '/Data/TrendData/'
    element_info = data_dir + '/Data/elementInfo.csv'
    alarm = data_dir + '/Data/streamingAlarms_all.csv'
    df_protocols = get_protocols(protocol_dir)
    df_fileinfo = get_trendata_file_info(trend_dir)
    df_element_info = get_element_info(element_info)
    df_all = get_all_device_data(df_protocols, df_element_info, df_fileinfo)
    df_alarms = get_alarm_data(alarm)


    #element_id = 1353  ##1353 example Veerle
    #parameter_id = 50233 #parameter 50233 from device 1353 has warning
    #element_id= 10  #aperi chassis 
    #parameter_id = 10011 #Dataplane_Network_Interface_Counters (62)
    #parameter_id=11002 #Dataplane_Interfaces_Transceiver_Temperature (20)
    #parameter_id=7505 # Capacity (Disk Usage) 
    
    #current_device = get_device_data(element_id, df_protocols, df_element_info, df_fileinfo)
    #print_full(current_device)
    df_alarms['instance_id_len'] = df_alarms['table_idx_pk'].apply(lambda s: str(s)) #.replace('/','-'))
    my_alarms =  pd.merge(df_alarms, df_all, left_on=['element_id','parameter_id'], right_on=['Element','Parameter'])
    my_alarms = my_alarms[my_alarms['severity_id'] <= 2]
    my_alarms = my_alarms.sort_values(by=['Element','Parameter','Instance', 'time_of_arrival'])
    my_alarms = my_alarms.drop_duplicates(subset=['root_alarm_id'])
    my_alarms = my_alarms[['alarm_id','protocol_name', 'element_id','parameter_id', 'instance_id_len', 'parameter_key', 'severity_id','status_id', 'parameter_name', 'time_of_arrival', 'Filename']]
    print(f"no alarms: {df_alarms.shape[0]} no alarms after join df_all with severity id=1: {my_alarms.shape[0]}")
    print_full(my_alarms,k=10)
    groups = my_alarms.groupby(by=["protocol_name", "element_id", "parameter_id"])['alarm_id'].count().reset_index()
    groups.to_csv('alarm_groups_severy_1.csv', index=False)
    print_full(groups)
    
    #element_id = 22
    #parameter_id = 10012
    element_id = 319
    parameter_id = 110
    
    df_all = df_all.sort_values(by=['Element','Parameter','Instance'])
    sample = df_all[(df_all['Element'] > element_id - 20) & (df_all['Element'] < element_id + 20)]
    print(f"Number of time series with element id in [{element_id -20},{element_id+20}]: {sample.shape[0]}")
    sample = sample[(sample['Parameter'] == parameter_id)].head(10)
    print(f"Filter on parameter_id {parameter_id}: {sample.shape[0]}")
    print_full(sample)         
    alarm_events = my_alarms[my_alarms['Filename'].isin(sample['Filename'].values)]
    print(f"Number of my_alarms series with element_id/parameter_id in sample: {alarm_events.shape[0]}]")
    print_full(alarm_events)         
   
    #show_timeseries(sample.head(10))
    titles = [f'{row["protocol_name"]}#{row["Element"]}: {row["Description"]} - {row["Units"]}({row["Parameter"]}))' for idx, row in sample.iterrows()]
    run_homogenous_multivariate_time_series_mdl(alarm_events, sample['Filename'].values.tolist(), titles)

    
    