from loading import *
from timeseries import *
from symbolic_representation import *
from anomaly_detection import *
from visualisation import *
import matplotlib.pyplot as plt
from random import randint
import collections

#### LOADING #####
data_dir = '/Users/lfereman/git2/conscious/data/skyline_data/CONSCIOUS_DATA/BroadcastMonitoring'
protocol_dir = data_dir + '/protocols/'
df_protocols = get_protocols(protocol_dir)
#print_full(df_protocols)
    
trend_dir = data_dir + '/Data/TrendData/'
df_fileinfo = get_trendata_file_info(trend_dir)
#print_full(df_fileinfo)

element_info = data_dir + '/Data/elementInfo.csv'
df_element_info = get_element_info(element_info)
#print_full(df_element_info)

df_all = get_all_device_data(df_protocols, df_element_info, df_fileinfo)
#print_full(df_all)

element_id = 10
current_device = get_device_data(element_id, df_protocols, df_element_info, df_fileinfo)
print_full(current_device)

alarm = data_dir + '/Data/streamingAlarms_all.csv'
df_alarms = get_alarm_data(alarm)
#print_full(df_alarms)

interval=72
stride = 6
no_symbols = 10
no_bins = 5

row_instance = current_device.iloc[4]
first_ts_filename = trend_dir + row_instance['Filename']
ts_i = fix_ts(load_ts(first_ts_filename))#0, 6, 12, 18
ts_i = min_max_norm(ts_i)
windows = create_windows(ts_i, interval=interval, stride=stride)
print(f'no windows: {len(windows)}, first: {windows[0]}, last: {windows[-1]}')
segments = create_segments(ts_i, windows)
segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
#segments_discretised = discretise_segments_kmeans(ts_i, segments, no_bins=no_bins, no_symbols=no_symbols)

print(segments_discretised)



no_patterns = 100
min_len = no_symbols // 3
max_len = no_symbols
duration = 1.0
#2.1-2.2-2.3) mine patterns and create embedding  
fname_transactions = os.path.abspath('transactions.txt')
fname_patterns = os.path.abspath('output.txt')
fname_embedding = os.path.abspath('embedding.txt')
save_transactions(fname_transactions, segments_discretised)
patterns_df, embedding = mine_patterns_and_create_embedding(fname_transactions, 
                                                            no_transactions = len(segments_discretised), 
                                                            top_k=no_patterns, min_size=min_len, max_size=max_len, duration=duration, 
                                                            patterns_fname=fname_patterns, embedding_fname=fname_embedding)
print(patterns_df.head())
print(patterns_df.tail())
#test/debug:
print('test embedding 0...5')
for i in range(0,5):
    i = randint(0, len(segments_discretised))
    print(segments_discretised[i])
    for pattern_idx, rsupport in embedding[i]:
        print(patterns_df['pattern'].values[pattern_idx], rsupport)
 

patterns_df = compute_occurrences_for_pattern(patterns_df, embedding)
print_full(patterns_df)


#test support embedding matches pattern mining support
def show_pattern(row, segments_discretised):
    print(f'pattern: {row["pattern"]}, support: {row["support"]}, segments: {len(row["instances"])}, segment instances: {row["instances"]}')  
    for j in row["instances"]:
        print(f"  segment {j}: {segments_discretised[j]}")
        
for idx, row in patterns_df.tail(5).iterrows():
    show_pattern(row,segments_discretised)
            
        
fig, axs = plt.subplots(10, figsize=(20,3*10))
plt.title(f'Protocol: {row_instance["protocol_name"]} Element: {row_instance["Element"]} Parameter: {row_instance["Parameter"]} Description: {row_instance["Parameter"]}') 
axs[0].plot(ts_i['time'].values, ts_i['average_value'].values, color='blue', alpha=0.8)
ts_i_paa = paa_time_based(ts_i, interval_minutes= int(interval*60 / no_symbols))
axs[1].plot(ts_i_paa['time'].values, ts_i_paa['average_value'].values, color='orange', alpha=0.8)
plot_discrete(axs[2], segments_discretised, windows, interval, stride, no_symbols)
for i in range(0,3):
    row = patterns_df.iloc[i] 
    plot_pattern(axs[3 + i], row, segments_discretised, windows, interval, stride, no_symbols)
    show_pattern(row,segments_discretised)
for i in range(0,3):
    row = patterns_df.iloc[patterns_df.shape[0]-(i+1)]
    plot_pattern(axs[6 + i], row, segments_discretised, windows, interval, stride, no_symbols)
    show_pattern(row,segments_discretised)
#plt.xticks(rotation=90)
plt.savefig('patterns.png')
plt.show()








