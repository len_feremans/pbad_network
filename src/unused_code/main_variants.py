import pandas as pd
import numpy as np
import math
import time

import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest

from loading import *
from timeseries import *
from symbolic_representation import *
from anomaly_detection import *
from visualisation import *




def run_time_series(axs, i, ts_filename, interval_width=24, stride=6, no_bins=5, no_symbols=10, 
                                         no_patterns=100, min_len=4, max_len=10, duration=1.0, 
                                         title='', xlabel='', bin_method='equal_distance'):
    #1.3) pre-process and convert to symbolic representations
    ts_i = fix_ts(load_ts(ts_filename))#0, 6, 12, 18
    ts_i = min_max_norm(ts_i)
    windows = create_windows(ts_i, interval=interval_width, stride=stride)
    print(f'no windows: {len(windows)}, first: {windows[0]}, last: {windows[-1]}')
    segments = create_segments(ts_i, windows)
    #print(f'no segments: {len(segments)}, first: {segments[0]}, last: {segments[-1]}')
    if bin_method != 'equal_distance':
        segments_discretised = discretise_segments_kmeans(ts_i, segments, no_bins=no_bins, no_symbols=no_symbols)
    else:
        segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
    #2.1-2.2-2.3) mine patterns and create embedding  
    fname_transactions = os.path.abspath('transactions.txt')
    fname_patterns = os.path.abspath('output.txt')
    fname_embedding = os.path.abspath('embedding.txt')
    save_transactions(fname_transactions, segments_discretised)
    old_mode = False
    if old_mode:
        mine_top_k_sequential_patterns(fname_transactions, fname_patterns,top_k=no_patterns, min_len=min_len, max_len=max_len, max_gaps=1) #max gap=1: meaning no gaps!
        patterns_df = read_patterns_spmf(fname_patterns, no_transactions=len(segments_discretised), no_patterns=no_patterns)
        print(patterns_df.head())
        print(patterns_df.tail())
        embedding = create_embedding_naive(patterns_df, segments_discretised)
    else:
        patterns_df, embedding = mine_patterns_and_create_embedding(fname_transactions, 
                                                                no_transactions = len(segments_discretised), 
                                                                top_k=no_patterns, min_size=min_len, max_size=max_len, duration=duration, 
                                                                patterns_fname=fname_patterns, embedding_fname=fname_embedding)
    print(patterns_df.head())
    print(patterns_df.tail())
    #test/debug:
    verbose = True
    if verbose:
        print('test embedding 1')
        print(segments_discretised[0])
        for pattern_idx, rsupport in embedding[0]:
            print(patterns_df['pattern'].values[pattern_idx], rsupport)
        print('test embedding 2')
        print(segments_discretised[-1])
        for pattern_idx, rsupport in embedding[-1]:
            print(patterns_df['pattern'].values[pattern_idx], rsupport)
    #2.4 compute FPOF    
    fpof_scores = fpof_edit(embedding, patterns_df)
    #2.5 visualise time series and anomaly score
    if axs is None:
        fig, ax = plt.subplots(figsize=(20,3))
    else:
        ax = axs[i]
    ax.plot(ts_i['time'], ts_i['average_value'], color='blue', alpha=0.8) #min_max normalised
    ax.plot([window[0] for window in windows], fpof_scores, color='red', marker='x')
    ax.text(windows[0][0], 0, title + '-' + xlabel, fontsize=12)
    

#### LOADING #####
data_dir = '/Users/lfereman/git2/conscious/data/skyline_data/CONSCIOUS_DATA/BroadcastMonitoring'
protocol_dir = data_dir + '/protocols/'
df_protocols = get_protocols(protocol_dir)
#print_full(df_protocols)
    
trend_dir = data_dir + '/Data/TrendData/'
df_fileinfo = get_trendata_file_info(trend_dir)
#print_full(df_fileinfo)

element_info = data_dir + '/Data/elementInfo.csv'
df_element_info = get_element_info(element_info)
#print_full(df_element_info)

df_all = get_all_device_data(df_protocols, df_element_info, df_fileinfo)
#print_full(df_all)

element_id = 1353 #10 
#1353 example Veerle
current_device = get_device_data(element_id, df_protocols, df_element_info, df_fileinfo)
print_full(current_device)

alarm = data_dir + '/Data/streamingAlarms_all.csv'
df_alarms = get_alarm_data(alarm)
#print_full(df_alarms)
      
      
#### RUN All #####
run_single = True
if run_single:
    current_device_unique = current_device.drop_duplicates(subset='Parameter')
    the_range = 10
    fig, axs = plt.subplots(the_range, figsize=(20,3*the_range))
    for i in range(0,the_range):
        row = current_device_unique.iloc[i]
        title=f'{row["Description"]} - {row["Units"]}({row["Parameter"]}))'
        ts_filename = row['Filename']
        #Global var: trend_dir
        run_time_series(axs, i, trend_dir + ts_filename, interval_width=72, stride=24, no_bins=5, no_symbols=10, 
                        no_patterns=100, min_len=5, max_len=10, duration=1, 
                        title=title, xlabel='')
    plt.title(row['protocol_name'] + '#' + str(row['Element']))
    plt.savefig('results_single.png', dpi=600)
    plt.show(block=True)
    

    
#Resultaten
#Raar... zelf peaken bij "rechte" lijn.... niet goed!
#Score is mss wat arbitrair.... vaak hoog/laag onduidelijk waarom... lokaal klein verschil?
#Rekening houden met "lokaliteit"? 

#Two cases: single 'pattern' space, i.e. all parameters of same type -> FOCUS Here
#           multiple 'pattern' space, i.e. all parameters of different type -> find joined 'space'
def run_homogenous_multivariate_time_series(alarm_events, filenames, titles,
                                            interval_width=24, stride=6, no_bins=5, no_symbols=10, 
                                            no_patterns=100, min_len=4, max_len=10, duration=1):
    #Done Question 1: How to deal with different quantisation -> Same bins everywhere.... or per-time series... (apples and oranges)
    #TODO: Interval or phase-independent?  -> TODO: Try
    #1.3) pre-process and convert to symbolic representations
    all_segments = []
    all_time_series = []
    all_windows = []
    split_indexes = []
    index_start = 0
    for ts_filename in filenames:
        ts_i = min_max_norm(fix_ts(load_ts(trend_dir + ts_filename)))
        #ts_i = z_norm_ts(fix_ts(load_ts(trend_dir + ts_filename)))
        all_time_series.append(ts_i)
        windows = create_windows(ts_i, interval=interval_width, stride=stride)
        all_windows.append(windows)
        segments = create_segments(ts_i, windows)
        segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
        #Better for total packets without error?
        #segments_discretised = discretise_segments_equal_distance_bins_local(segments, no_bins=no_bins, no_symbols=no_symbols)
        all_segments.extend(segments_discretised)
        index_start = index_start + len(segments_discretised)
        split_indexes.append(index_start)
    
    print(f'Number of time series: {len(all_time_series)}. Length first: {len(all_time_series[0])}')
    print(f'Number of windows: {len(all_windows)}. Length first: {len(all_windows[0])}')
    print(f'Number of segments: {len(all_segments)}. Length first: {len(all_segments[0])}. Expected length: {len(all_time_series) * (len(all_windows[0]) + 1)})')
        
    #2.1-2.2) mine patterns    
    fname_transactions = os.path.abspath('transactions.txt')
    fname_patterns = os.path.abspath('output.txt')
    fname_embedding = os.path.abspath('embedding.txt')
    save_transactions(fname_transactions, all_segments)
    #max gap=1: meaning no gaps!
    
    patterns_df, embedding = mine_patterns_and_create_embedding(fname_transactions, 
                                                                no_transactions = len(segments_discretised), 
                                                                top_k=no_patterns, min_size=min_len, max_size=max_len, duration=duration, 
                                                                patterns_fname=fname_patterns, embedding_fname=fname_embedding)
    
    #2.4 compute FPOF    
    fpof_scores = fpof_edit(embedding, patterns_df)
    print(f'create fpof scores. Number of scores: {len(fpof_scores)}')
    #split
    def split(lst, split_indexes):
        prev_index = 0
        splitted = []
        for split_index in split_indexes:
            splitted.append(lst[prev_index:split_index])
            prev_index = split_index
        return splitted
    segments_splitted = split(all_segments, split_indexes)
    embedding = split(embedding, split_indexes)
    fpof_scores = split(fpof_scores, split_indexes)
    for i in range(0, len(embedding)):
        print(f'{i}: |ts|={all_time_series[i].shape[0]} |segments|={len(segments_splitted[i])} |embedding|={len(embedding[i])} |fpof|={len(fpof_scores[i])}')
    print(f'segments: #{len(segments_splitted)}, embedding: #{len(embedding)}, windows: #{len(all_windows)}, fpof_scores: #{len(fpof_scores)}')
    print(f' first embedding: #{len(embedding[0])}, first 10: {embedding[0][0:10]}')
    print(f' first fpof: #{len(fpof_scores[0])}, first 10: {fpof_scores[0][0:10]}')
    
    fig, axs = plt.subplots(len(filenames)+1, figsize=(20,3*len(filenames)))
    for i in range(0, len(embedding)):
        #i.e. every 24 hours -> 10 PAA symbols, thus every 2.4 hour or 2.4*60 minutes 1 symbol (or PAA abreage)
        #show PAA, since more related to PBAD score
        ts_i_paa = paa_time_based(min_max_norm(all_time_series[i]), interval_minutes=int(interval_width/no_symbols * 60))
        axs[i].plot(ts_i_paa['time'], ts_i_paa['average_value'], color=tableau10blind[i%10])
        axs[i].plot([window[0] for window in all_windows[i]], fpof_scores[i], color='red', marker='x')
        plot_no_patterns_embedding(axs[i], embedding[i], all_windows[i])
        plot_discrete(axs[i], segments_splitted[i], all_windows[i], interval_width, stride, no_symbols)
        axs[i].text(all_windows[i][0][0], 0, titles[i], fontsize=12)
        ts_alarms = alarm_events[alarm_events['Filename'] == filenames[i]]
        if ts_alarms.shape[0] > 0:
            plot_alarms(axs, i, ts_alarms)
    plt.savefig('results_multi.png', dpi=600)
    plt.show(block=True)
    


run_multi_home = True
if run_multi_home:
    #TODO: Also extract 'group'
    df_all = df_all.sort_values(by=['Element','Parameter','Instance'])
    parameter_id = 50233#11002
    parent_id = 50210
    element_id = 1353
    #parameter 50233 from device 1353 has warning
    sample = df_all[(df_all['Parameter'] == parameter_id) & (df_all['Element'] == element_id)].head(10) #10011: aperi chassis Dataplane_Network_Interface_Counters (62)
    alarm_events = df_alarms[(df_alarms['element_id'].isin(sample['Element'].values))]
    alarm_events = alarm_events.drop_duplicates(subset=['root_alarm_id'])
    print_full(alarm_events)         
    alarm_events = pd.merge(alarm_events, sample, left_on=['parameter_id'], right_on=['Parameter'])
    #11002: aperi chassis Dataplane_Interfaces_Transceiver_Temperature (20)
    #7505: Capacity (Disk Usage) 
    print_full(sample)         
    print_full(alarm_events)         
    #show_timeseries(sample.head(10))
    titles = [f'{row["protocol_name"]}#{row["Element"]}: {row["Description"]} - {row["Units"]}({row["Parameter"]}))' for idx, row in sample.iterrows()]
    run_homogenous_multivariate_time_series(alarm_events, sample['Filename'].values.tolist(), titles,
                                            interval_width=72, stride=12, no_bins=8, no_symbols=10, 
                                            no_patterns=150, min_len=3, max_len=10, duration=1)
    #Seem to work in some cases, i.e. 10011 for detecting concept drift
    
    
    #show_timeseries(current_device)


def run_hetero_multivariate_time_series(alarm_events, filenames, 
                                        interval_width=24, stride=6, no_bins=5, no_symbols=10, 
                                        no_patterns=100, min_len=4, max_len=10, duration=1):
    #1.3) pre-process and convert to symbolic representations
    #Assume same number of segments in each dimension.... TODO: PADDING!
    #IDEA: Use embedding, then isolation forest...
    #IDEA2: Pairwise FPOF, i.e. if #number of patterns matching D1 and D2 During S1/ total number of patterns in D1 and D2
    #IDEA3: One transaction databases, with itemsets, i.e. (i1,i2,i3), i.e. early -> Maybe better than late?... could also be uses for several devices, i.e. ram/CPU often high in aresta -> find frequent sequential pattenrs (or itemset)
    all_time_series = []
    min_date = max_date = None
    for k, ts_filename in enumerate(filenames):
        ts_i = min_max_norm(remove_outliers_ts(fix_ts(load_ts(trend_dir + ts_filename))))#z_norm_ts
        print(f'ts start: {ts_i["time"].min()}, end: {ts_i["time"].max()}')
        all_time_series.append(ts_i)
        if min_date is None:
            min_date = ts_i['time'].min()
            max_date = ts_i['time'].max()
        else:
            min_date = min(min_date, ts_i['time'].min())
            max_date = min(max_date, ts_i['time'].max())
    
    first_windows = None
    embedding_concat = []
    for k, ts_filename in enumerate(filenames):    
        ts_i = all_time_series[k]
        #padd
        all_time_series[k] = ts_i =  padd(ts_i, min_date, max_date, value='mean', col='average_value')
        print(f'ts start: {ts_i["time"].min()}, end: {ts_i["time"].max()}')
        #symbolic representation
        windows = create_windows(ts_i, interval=interval_width, stride=stride)
        if first_windows is None:
            first_windows = windows
        segments = create_segments(ts_i, windows)
        segments_discretised = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
        #2.1-2.2) mine patterns
        fname_transactions = os.path.abspath('transactions.txt')
        fname_patterns = os.path.abspath('patterns.txt')
        fname_embedding = os.path.abspath('embedding.txt')
        save_transactions(fname_transactions, segments_discretised)
        patterns_df, embedding = mine_patterns_and_create_embedding(fname_transactions, 
                                                                no_transactions = len(segments_discretised), 
                                                                top_k=no_patterns, min_size=min_len, max_size=max_len, duration=duration, 
                                                                patterns_fname=fname_patterns, embedding_fname=fname_embedding)
    
        if len(embedding_concat) == 0:
            embedding_concat = embedding
        else:
            for i in range(0, len(embedding_concat)):
                embedding_concat[i] = embedding_concat[i] + [(pattern_idx + (k * no_patterns), rsupport) for (pattern_idx, rsupport) in embedding[i]]
            print(f'created concatenated embedding. Number of segments: {len(embedding_concat)}. Length first segment: {len(embedding_concat[0])}')
    print(f'created concatenated embedding. Number of segments: {len(embedding_concat)}. Length first segment: {len(embedding_concat[0])}. First 10: {embedding_concat[0][0:10]}')
    #TODO: Check if still works after re-factor
    isof_scores = iso_forest(embedding, no_patterns * len(filenames) + 1, q=0.95)
     
    #res = (1 -res) /2 #1 anomaly, 0 normal
    fig, axs = plt.subplots(len(filenames)+1, figsize=(20,3*len(filenames)))
    
    #plot time series
    for i in range(0, len(filenames)):
        #i.e. every 24 hours -> 10 PAA symbols, thus every 2.4 hour or 2.4*60 minutes 1 symbol 
        #show PAA, since more related to PBAD score
        ts_i_paa = min_max_norm(paa_time_based(all_time_series[i], interval_minutes=int(interval_width/no_symbols * 60)))
        axs[i].plot(ts_i_paa['time'], ts_i_paa['average_value'], color=tableau10blind[i % 10])
        #plot number of patterns matching each segment:
        plot_no_patterns_embedding_concat(axs[i], embedding_concat, first_windows, no_patterns)
        #plot alarms
        ts_alarms = alarm_events[alarm_events['Filename'] == filenames[i]]
        if ts_alarms.shape[0] > 0:
            plot_alarms(axs, i, ts_alarms)
                
    #plot isolation forest score
    axs[len(filenames)].plot([window[0] for window in first_windows], isof_scores, color='orange', marker='x')
    #plot number of patterns matching each segment:
    plot_no_patterns_embedding(axs[len(filenames)], embedding, windows)
    plt.savefig('result_multi_hetero.png',dpi=600)
    plt.show(block=True)
    

run_hetero = True 
if run_hetero:
    element_filter_id = 3
    
    df_all = df_all.sort_values(by=['Element','Parameter','Instance'])
    sample = df_all[(df_all['Element'] == element_filter_id)]  #was 10 
    #3: Aperi Chassis with some interesting warnings
    #10: Aperi Chassis 
    #33: Thomson Video Networks Vibe CP6000 with only BitRate parameter (mixture of supPhoIOTSBitRateUsefulRate and supPhoIOTSBitRateUsefulRate)
    #54: Another Aperi Chassis
    print_full(sample)   
    alarm_events = df_alarms[(df_alarms['element_id'] == element_filter_id)]# & (df_alarms['parameter_id'].isin(sample)
    alarm_events = alarm_events.drop_duplicates(subset=['root_alarm_id'])
    print(sample.iloc[0].to_dict())
    print(list(zip(sample.columns, sample.dtypes)))
    print(alarm_events.iloc[0].to_dict())
    print(list(zip(alarm_events.columns, alarm_events.dtypes)))
    sample['Parameter'] = sample['Parameter'].astype('int32')    
    alarm_events = pd.merge(alarm_events, sample, left_on=['parameter_id'], right_on=['Parameter'])
    print_full(alarm_events)
    #problem: if instance is not NaN/ None -> filter using instance
    #alarm_events['different_instance'] = alarm_events.apply(lambda row: str(row['Instance']) != '' and row['Instance'] != row['table_idx_pk'], axis=1)
    #alarm_events = alarm_events[~alarm_events['different_instance']]
    
    print_full(alarm_events)  
    #sample = sample.head(10)
    sample = sample[sample['Filename'].isin(alarm_events['Filename'].values)] #only TS with alarms
    run_hetero_multivariate_time_series(alarm_events, sample['Filename'].values.tolist(), interval_width=24, stride=5, no_bins=5, no_symbols=10, no_patterns=100, min_len=4, max_len=10, duration=1)
        